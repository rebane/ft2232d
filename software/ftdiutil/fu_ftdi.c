#include "fu_ftdi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <ftdi.h>
#include <usb.h>
#include "fu.h"
#include "fu_ftdi_mpsse.h"

int fu_ftdi_init(fu_t *fu){
	fu->ftdi.debug = 0;
	if(fu->ftdi.debug)printf("fu_ftdi_init\n");
	fu->ftdi.serial = NULL;
	fu->ftdi.desc = NULL;
	fu->ftdi.vid = 0x0403;
	fu->ftdi.pid = 0x6010;
	fu->ftdi.channel = FU_FTDI_CHANNEL_0;
	fu->ftdi.ftdi = NULL;
	fu->ftdi.bitmode = -1;
	fu->ftdi.rebind_on_exit = 0;
	fu->ftdi.buffer_len = 0;
	fu->ftdi.buffer_loc = 0;
	fu->ftdi.buffer = NULL;
	return(FU_OK);
}

int fu_ftdi_test_open(fu_t *fu){
//	if(fu->ftdi.debug)printf("fu_ftdi_test_open\n");
	if(fu->ftdi.ftdi == NULL){
		fu_eprintf(fu, "ftdi_test_open: ftdi interface not opened");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_test_bitmode(fu_t *fu, int bitmode){
//	if(fu->ftdi.debug)printf("fu_ftdi_test_bitmode: %d\n", bitmode);
	fu_ftdi_test_open(fu);
	if(fu->ftdi.bitmode != bitmode){
		fu_eprintf(fu, "ftdi_test_bitmode: ftdi not in current bitmode");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_serial(fu_t *fu, const char *serial){
	if(fu->ftdi.debug)printf("fu_ftdi_serial: %s\n", serial);
	if(fu->ftdi.serial != NULL)free(fu->ftdi.serial);
	fu->ftdi.serial = strdup(serial);
	if(fu->ftdi.serial == NULL){
		fu_eprintf(fu, "ftdi_serial: out of memory");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_device_desc(fu_t *fu, const char *desc){
	if(fu->ftdi.debug)printf("fu_ftdi_desc: %s\n", desc);
	if(fu->ftdi.desc != NULL)free(fu->ftdi.desc);
	fu->ftdi.desc = strdup(desc);
	if(fu->ftdi.desc == NULL){
		fu_eprintf(fu, "ftdi_device_desc: out of memory");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_vid_pid(fu_t *fu, uint16_t vid, uint16_t pid){
	if(fu->ftdi.debug)printf("fu_ftdi_vid_pid: 0x%04X, 0x%04X\n", (unsigned int)vid, (unsigned int)pid);
	fu->ftdi.vid = vid;
	fu->ftdi.pid = pid;
	return(FU_OK);
}

int fu_ftdi_channel(fu_t *fu, int channel){
	if(fu->ftdi.debug)printf("fu_ftdi_channel: %d\n", channel);
	if((channel < FU_FTDI_CHANNEL_0) || (channel > FU_FTDI_CHANNEL_3)){
		fu_eprintf(fu, "channel out of range");
		return(FU_ERROR);
	}
	fu->ftdi.channel = channel;
	return(FU_OK);
}

int fu_ftdi_open(fu_t *fu){
	char device[512];
	int ret;
	if(fu->ftdi.debug)printf("fu_ftdi_open\n");
	if(fu->ftdi.serial == NULL){
		snprintf(device, 511, "i:0x%04X:0x%04X", (unsigned int)fu->ftdi.vid, (unsigned int)fu->ftdi.pid);
	}else{
		snprintf(device, 511, "s:0x%04X:0x%04X:%s", (unsigned int)fu->ftdi.vid, (unsigned int)fu->ftdi.pid, fu->ftdi.serial);
	}
	device[511] = 0;
	fu->ftdi.ftdi = ftdi_new();
	if(fu->ftdi.ftdi == NULL){
		fu_eprintf(fu, "ftdi_new");
		return(FU_ERROR);
	}
	if(fu->ftdi.channel == FU_FTDI_CHANNEL_0){
		ret = ftdi_set_interface(fu->ftdi.ftdi, INTERFACE_A);
	}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_1){
		ret = ftdi_set_interface(fu->ftdi.ftdi, INTERFACE_B);
	}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_2){
		ret = ftdi_set_interface(fu->ftdi.ftdi, INTERFACE_C);
	}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_3){
		ret = ftdi_set_interface(fu->ftdi.ftdi, INTERFACE_D);
	}else{
		fu_eprintf(fu, "ftdi_set_interface: unknown interface");
		return(FU_ERROR);
	}
	if(ret){
		fu_eprintf(fu, "ftdi_set_interface: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	ret = ftdi_usb_open_string(fu->ftdi.ftdi, device);
	if((ret < 0) && (ret != -5)){
		fu_eprintf(fu, "ftdi_usb_open: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	if(ftdi_usb_reset(fu->ftdi.ftdi)){
		fu_eprintf(fu, "ftdi_usb_reset: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	if(ftdi_set_latency_timer(fu->ftdi.ftdi, 2)){
		fu_eprintf(fu, "ftdi_set_latency_timer: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	if(ftdi_usb_purge_buffers(fu->ftdi.ftdi)){
		fu_eprintf(fu, "ftdi_purge_buffers: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	fu->ftdi.buffer_loc = 0;
	fu->ftdi.buffer_len = 4096;
	fu->ftdi.buffer = malloc(fu->ftdi.buffer_len);
	if(fu->ftdi.buffer == NULL){
		fu_eprintf(fu, "out of memory");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_set_bitmode(fu_t *fu, int bitmode, uint8_t bitmask){
	int bm;
	if(fu->ftdi.debug)printf("fu_ftdi_set_bitmode: %d, 0x%02X\n", bitmode, (unsigned int)bitmask);
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	fu->ftdi.bitmode = bitmode;
	if(fu->ftdi.bitmode == FU_FTDI_BITMODE_RESET){
		bm = BITMODE_RESET;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_BITBANG){
		bm = BITMODE_BITBANG;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_MPSSE){
		bm = BITMODE_MPSSE;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_SYNCBB){
		bm = BITMODE_SYNCBB;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_MCU){
		bm = BITMODE_MCU;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_OPTO){
		bm = BITMODE_OPTO;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_CBUS){
		bm = BITMODE_CBUS;
	}else if(fu->ftdi.bitmode == FU_FTDI_BITMODE_SYNCFF){
		bm = BITMODE_SYNCFF;
/*	}else if(fu->ftdi_bitmode == FU_FTDI_BITMODE_FT1284){
		bm = BITMODE_FT1284;*/
	}else{
		fu_eprintf(fu, "ftdi_set_bitmode: unknown bitmode: %d", fu->ftdi.bitmode);
		return(FU_ERROR);
	}
	if(ftdi_set_bitmode(fu->ftdi.ftdi, bitmask, bm)){
		fu_eprintf(fu, "ftdi_set_bitmode: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
	if(fu->ftdi.bitmode == FU_FTDI_BITMODE_MPSSE){
		fu_ftdi_mpsse_init(fu);
	}
	return(FU_OK);
}

int fu_ftdi_set_baudrate(fu_t *fu, int baudrate){
	if(fu->ftdi.debug)printf("fu_ftdi_set_baudrate: %d\n", baudrate);
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	if(ftdi_set_baudrate(fu->ftdi.ftdi, baudrate)){
		fu_eprintf(fu, "ftdi_set_baudrate: %s", ftdi_get_error_string(fu->ftdi.ftdi));
		return(FU_ERROR);
	}
//	printf("INFO: real baudrate: %d\n", ftdi->baudrate);
	return(FU_OK);
}

int fu_ftdi_flush(fu_t *fu){
	if(fu->ftdi.debug)printf("fu_ftdi_flush\n");
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	fu->ftdi.buffer_loc = 0;
	return(FU_OK);
}

int fu_ftdi_sync(fu_t *fu){
	int l;
	if(fu->ftdi.debug)printf("fu_ftdi_sync\n");
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	if(fu->ftdi.buffer_loc){
		l = ftdi_write_data(fu->ftdi.ftdi, fu->ftdi.buffer, fu->ftdi.buffer_loc);
		if(l < 0){
			fu_eprintf(fu, "ftdi_sync: ftdi_write_data: %s", ftdi_get_error_string(fu->ftdi.ftdi));
			return(FU_ERROR);
		}else if(l < fu->ftdi.buffer_loc){
			fu_eprintf(fu, "ftdi_sync: ftdi_write_data");
			return(FU_ERROR);
		}
		fu->ftdi.buffer_loc = 0;
	}
	return(FU_OK);
}

int fu_ftdi_read(fu_t *fu, void *buf, int count){
	int i, l;
	if(fu->ftdi.debug)printf("fu_ftdi_read: %d\n", count);
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	for(i = 0; i < count; ){
		l = ftdi_read_data(fu->ftdi.ftdi, &((uint8_t *)buf)[i], (count - i));
		if(l < 0){
			fu_eprintf(fu, "ftdi_read: ftdi_read_data: %s", ftdi_get_error_string(fu->ftdi.ftdi));
			return(FU_ERROR);
		}
		i += l;
	}
	return(FU_OK);
}

int fu_ftdi_write_nosync(fu_t *fu, void *buf, int count){
	if(fu->ftdi.debug){
		printf("fu_ftdi_write_nosync: %d,", count);
		for(int i = 0; i < count; i++){
			printf(" %02X", (unsigned int)((uint8_t *)buf)[i]);
		}
		printf("\n");
	}
	if(fu_ftdi_test_open(fu) == FU_ERROR)return(FU_ERROR);
	if((fu->ftdi.buffer_loc + count) > fu->ftdi.buffer_len){
		if((count * 2) > 1024){
			fu->ftdi.buffer_len += (count * 2);
		}else{
			fu->ftdi.buffer_len += 1024;
		}
		fu->ftdi.buffer = realloc(fu->ftdi.buffer, fu->ftdi.buffer_len);
	}
	if(fu->ftdi.buffer == NULL){
		fu_eprintf(fu, "ftdi_write: out of memory");
		return(FU_ERROR);
	}
	memcpy(&fu->ftdi.buffer[fu->ftdi.buffer_loc], buf, count);
	fu->ftdi.buffer_loc += count;
	return(FU_OK);
}

int fu_ftdi_write(fu_t *fu, void *buf, int count){
	if(fu_ftdi_write_nosync(fu, buf, count) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_rebind_on_exit(fu_t *fu){
	if(fu->ftdi.debug)printf("fu_ftdi_rebind_on_exit\n");
	fu->ftdi.rebind_on_exit = 1;
	return(FU_OK);
}

int fu_ftdi_rebind(fu_t *fu){
	struct usbfs_ioctl_{
		int ifno;       /* interface 0..N ; negative numbers reserved */
		int ioctl_code; /* MUST encode size + direction of data so the macros in <asm/ioctl.h> give correct values */
		void *data;     /* param buffer (in, or out) */
	}ioctl_param;
	struct usb_dev_handle_{
		int fd;
	};
	struct usb_bus *bus;
	struct usb_device *dev;
	usb_dev_handle *usb_dev;
	char string[256];

#ifndef IOCTL_USBFS_CONNECT
#define IOCTL_USBFS_CONNECT _IO('U', 23)
#endif
#ifndef IOCTL_USBFS_IOCTL
#define IOCTL_USBFS_IOCTL   _IOWR('U', 18, struct usbfs_ioctl_)
#endif
	if(fu->ftdi.debug)printf("fu_ftdi_rebind\n");
	if(fu->ftdi.ftdi != NULL){
		fu_eprintf(fu, "ftdi_rebind: device is opened");
		return(FU_ERROR);
	}
	if((fu->ftdi.channel < FU_FTDI_CHANNEL_0) || (fu->ftdi.channel > FU_FTDI_CHANNEL_3)){
		fu_eprintf(fu, "ftdi_rebind: channel out of range");
		return(FU_ERROR);
	}
	usb_init();
	if(usb_find_busses() < 0){
		fu_eprintf(fu, "ftdi_rebind: usb_find_busses");
		return(FU_ERROR);
	}
	if(usb_find_devices() < 0){
		fu_eprintf(fu, "ftdi_rebind: usb_find_devices");
		return(FU_ERROR);
	}
	for(bus = usb_get_busses(); bus; bus = bus->next){
		for(dev = bus->devices; dev; dev = dev->next){
			if((dev->descriptor.idVendor == fu->ftdi.vid) && (dev->descriptor.idProduct == fu->ftdi.pid)){
				usb_dev = usb_open(dev);
				if(usb_dev == NULL)continue;
				if(fu->ftdi.serial != NULL){
					if(usb_get_string_simple(usb_dev, dev->descriptor.iSerialNumber, string, sizeof(string)) <= 0){
						usb_close(usb_dev);
						continue;
					}
					if(strncmp(string, fu->ftdi.serial, sizeof(string))){
						usb_close(usb_dev);
						continue;
					}
				}
				if(fu->ftdi.channel == FU_FTDI_CHANNEL_0){
					ioctl_param.ifno = 0;
				}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_1){
					ioctl_param.ifno = 1;
				}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_2){
					ioctl_param.ifno = 2;
				}else{
					ioctl_param.ifno = 3;
				}
				ioctl_param.ioctl_code = IOCTL_USBFS_CONNECT;
				ioctl(((struct usb_dev_handle_ *)usb_dev)->fd, IOCTL_USBFS_IOCTL, &ioctl_param);
				usb_close(usb_dev);
				return(FU_OK);
			}
		}
	}
	fu_eprintf(fu, "ftdi_rebind: cannot find device");
	return(FU_ERROR);
}

int fu_ftdi_close(fu_t *fu){
	struct usbfs_ioctl_{
		int ifno;       /* interface 0..N ; negative numbers reserved */
		int ioctl_code; /* MUST encode size + direction of data so the macros in <asm/ioctl.h> give correct values */
		void *data;     /* param buffer (in, or out) */
	}ioctl_param;
	struct usb_dev_handle_{
		int fd;
	};

	if(fu->ftdi.debug)printf("fu_ftdi_close\n");
	if(fu->ftdi.ftdi != NULL){
		usb_release_interface((fu->ftdi.ftdi)->usb_dev, (fu->ftdi.ftdi)->interface);
		if(fu->ftdi.rebind_on_exit){
			ioctl_param.ifno = (fu->ftdi.ftdi)->interface;
						printf("IFNO: %d\n", (int)ioctl_param.ifno);
			ioctl_param.ioctl_code = IOCTL_USBFS_CONNECT;
			ioctl(((struct usb_dev_handle_ *)((fu->ftdi.ftdi)->usb_dev))->fd, IOCTL_USBFS_IOCTL, &ioctl_param);
		}
		ftdi_usb_close(fu->ftdi.ftdi);
		fu->ftdi.ftdi = NULL;
	}else{
		if(fu->ftdi.rebind_on_exit){
			return(fu_ftdi_rebind(fu));
		}
	}
	return(FU_OK);
}

int fu_ftdi_finduart(fu_t *fu){
/*
[rebane@rebane ftdi_sio]$ pwd
/sys/bus/usb/drivers/ftdi_sio

[rebane@rebane ftdi_sio]$ ls -l
total 0
lrwxrwxrwx 1 root root    0 Oct  8 21:23 2-1.2.2:1.0 -> ../../../../devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.2/2-1.2.2/2-1.2.2:1.0
lrwxrwxrwx 1 root root    0 Oct  8 21:23 2-1.2.2:1.1 -> ../../../../devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.2/2-1.2.2/2-1.2.2:1.1
--w------- 1 root root 4096 Oct  8 21:23 bind
lrwxrwxrwx 1 root root    0 Oct  8 21:23 module -> ../../../../module/usbserial
--w------- 1 root root 4096 Oct  8 21:23 uevent
--w------- 1 root root 4096 Oct  8 21:23 unbind

[rebane@rebane ftdi_sio]$ cat 2-1.2.2\:1.0/../serial
R01

[rebane@rebane ftdi_sio]$ cat 2-1.2.2\:1.0/ttyUSB0/tty/ttyUSB0/dev
188:0

[rebane@rebane ftdi_sio]$ ls -l /dev/ttyUSB0
crw-rw----+ 1 root plugdev 188, 0 Oct  8 20:52 /dev/ttyUSB0
*/
	return(FU_OK);
}

uint16_t fu_ftdi_pin_get(fu_t *fu, char *pin){
	if(fu->ftdi.channel == FU_FTDI_CHANNEL_0){
		if(!strcasecmp(pin, "ad0"))return(0x0001);
		if(!strcasecmp(pin, "ad1"))return(0x0002);
		if(!strcasecmp(pin, "ad2"))return(0x0004);
		if(!strcasecmp(pin, "ad3"))return(0x0008);
		if(!strcasecmp(pin, "ad4"))return(0x0010);
		if(!strcasecmp(pin, "ad5"))return(0x0020);
		if(!strcasecmp(pin, "ad6"))return(0x0040);
		if(!strcasecmp(pin, "ad7"))return(0x0080);

		if(!strcasecmp(pin, "ac0"))return(0x0100);
		if(!strcasecmp(pin, "ac1"))return(0x0200);
		if(!strcasecmp(pin, "ac2"))return(0x0400);
		if(!strcasecmp(pin, "ac3"))return(0x0800);
		if(!strcasecmp(pin, "ac4"))return(0x1000);
		if(!strcasecmp(pin, "ac5"))return(0x2000);
		if(!strcasecmp(pin, "ac6"))return(0x4000);
		if(!strcasecmp(pin, "ac7"))return(0x8000);
	}else if(fu->ftdi.channel == FU_FTDI_CHANNEL_1){
		if(!strcasecmp(pin, "bd0"))return(0x0001);
		if(!strcasecmp(pin, "bd1"))return(0x0002);
		if(!strcasecmp(pin, "bd2"))return(0x0004);
		if(!strcasecmp(pin, "bd3"))return(0x0008);
		if(!strcasecmp(pin, "bd4"))return(0x0010);
		if(!strcasecmp(pin, "bd5"))return(0x0020);
		if(!strcasecmp(pin, "bd6"))return(0x0040);
		if(!strcasecmp(pin, "bd7"))return(0x0080);

		if(!strcasecmp(pin, "bc0"))return(0x0100);
		if(!strcasecmp(pin, "bc1"))return(0x0200);
		if(!strcasecmp(pin, "bc2"))return(0x0400);
		if(!strcasecmp(pin, "bc3"))return(0x0800);
		if(!strcasecmp(pin, "bc4"))return(0x1000);
		if(!strcasecmp(pin, "bc5"))return(0x2000);
		if(!strcasecmp(pin, "bc6"))return(0x4000);
		if(!strcasecmp(pin, "bc7"))return(0x8000);
	}
	if(!strcasecmp(pin, "txd") || !strcasecmp(pin, "tx") || !strcasecmp(pin, "tck") || !strcasecmp(pin, "sk") || !strcasecmp(pin, "clk") || !strcasecmp(pin, "sclk") || !strcasecmp(pin, "sck") || !strcasecmp(pin, "swc") || !strcasecmp(pin, "swclk"))return(0x0001);
	if(!strcasecmp(pin, "rxd") || !strcasecmp(pin, "rx") || !strcasecmp(pin, "tdi") || !strcasecmp(pin, "do") || !strcasecmp(pin, "si") || !strcasecmp(pin, "mosi"))return(0x0002);
	if(!strcasecmp(pin, "rts") || !strcasecmp(pin, "tdo") || !strcasecmp(pin, "di") || !strcasecmp(pin, "so") || !strcasecmp(pin, "miso") || !strcasecmp(pin, "swd") || !strcasecmp(pin, "swdio"))return(0x0004);
	if(!strcasecmp(pin, "cts") || !strcasecmp(pin, "tms") || !strcasecmp(pin, "cs"))return(0x0008);
	if(!strcasecmp(pin, "dtr") || !strcasecmp(pin, "gpiol0") || !strcasecmp(pin, "trst") || !strcmp(pin, "rst"))return(0x0010);
	if(!strcasecmp(pin, "dsr") || !strcasecmp(pin, "gpiol1"))return(0x0020);
	if(!strcasecmp(pin, "dcd") || !strcasecmp(pin, "gpiol2"))return(0x0040);
	if(!strcasecmp(pin, "ri")  || !strcasecmp(pin, "gpiol3"))return(0x0080);

	if(!strcasecmp(pin, "gpioh0"))return(0x0100);
	if(!strcasecmp(pin, "gpioh1"))return(0x0200);
	if(!strcasecmp(pin, "gpioh2"))return(0x0400);
	if(!strcasecmp(pin, "gpioh3"))return(0x0800);
	if(!strcasecmp(pin, "gpioh4"))return(0x1000);
	if(!strcasecmp(pin, "gpioh5"))return(0x2000);
	if(!strcasecmp(pin, "gpioh6"))return(0x4000);
	if(!strcasecmp(pin, "gpioh7"))return(0x8000);

	fu_eprintf(fu, "unknown pin \"%s\"", pin);
	return(0);
}

