#ifndef _FU_FTDI_H_
#define _FU_FTDI_H_

#include <stdint.h>
#include "fu.h"

#define FU_FTDI_CHANNEL_0       1
#define FU_FTDI_CHANNEL_1       2
#define FU_FTDI_CHANNEL_2       3
#define FU_FTDI_CHANNEL_3       4

#define FU_FTDI_BITMODE_RESET   0x00
#define FU_FTDI_BITMODE_BITBANG 0x01
#define FU_FTDI_BITMODE_MPSSE   0x02
#define FU_FTDI_BITMODE_SYNCBB  0x04
#define FU_FTDI_BITMODE_MCU     0x08
#define FU_FTDI_BITMODE_OPTO    0x10
#define FU_FTDI_BITMODE_CBUS    0x20
#define FU_FTDI_BITMODE_SYNCFF  0x40
#define FU_FTDI_BITMODE_FT1284  0x80

int fu_ftdi_init(fu_t *fu);
int fu_ftdi_test_open(fu_t *fu);
int fu_ftdi_test_bitmode(fu_t *fu, int bitmode);
int fu_ftdi_serial(fu_t *fu, const char *serial);
int fu_ftdi_device_desc(fu_t *fu, const char *desc);
int fu_ftdi_vid_pid(fu_t *fu, uint16_t vid, uint16_t pid);
int fu_ftdi_channel(fu_t *fu, int channel);
int fu_ftdi_open(fu_t *fu);
int fu_ftdi_set_bitmode(fu_t *fu, int bitmode, uint8_t bitmask);
int fu_ftdi_set_baudrate(fu_t *fu, int baudrate);
int fu_ftdi_flush(fu_t *fu);
int fu_ftdi_sync(fu_t *fu);
int fu_ftdi_read(fu_t *fu, void *buf, int count);

int fu_ftdi_write_nosync(fu_t *fu, void *buf, int count);
int fu_ftdi_write(fu_t *fu, void *buf, int count);

int fu_ftdi_rebind_on_exit(fu_t *fu);
int fu_ftdi_rebind(fu_t *fu);
int fu_ftdi_close(fu_t *fu);

uint16_t fu_ftdi_pin_get(fu_t *fu, char *pin);

#endif

