#include "fu_ftdi_mpsse_pins.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"

int fu_ftdi_mpsse_pins_init(fu_t *fu){
	fu->ftdi_mpsse_pins.dir_wanted = 0;
	fu->ftdi_mpsse_pins.data_wanted = 0;
	fu->ftdi_mpsse_pins.refresh_needed = 1;
	fu->ftdi_mpsse_pins.ready = 1;
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_test_init(fu_t *fu){
	if(!fu->ftdi_mpsse_pins.ready){
		fu_eprintf(fu, "ftdi_mpsse_pins_test_init: ftdi_mpsse_pins not initialized\n");
		return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_set_norefresh(fu_t *fu, uint16_t high, uint16_t low){
	int i;
	uint16_t pin;
	fu->ftdi_mpsse_pins.dir_wanted = 0;
	for(i = 0; i < 16; i++){
		pin = ((uint16_t)1 << i);
		if(high & pin){
			fu->ftdi_mpsse_pins.dir_wanted |= pin;
			fu->ftdi_mpsse_pins.data_wanted |= pin;
		}
	}
	for(i = 0; i < 16; i++){
		pin = ((uint16_t)1 << i);
		if(low & pin){
			fu->ftdi_mpsse_pins.dir_wanted |= pin;
			fu->ftdi_mpsse_pins.data_wanted &= ~pin;
		}
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_set_nosync(fu_t *fu, uint16_t high, uint16_t low){
	if(fu_ftdi_mpsse_pins_set_norefresh(fu, high, low) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_mpsse_pins_refresh_nosync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_set(fu_t *fu, uint16_t high, uint16_t low){
	if(fu_ftdi_mpsse_pins_set_nosync(fu, high, low) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_mpsse_pins_sync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_merge_norefresh(fu_t *fu, uint16_t high, uint16_t low, uint16_t input){
	int i;
	uint16_t pin;
	for(i = 0; i < 16; i++){
		pin = ((uint16_t)1 << i);
		if(high & pin){
			fu->ftdi_mpsse_pins.dir_wanted |= pin;
			fu->ftdi_mpsse_pins.data_wanted |= pin;
		}
	}
	for(i = 0; i < 16; i++){
		pin = ((uint16_t)1 << i);
		if(low & pin){
			fu->ftdi_mpsse_pins.dir_wanted |= pin;
			fu->ftdi_mpsse_pins.data_wanted &= ~pin;
		}
	}
	for(i = 0; i < 16; i++){
		pin = ((uint16_t)1 << i);
		if(input & pin){
			fu->ftdi_mpsse_pins.dir_wanted &= ~pin;
		}
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_merge_nosync(fu_t *fu, uint16_t high, uint16_t low, uint16_t input){
	if(fu_ftdi_mpsse_pins_merge_norefresh(fu, high, low, input) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_mpsse_pins_refresh_nosync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_merge(fu_t *fu, uint16_t high, uint16_t low, uint16_t input){
	if(fu_ftdi_mpsse_pins_merge_nosync(fu, high, low, input) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_mpsse_pins_sync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_refresh_nosync(fu_t *fu){
	if(fu->ftdi_mpsse_pins.refresh_needed || (((fu->ftdi_mpsse_pins.data_wanted >> 0) & 0xFF) != ((fu->ftdi_mpsse_pins.data >> 0) & 0xFF)) || (((fu->ftdi_mpsse_pins.dir_wanted >> 0) & 0xFF) != ((fu->ftdi_mpsse_pins.dir >> 0) & 0xFF))){
		fu->ftdi_mpsse_pins.sync_needed = 1;
		if(fu_ftdi_mpsse_set_bits_low(fu, ((fu->ftdi_mpsse_pins.data_wanted >> 0) & 0xFF), ((fu->ftdi_mpsse_pins.dir_wanted >> 0) & 0xFF)) == FU_ERROR)return(FU_ERROR);
	}
	if(fu->ftdi_mpsse_pins.refresh_needed || (((fu->ftdi_mpsse_pins.data_wanted >> 8) & 0xFF) != ((fu->ftdi_mpsse_pins.data >> 8) & 0xFF)) || (((fu->ftdi_mpsse_pins.dir_wanted >> 8) & 0xFF) != ((fu->ftdi_mpsse_pins.dir >> 8) & 0xFF))){
		fu->ftdi_mpsse_pins.sync_needed = 1;
		if(fu_ftdi_mpsse_set_bits_high(fu, ((fu->ftdi_mpsse_pins.data_wanted >> 8) & 0xFF), ((fu->ftdi_mpsse_pins.dir_wanted >> 8) & 0xFF)) == FU_ERROR)return(FU_ERROR);
	}
	fu->ftdi_mpsse_pins.data = fu->ftdi_mpsse_pins.data_wanted;
	fu->ftdi_mpsse_pins.dir = fu->ftdi_mpsse_pins.dir_wanted;
	fu->ftdi_mpsse_pins.refresh_needed = 0;
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_refresh(fu_t *fu){
	if(fu_ftdi_mpsse_pins_refresh_nosync(fu) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_mpsse_pins_sync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_sync(fu_t *fu){
	if(fu->ftdi_mpsse_pins.sync_needed){
		if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
		fu->ftdi_mpsse_pins.sync_needed = 0;
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_get_low(fu_t *fu, uint16_t *pins){
	uint8_t c;
	if(fu_ftdi_mpsse_get_bits_low(fu) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_read(fu, &c, 1) == FU_ERROR)return(FU_ERROR);
	*pins = (*pins & 0xFF00) | c;
	return(FU_OK);
}

int fu_ftdi_mpsse_pins_get_high(fu_t *fu, uint16_t *pins){
	uint8_t c;
	if(fu_ftdi_mpsse_get_bits_high(fu) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_read(fu, &c, 1) == FU_ERROR)return(FU_ERROR);
	*pins = (*pins & 0x00FF) | ((uint16_t)c << 8);
	return(FU_OK);
}

