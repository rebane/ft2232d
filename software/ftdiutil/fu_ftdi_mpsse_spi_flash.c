#include "fu_ftdi_mpsse_spi_flash.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"
#include "fu_ftdi_mpsse_pins.h"
#include "fu_ftdi_mpsse_spi.h"

int fu_ftdi_mpsse_spi_flash_init(fu_t *fu, char *pin){
	if (pin == NULL)
		fu->ftdi_mpsse_spi_flash.cs_low = fu_ftdi_pin_get(fu, "cs");
	else
		fu->ftdi_mpsse_spi_flash.cs_low = fu_ftdi_pin_get(fu, pin);

	fu->ftdi_mpsse_spi_flash.cs_high = 0;
	fu->ftdi_mpsse_spi_flash.write_flags = FU_FTDI_MPSSE_FLAG_WRITE_NEG;
	fu->ftdi_mpsse_spi_flash.read_flags = 0;
	fu->ftdi_mpsse_spi_flash.address_bytes = 3;
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_test_init(fu_t *fu){
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_transfer(fu_t *fu, void *out, int out_len, void *in, int in_len){
	fu_ftdi_mpsse_pins_merge_nosync(fu, fu->ftdi_mpsse_spi_flash.cs_high, fu->ftdi_mpsse_spi_flash.cs_low, 0);
	fu_ftdi_mpsse_spi_transfer_half_nosync(fu, fu->ftdi_mpsse_spi_flash.write_flags, fu->ftdi_mpsse_spi_flash.read_flags, out, out_len, in_len);
	fu_ftdi_mpsse_pins_merge_nosync(fu, fu->ftdi_mpsse_spi_flash.cs_low, fu->ftdi_mpsse_spi_flash.cs_high, 0);
	fu_ftdi_sync(fu);
	fu_ftdi_read(fu, in, in_len);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_status(fu_t *fu, uint8_t *status){
	fu_ftdi_mpsse_spi_flash_transfer(fu, "\x05", 1, status, 1);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_write_status(fu_t *fu, uint8_t status){
	uint8_t buffer[2];
	buffer[0] = 0x01;
	buffer[1] = status;
	fu_ftdi_mpsse_spi_flash_transfer(fu, buffer, 2, NULL, 0);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_wait_busy(fu_t *fu){
	uint8_t status;
	while(1){
		fu_ftdi_mpsse_spi_flash_status(fu, &status);
		if(!(status & 0x01))return(FU_OK);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_wait_wel(fu_t *fu){
	uint8_t status;
	while(1){
		fu_ftdi_mpsse_spi_flash_status(fu, &status);
		if(status & 0x02)return(FU_OK);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_jedec(fu_t *fu, void *jedec){
	fu_ftdi_mpsse_spi_flash_transfer(fu, "\x9F", 1, jedec, 3);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_write_enable(fu_t *fu, int on){
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	if(on){
		fu_ftdi_mpsse_spi_flash_transfer(fu, "\x06", 1, NULL, 0);
		fu_ftdi_mpsse_spi_flash_wait_wel(fu);
	}else{
		fu_ftdi_mpsse_spi_flash_transfer(fu, "\x04", 1, NULL, 0);
		fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_set_address_bytes(fu_t *fu, int address_bytes){
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	if(address_bytes == 3){
		fu_ftdi_mpsse_spi_flash_transfer(fu, "\xE9", 1, NULL, 0);
		fu->ftdi_mpsse_spi_flash.address_bytes = 3;
	}else if(address_bytes == 4){
		fu_ftdi_mpsse_spi_flash_transfer(fu, "\xB7", 1, NULL, 0);
		fu->ftdi_mpsse_spi_flash.address_bytes = 4;
	}
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_write_extended_address(fu_t *fu, uint8_t ea){
	uint8_t buffer[2];
	fu_ftdi_mpsse_spi_flash_write_enable(fu, 1);
	buffer[0] = 0xC5;
	buffer[1] = ea;
	fu_ftdi_mpsse_spi_flash_transfer(fu, buffer, 2, NULL, 0);
	return(FU_OK);
}

static void fu_ftdi_mpsse_spi_encode_address(fu_t *fu, uint8_t *buffer, uint32_t address){
	if(fu->ftdi_mpsse_spi_flash.address_bytes == 3){
		buffer[0] = (address >> 16) & 0xFF;
		buffer[1] = (address >> 8) & 0xFF;
		buffer[2] = (address >> 0) & 0xFF;
	}else if(fu->ftdi_mpsse_spi_flash.address_bytes == 4){
		buffer[0] = (address >> 24) & 0xFF;
		buffer[1] = (address >> 16) & 0xFF;
		buffer[2] = (address >> 8) & 0xFF;
		buffer[3] = (address >> 0) & 0xFF;
	}
}

int fu_ftdi_mpsse_spi_flash_erase(fu_t *fu, uint8_t command, uint32_t offset){
	uint8_t buffer[5];
	fu_ftdi_mpsse_spi_flash_write_enable(fu, 1);
	buffer[0] = command;
	fu_ftdi_mpsse_spi_encode_address(fu, &buffer[1], offset);
	fu_ftdi_mpsse_spi_flash_transfer(fu, buffer, 1 + fu->ftdi_mpsse_spi_flash.address_bytes, NULL, 0);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_erase_area(fu_t *fu, uint8_t command, uint32_t step, uint32_t offset, uint32_t len){
	uint32_t i;
	for(i = 0; i < len; i += step){
		printf("\rerase: %lu", (unsigned long int)i);
		fflush(stdout);
		fu_ftdi_mpsse_spi_flash_erase(fu, command, offset + i);
	}
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	printf("\rerase: %lu\n", (unsigned long int)i);
	return(FU_OK); // 1c:88:79:54:bd:85
}

int fu_ftdi_mpsse_spi_flash_write(fu_t *fu, uint32_t offset, void *data, uint32_t len){
	uint8_t buffer[5 + 256];
	fu_ftdi_mpsse_spi_flash_write_enable(fu, 1);
	buffer[0] = 0x02;
	fu_ftdi_mpsse_spi_encode_address(fu, &buffer[1], offset);
	memcpy(&buffer[1 + fu->ftdi_mpsse_spi_flash.address_bytes], data, len);
	fu_ftdi_mpsse_spi_flash_transfer(fu, buffer, 1 + fu->ftdi_mpsse_spi_flash.address_bytes + len, NULL, 0);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_write_fd(fu_t *fu, uint32_t offset, int fd, uint32_t len){
	uint32_t i, l;
	uint8_t buffer[256];
	for(i = 0; i < len; i += 256){
		l = len - i;
		if(l > 256)l = 256;
		printf("\rwrite: %lu", (unsigned long int)i);
		fflush(stdout);
		read(fd, buffer, l);
		fu_ftdi_mpsse_spi_flash_write(fu, (offset + i), buffer, l);
	}
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	printf("\rwrite: %lu\n", (unsigned long int)i);
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_erase_write_file(fu_t *fu, uint32_t flash_offset, uint32_t file_offset, char *filename, int32_t len){
	int fd;
	uint32_t l;
	struct stat s;
	fd = open(filename, O_RDONLY);
	fstat(fd, &s);
	if(s.st_size < file_offset){
		return(FU_ERROR);
	}
	l = s.st_size - file_offset;
	if((len >= 0) && (len < l))l = len;
	lseek(fd, file_offset, SEEK_SET);
	fu_ftdi_mpsse_spi_flash_erase_area(fu, 0x20, 4096, flash_offset, l);
	fu_ftdi_mpsse_spi_flash_write_fd(fu, flash_offset, fd, l);
	close(fd);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_read(fu_t *fu, uint32_t offset, void *data, uint32_t len){
	uint8_t buffer[6];
	buffer[0] = 0x0B;
	fu_ftdi_mpsse_spi_encode_address(fu, &buffer[1], offset);
	fu_ftdi_mpsse_spi_flash_transfer(fu, buffer, 1 + fu->ftdi_mpsse_spi_flash.address_bytes + 1, data, len);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_read_fd(fu_t *fu, uint32_t offset, int fd, uint32_t len){
	uint32_t i, l;
	uint8_t buffer[256];
	for(i = 0; i < len; i += 256){
		l = len - i;
		if(l > 256)l = 256;
		printf("\rread: %lu", (unsigned long int)i);
		fflush(stdout);
		fu_ftdi_mpsse_spi_flash_read(fu, (offset + i), buffer, l);
		write(fd, buffer, l);
	}
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	printf("\rread: %lu\n", (unsigned long int)i);
	fu_ftdi_mpsse_spi_flash_wait_busy(fu);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_flash_read_file(fu_t *fu, uint32_t flash_offset, uint32_t file_offset, char *filename, int32_t len){
	int fd;
	fd = open(filename, O_RDWR | O_CREAT, 0644);
	lseek(fd, file_offset, SEEK_SET);
	fu_ftdi_mpsse_spi_flash_read_fd(fu, flash_offset, fd, len);
	close(fd);
	return(FU_OK);
}

