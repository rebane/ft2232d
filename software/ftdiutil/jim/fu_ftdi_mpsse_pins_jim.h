#ifndef _FU_FTDI_MPSSE_PINS_JIM_H_
#define _FU_FTDI_MPSSE_PINS_JIM_H_

#include <stdint.h>
#include <jim.h>
#include "fu.h"

int fu_ftdi_mpsse_pins_jim_init(fu_t *fu);

#endif


