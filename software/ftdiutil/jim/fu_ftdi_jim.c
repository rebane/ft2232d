#include "fu_ftdi_jim.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu.h"
#include "fu_jim.h"

static int fu_ftdi_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	const char *subcmd;
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int c;
	fu_func(fu, "ftdi");
	if(argc < 2){
		fu_eprintf(fu, "ftdi subcommand (args)");
		return(JIM_ERR);
	}
	subcmd = Jim_String(argv[1]);
	if(!strcmp(subcmd, "vid_pid")){
		if(argc < 4){
			fu_eprintf(fu, "ftdi vid_pid (vid pid)");
			return(JIM_ERR);
		}
		if(fu_ftdi_vid_pid(fu, strtol(Jim_String(argv[2]), NULL, 0), strtol(Jim_String(argv[3]), NULL, 0)) == FU_ERROR)return(JIM_ERR);
		return(JIM_OK);
	}else if(!strcmp(subcmd, "channel")){
		if(argc < 3){
			fu_eprintf(fu, "ftdi channel (0-3)");
			return(JIM_ERR);
		}
		c = strtol(Jim_String(argv[2]), NULL, 0);
		if(c == 0){
			c = FU_FTDI_CHANNEL_0;
		}else if(c == 1){
			c = FU_FTDI_CHANNEL_1;
		}else if(c == 2){
			c = FU_FTDI_CHANNEL_2;
		}else if(c == 3){
			c = FU_FTDI_CHANNEL_3;
		}else{
			fu_eprintf(fu, "ftdi_channel: channel out of range: %d", c);
			return(JIM_ERR);
		}
		if(fu_ftdi_channel(fu, c) == FU_ERROR)return(JIM_ERR);
		return(JIM_OK);
	}
	return(JIM_OK);
}

static int fu_ftdi_serial_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_serial");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_serial serial_string");
		return(JIM_ERR);
	}
	if(fu_ftdi_serial(fu, Jim_String(argv[1])) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_vid_pid_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_vid_pid");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_vid_pid (vid pid)");
		return(JIM_ERR);
	}
	if(fu_ftdi_vid_pid(fu, strtol(Jim_String(argv[1]), NULL, 0), strtol(Jim_String(argv[2]), NULL, 0)) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_channel_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int c;
	fu_func(fu, "ftdi_channel");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_channel (0-3)");
		return(JIM_ERR);
	}
	c = strtol(Jim_String(argv[1]), NULL, 0);
	if(c == 0){
		c = FU_FTDI_CHANNEL_0;
	}else if(c == 1){
		c = FU_FTDI_CHANNEL_1;
	}else if(c == 2){
		c = FU_FTDI_CHANNEL_2;
	}else if(c == 3){
		c = FU_FTDI_CHANNEL_3;
	}else{
		fu_eprintf(fu, "ftdi_channel: channel out of range: %d", c);
		return(JIM_ERR);
	}
	if(fu_ftdi_channel(fu, c) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_init_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "init");
	if(fu_ftdi_open(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static uint16_t fu_ftdi_set_bitmask_jim(fu_t *fu, Jim_Obj *obj, uint16_t bitmask){
	uint16_t pin;
	int i, l;
	Jim_Obj *name;
	l = Jim_ListLength((Jim_Interp *)fu->user, obj);
	if((l > 1) && Jim_IsList(obj)){
		for(i = 0; i < l; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, obj, i, &name, JIM_NONE);
			bitmask |= fu_ftdi_set_bitmask_jim(fu, name, bitmask);
		}
	}else{
		pin = fu_ftdi_pin_get(fu, (char *)Jim_String(obj));
		if(pin > 255){
			fu_eprintf(fu, "unsupported pin \"%s\" for this function", Jim_String(obj));
			return(256);
		}
		bitmask |= pin;
	}
	return(bitmask);
}

static int fu_ftdi_set_bitmode_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	char *bitmode;
	uint8_t bitmask;
	int i, ret;
	fu_func(fu, "ftdi_set_bitmode");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_set_bitmode (reset, bitbang, mpsse, syncbb, mcu, opto, cbus, syncff) output-pin-list");
		return(JIM_ERR);
	}
	bitmask = 0;
	bitmode = (char *)Jim_String(argv[1]);
	for(i = 2; i < argc; i++){
		bitmask = fu_ftdi_set_bitmask_jim(fu, argv[i], bitmask);
		if(bitmask > 255)return(JIM_ERR);
	}
	// printf("INFO: bitmask: 0x%02X\n", (unsigned int)bitmask);
	if(!strcasecmp(bitmode, "reset")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_RESET, bitmask);
	}else if(!strcasecmp(bitmode, "bitbang")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_BITBANG, bitmask);
	}else if(!strcasecmp(bitmode, "mpsse")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_MPSSE, bitmask);
	}else if(!strcasecmp(bitmode, "syncbb")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_SYNCBB, bitmask);
	}else if(!strcasecmp(bitmode, "mcu")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_MCU, bitmask);
	}else if(!strcasecmp(bitmode, "opto")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_OPTO, bitmask);
	}else if(!strcasecmp(bitmode, "cbus")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_CBUS, bitmask);
	}else if(!strcasecmp(bitmode, "syncff")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_SYNCFF, bitmask);
	}else if(!strcasecmp(bitmode, "ft1284")){
		ret = fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_FT1284, bitmask);
	}else{
		fu_eprintf(fu, "invalid bitmode");
		return(JIM_ERR);
	}
	if(ret == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_set_baudrate_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int baudrate;
	fu_func(fu, "ftdi_set_baudrate");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_set_baudrate baudrate");
		return(JIM_ERR);
	}
	baudrate = strtol(Jim_String(argv[1]), NULL, 0);
	if(fu_ftdi_set_baudrate(fu, baudrate) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_flush_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_flush");
	if(fu_ftdi_flush(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_sync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_sync");
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
//	Jim_Collect(jim);
	return(JIM_OK);
}

static int fu_ftdi_read_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	void *buf;
	int count, start_index;
	fu_func(fu, "ftdi_read");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_read variable count (start_index)");
		return(JIM_ERR);
	}
	if(argc > 3){
		start_index = strtol((char *)Jim_String(argv[3]), NULL, 0);
	}else{
		start_index = 0;
	}
	count = strtoul(Jim_String(argv[2]), NULL, 0);
	buf = malloc(count);
	if(buf == NULL){
		fu_eprintf(fu, "out of memory");
		return(JIM_ERR);
	}
	if(fu_ftdi_read(fu, buf, count) == FU_ERROR){
		free(buf);
		return(JIM_ERR);
	}
	jim_buf2array(jim, (char *)Jim_String(argv[1]), buf, count, start_index);
	free(buf);
	return(JIM_OK);
}

static int fu_ftdi_read_file_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	void *buf;
	int count, i;
	fu_func(fu, "ftdi_read_file");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_read_file fd count");
		return(JIM_ERR);
	}
	count = strtoul(Jim_String(argv[2]), NULL, 0);
	buf = malloc(count);
	if(buf == NULL){
		fu_eprintf(fu, "out of memory");
		return(JIM_ERR);
	}
	if(fu_ftdi_read(fu, buf, count) == FU_ERROR){
		free(buf);
		return(JIM_ERR);
	}
	i = write(strtol(Jim_String(argv[1]), NULL, 0), buf, count);
	if(i < 0){
		free(buf);
		fu_eprintf(fu, "write: %s", strerror(errno));
		return(JIM_ERR);
	}else if(i != count){
		free(buf);
		fu_eprintf(fu, "write");
		return(JIM_ERR);
	}
	free(buf);
	return(JIM_OK);
}

static int fu_ftdi_write_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	int i, j, l, count, buf_len;
	uint8_t *buf;
	Jim_Obj *name, *value;
	if(argc < 2){
		fu_eprintf(fu, "%s: data (start_index) (len)", fu->func);
		return(FU_ERROR);
	}
	count = 0;
	buf_len = 0;
	buf = NULL;
	if(Jim_IsList(argv[1])){
		l = Jim_ListLength((Jim_Interp *)fu->user, argv[1]) / 2;
		if(argc > 2){
			i = strtol(Jim_String(argv[2]), NULL, 0);
			if(i < 0){
				fu_eprintf(fu, "ftdi_write: out of range: %d", i);
				return(FU_ERROR);
			}
			if(i > l){
				fu_eprintf(fu, "ftdi_write: out of range: %d > %d", i, l);
				return(FU_ERROR);
			}
		}else{
			i = 0;
		}
		if(argc > 3){
			j = strtoul(Jim_String(argv[3]), NULL, 0);
			if(j < 0){
				fu_eprintf(fu, "ftdi_write: out of range: %d", j);
				return(FU_ERROR);
			}
			if((j + i) > l){
				fu_eprintf(fu, "ftdi_write: out of range: (%d + %d) > %d", i, j, l);
				return(FU_ERROR);
			}
			j += i;
		}else{
			j = l;
		}
		for( ; i < j; i++){
			if(buf_len == count){
				buf_len += 256;
				buf = realloc(buf, buf_len);
				if(buf == NULL){
					fu_eprintf(fu, "ftdi_write: out of memory");
					return(FU_ERROR);
				}
			}
			Jim_ListIndex((Jim_Interp *)fu->user, argv[1], (i * 2), &name, JIM_NONE);
			Jim_ListIndex((Jim_Interp *)fu->user, argv[1], (i * 2) + 1, &value, JIM_NONE);
			buf[count++] = strtoul(Jim_String(value), NULL, 0);
		}
	}else{
		for(i = 1; i < argc; i++){
			if(buf_len == count){
				buf_len += 256;
				buf = realloc(buf, buf_len);
				if(buf == NULL){
					fu_eprintf(fu, "ftdi_write: out of memory");
					return(FU_ERROR);
				}
			}
			buf[count++] = strtoul(Jim_String(argv[i]), NULL, 0);
		}
	}
	if(fu_ftdi_write_nosync(fu, buf, count) == FU_ERROR){
		free(buf);
		return(FU_ERROR);
	}
	free(buf);
	return(FU_OK);
}

static int fu_ftdi_write_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_write_nosync");
	if(fu_ftdi_write_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_write_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // tuleks kasutada õiget API funktsiooni
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_write");
	if(fu_ftdi_write_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_write_file_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	void *buf;
	int count, i;
	if(argc < 3){
		fu_eprintf(fu, "fd count");
		return(FU_ERROR);
	}
	count = strtoul(Jim_String(argv[2]), NULL, 0);
	buf = malloc(count);
	if(buf == NULL){
		fu_eprintf(fu, "out of memory");
		return(FU_ERROR);
	}
	i = read(strtol(Jim_String(argv[1]), NULL, 0), buf, count);
	if(i < 0){
		free(buf);
		fu_eprintf(fu, "read: %s", strerror(errno));
		return(FU_ERROR);
	}else if(i != count){
		free(buf);
		fu_eprintf(fu, "read");
		return(FU_ERROR);
	}
	if(fu_ftdi_write_nosync(fu, buf, count) == FU_ERROR){
		free(buf);
		return(FU_ERROR);
	}
	free(buf);
	return(FU_OK);
}

static int fu_ftdi_write_file_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_write_file_nosync");
	if(fu_ftdi_write_file_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_write_file_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_write_file");
	if(fu_ftdi_write_file_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_rebind_on_exit_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_rebind_on_exit");
	if(fu_ftdi_rebind_on_exit(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_rebind_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_rebind");
	if(fu_ftdi_rebind(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

int fu_ftdi_jim_init(fu_t *fu){
	int ret;
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi", fu_ftdi_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_serial", fu_ftdi_serial_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_vid_pid", fu_ftdi_vid_pid_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_channel", fu_ftdi_channel_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_layout_init", fu_jim_nop_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_layout_signal", fu_jim_nop_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "init", fu_ftdi_init_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_set_bitmode", fu_ftdi_set_bitmode_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_set_baudrate", fu_ftdi_set_baudrate_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_flush", fu_ftdi_flush_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_sync", fu_ftdi_sync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_read", fu_ftdi_read_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_read_file", fu_ftdi_read_file_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_write_nosync", fu_ftdi_write_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_write", fu_ftdi_write_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_write_file_nosync", fu_ftdi_write_file_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_write_file", fu_ftdi_write_file_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_rebind_on_exit", fu_ftdi_rebind_on_exit_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_rebind", fu_ftdi_rebind_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	return(FU_OK);
}

