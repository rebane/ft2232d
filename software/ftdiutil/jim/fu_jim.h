#ifndef _FU_JIM_H_
#define _FU_JIM_H_

#include <stdint.h>
#include <jim.h>
#include "fu.h"

typedef struct fu_jim_struct fu_jim_t;

#include "fu_ftdi_jim.h"
#include "fu_ftdi_mpsse_jim.h"
#include "fu_ftdi_mpsse_pins_jim.h"
#include "fu_jim_ftdi_mpsse_spi.h"
#include "fu_jim_ftdi_mpsse_spi_flash.h"
#include "fu_jim_ftdi_mpsse_i2c.h"

struct fu_jim_struct{
	Jim_Interp *jim;
	fu_t *fu;
	void *user;
};

int fu_jim_init(fu_jim_t *fj, fu_t *fu, void *user);
int fu_jim_source(fu_jim_t *fj, char *filename);
int fu_jim_command(fu_jim_t *fj, char *command);

int fu_jim_nop_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv);

void jim_buf2array(Jim_Interp *jim, char *variable, void *buf, int count, int start_index);
void jim_uchar2variable(Jim_Interp *jim, char *variable, uint8_t c, char *func);
void jim_int2variable(Jim_Interp *jim, char *variable, int i, char *func);

#endif

