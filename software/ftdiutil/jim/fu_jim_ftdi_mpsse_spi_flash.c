#include "fu_jim_ftdi_mpsse_spi_flash.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu_jim.h"
#include "fu.h"

static int fu_jim_ftdi_mpsse_spi_flash_status_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	uint8_t status;
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_status");
	if(argc < 1){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_status: variable");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_flash_status(fu, &status) == FU_ERROR)return(JIM_ERR);
	jim_buf2array(jim, (char *)Jim_String(argv[1]), &status, 1, 0);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_write_status_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_write_status");
	if(argc < 1){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_write_status: status");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_flash_write_status(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_jedec_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	uint8_t jedec[3];
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_jedec");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_jedec: variable");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_flash_jedec(fu, jedec) == FU_ERROR)return(JIM_ERR);
	jim_buf2array(jim, (char *)Jim_String(argv[1]), jedec, 3, 0);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_set_address_bytes_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_set_address_bytes");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_set_address_bytes: num_of_bytes");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_flash_set_address_bytes(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_write_extended_address_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_write_extended_address");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_write_extended_address: extended_address");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_flash_write_extended_address(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_erase_write_file_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	int32_t len;
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_erase_write_file");
	if(argc < 4){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_erase_write_file: flash_offset file_offset file [length]");
		return(JIM_ERR);
	}
	if(argc > 4){
		len = strtol(Jim_String(argv[4]), NULL, 0);
	}else{
		len = -1;
	}
	if(fu_ftdi_mpsse_spi_flash_erase_write_file(fu, strtol(Jim_String(argv[1]), NULL, 0), strtol(Jim_String(argv[2]), NULL, 0), (char *)Jim_String(argv[3]), len) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_jim_ftdi_mpsse_spi_flash_read_file_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	int32_t len;
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_flash_erase_write_file");
	if(argc < 4){
		fu_eprintf(fu, "ftdi_mpsse_spi_flash_read_file: flash_offset file_offset file [length]");
		return(JIM_ERR);
	}
	if(argc > 4){
		len = strtol(Jim_String(argv[4]), NULL, 0);
	}else{
		len = -1;
	}
	if(fu_ftdi_mpsse_spi_flash_read_file(fu, strtol(Jim_String(argv[1]), NULL, 0), strtol(Jim_String(argv[2]), NULL, 0), (char *)Jim_String(argv[3]), len) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

int fu_jim_ftdi_mpsse_spi_flash_init(fu_t *fu){
	int ret;
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_status", fu_jim_ftdi_mpsse_spi_flash_status_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_write_status", fu_jim_ftdi_mpsse_spi_flash_write_status_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_jedec", fu_jim_ftdi_mpsse_spi_flash_jedec_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_set_address_bytes", fu_jim_ftdi_mpsse_spi_flash_set_address_bytes_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_write_extended_address", fu_jim_ftdi_mpsse_spi_flash_write_extended_address_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_erase_write_file", fu_jim_ftdi_mpsse_spi_flash_erase_write_file_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_flash_read_file", fu_jim_ftdi_mpsse_spi_flash_read_file_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	return(FU_OK);
}

