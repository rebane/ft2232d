#include "fu_ftdi_mpsse_jim.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"

static int fu_ftdi_mpsse_set_clock_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	uint32_t clock_requested, clock_actual;
	if(argc < 2){
		fu_eprintf(fu, "%s clock", fu->func);
		return(FU_ERROR);
	}
	clock_requested = strtoul(Jim_String(argv[1]), NULL, 0);
	if(fu_ftdi_mpsse_set_clock(fu, clock_requested, &clock_actual) == FU_ERROR)return(FU_ERROR);
	if(clock_actual != clock_requested){
		printf("WARNING: %s: setting clock to %llu Hz (requested %llu Hz)\n", fu->func, (unsigned long long int)clock_actual, (unsigned long long int)clock_requested);
	}else{
		printf("INFO: %s: setting clock to %llu Hz\n", fu->func, (unsigned long long int)clock_actual);
	}
	return(FU_OK);
}

static int fu_ftdi_mpsse_set_clock_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_clock_nosync");
	if(fu_ftdi_mpsse_set_clock_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_clock_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_clock");
	if(fu_ftdi_mpsse_set_clock_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_loopback_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	if(argc < 2){
		fu_eprintf(fu, "%s (1/0)", fu->func);
		return(FU_ERROR);
	}
	if(fu_ftdi_mpsse_set_loopback(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

static int fu_ftdi_mpsse_set_loopback_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_loopback_nosync");
	if(fu_ftdi_mpsse_set_loopback_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_loopback_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_loopback_nosync");
	if(fu_ftdi_mpsse_set_loopback_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_get_bits_low_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_get_bits_low_nosync");
	if(fu_ftdi_mpsse_get_bits_low(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_get_bits_low_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_get_bits_low");
	if(fu_ftdi_mpsse_get_bits_low(fu) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_get_bits_high_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_get_bits_high_nosync");
	if(fu_ftdi_mpsse_get_bits_high(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_get_bits_high_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_get_bits_high");
	if(fu_ftdi_mpsse_get_bits_high(fu) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_bits_low_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	unsigned int data, dir;
	if(argc < 3){
		fu_eprintf(fu, "%s data dir\n", fu->func);
		return(FU_ERROR);
	}
	data = strtoul(Jim_String(argv[1]), NULL, 0);
	dir = strtoul(Jim_String(argv[2]), NULL, 0);
	if(fu_ftdi_mpsse_set_bits_low(fu, data, dir) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

static int fu_ftdi_mpsse_set_bits_low_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_bits_low_nosync");
	if(fu_ftdi_mpsse_set_bits_low_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_bits_low_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_bits_low");
	if(fu_ftdi_mpsse_set_bits_low_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_bits_high_jim(fu_t *fu, int argc, Jim_Obj *const *argv){
	unsigned int data, dir;
	if(argc < 3){
		fu_eprintf(fu, "%s data dir\n", fu->func);
		return(FU_ERROR);
	}
	data = strtoul(Jim_String(argv[1]), NULL, 0);
	dir = strtoul(Jim_String(argv[2]), NULL, 0);
	if(fu_ftdi_mpsse_set_bits_high(fu, data, dir) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

static int fu_ftdi_mpsse_set_bits_high_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_bits_high_nosync");
	if(fu_ftdi_mpsse_set_bits_high_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_set_bits_high_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_set_bits_high");
	if(fu_ftdi_mpsse_set_bits_high_jim(fu, argc, argv) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_transfer_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int count;
	uint8_t flags;
	fu_func(fu, "ftdi_mpsse_transfer_nosync");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_mpsse_transfer_nosync: {flags} count\n");
		return(JIM_ERR);
	}
	flags = fu_ftdi_mpsse_flags_jim(fu, argv[1]);
	count = strtol(Jim_String(argv[2]), NULL, 0);
	if((count < 1) || (count > 65536)){
		fu_eprintf(fu, "count out of range: %d\n", count);
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_transfer_nosync(fu, flags, count) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_transfer_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int count;
	uint8_t flags;
	fu_func(fu, "ftdi_mpsse_transfer");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_mpsse_transfer: {flags} count\n");
		return(JIM_ERR);
	}
	flags = fu_ftdi_mpsse_flags_jim(fu, argv[1]);
	count = strtol(Jim_String(argv[2]), NULL, 0);
	if((count < 1) || (count > 65536)){
		fu_eprintf(fu, "count out of range: %d\n", count);
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_transfer(fu, flags, count) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

uint8_t fu_ftdi_mpsse_flags_jim(fu_t *fu, Jim_Obj *obj){
	int i, l;
	uint8_t flags;
	Jim_Obj *flag;
	char *flag_str;
	flags = 0;
	l = Jim_ListLength((Jim_Interp *)fu->user, obj);
	for(i = 0; i < l; i++){
		Jim_ListIndex((Jim_Interp *)fu->user, obj, i, &flag, JIM_NONE);
		flag_str = (char *)Jim_String(flag);
		if(!strcasecmp(flag_str, "write_neg")){
			flags |= FU_FTDI_MPSSE_FLAG_WRITE_NEG;
		}else if(!strcasecmp(flag_str, "bitmode")){
			flags |= FU_FTDI_MPSSE_FLAG_BITMODE;
		}else if(!strcasecmp(flag_str, "read_neg")){
			flags |= FU_FTDI_MPSSE_FLAG_READ_NEG;
		}else if(!strcasecmp(flag_str, "lsb")){
			flags |= FU_FTDI_MPSSE_FLAG_LSB;
		}else if(!strcasecmp(flag_str, "write")){
			flags |= FU_FTDI_MPSSE_FLAG_DO_WRITE;
		}else if(!strcasecmp(flag_str, "read")){
			flags |= FU_FTDI_MPSSE_FLAG_DO_READ;
		}else if(!strcasecmp(flag_str, "write_tms")){
			flags |= FU_FTDI_MPSSE_FLAG_WRITE_TMS;
		}else{
			fu_eprintf(fu, "unknown flag \"%s\"\n", flag_str);
			return(0);
		}
	}
	return(flags);
}

int fu_ftdi_mpsse_jim_init(fu_t *fu){
	int ret;
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_clock_nosync", fu_ftdi_mpsse_set_clock_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_clock", fu_ftdi_mpsse_set_clock_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_loopback_nosync", fu_ftdi_mpsse_set_loopback_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_loopback", fu_ftdi_mpsse_set_loopback_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_get_bits_low_nosync", fu_ftdi_mpsse_get_bits_low_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_get_bits_low", fu_ftdi_mpsse_get_bits_low_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_get_bits_high_nosync", fu_ftdi_mpsse_get_bits_high_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_get_bits_high", fu_ftdi_mpsse_get_bits_high_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_bits_low_nosync", fu_ftdi_mpsse_set_bits_low_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_bits_low", fu_ftdi_mpsse_set_bits_low_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_bits_high_nosync", fu_ftdi_mpsse_set_bits_high_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_set_bits_high", fu_ftdi_mpsse_set_bits_high_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_transfer_nosync", fu_ftdi_mpsse_transfer_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_transfer", fu_ftdi_mpsse_transfer_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	return(FU_OK);
}

