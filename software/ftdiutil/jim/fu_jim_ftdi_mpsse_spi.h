#ifndef _FU_JIM_FTDI_MPSSE_SPI_H_
#define _FU_JIM_FTDI_MPSSE_SPI_H_

#include "fu_jim.h"
#include "fu.h"

int fu_jim_ftdi_mpsse_spi_init(fu_t *fu);

#endif

