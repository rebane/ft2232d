#include "fu_ftdi_mpsse_pins_jim.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu_jim.h"
#include "fu.h"

static int fu_ftdi_mpsse_pins_parse_jim(fu_t *fu, int argc, Jim_Obj *const *argv, uint16_t *high, uint16_t *low, uint16_t *input){
	int i, l;
	Jim_Obj *name;
	uint16_t pin;
	*high = *low = 0;
	if(input != NULL)*input = 0;
	if(argc > 1){
		l = Jim_ListLength((Jim_Interp *)fu->user, argv[1]);
		for(i = 0; i < l; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, argv[1], i, &name, JIM_NONE);
			pin = fu_ftdi_pin_get(fu, (char *)Jim_String(name));
			if(pin == 0)return(FU_ERROR);
			*high |= pin;
		}
	}
	if(argc > 2){
		l = Jim_ListLength((Jim_Interp *)fu->user, argv[2]);
		for(i = 0; i < l; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, argv[2], i, &name, JIM_NONE);
			pin = fu_ftdi_pin_get(fu, (char *)Jim_String(name));
			if(pin == 0)return(FU_ERROR);
			*low |= pin;
		}
	}
	if((input != NULL) && (argc > 3)){
		l = Jim_ListLength((Jim_Interp *)fu->user, argv[3]);
		for(i = 0; i < l; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, argv[3], i, &name, JIM_NONE);
			pin = fu_ftdi_pin_get(fu, (char *)Jim_String(name));
			if(pin == 0)return(FU_ERROR);
			*input |= pin;
		}
	}
	return(FU_OK);
}

static int fu_ftdi_mpsse_pins_init_norefresh_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_init_norefresh");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set_norefresh(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_init_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_init_nosync");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set_nosync(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_init_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_init");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_set_norefresh_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_set_norefresh");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set_norefresh(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_set_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_set_nosync");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set_nosync(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_set_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, others in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low;
	fu_func(fu, "ftdi_mpsse_pins_set");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, NULL) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_set(fu, high, low) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_merge_norefresh_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low, input;
	fu_func(fu, "ftdi_mpsse_pins_merge_norefresh");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, &input) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_merge_norefresh(fu, high, low, input) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_merge_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low, input;
	fu_func(fu, "ftdi_mpsse_pins_merge_nosync");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, &input) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_merge_nosync(fu, high, low, input) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_merge_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){ // hi, lo, in
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t high, low, input;
	fu_func(fu, "ftdi_mpsse_pins_merge");
	if(fu_ftdi_mpsse_pins_parse_jim(fu, argc, argv, &high, &low, &input) == FU_ERROR)return(JIM_ERR);
	if(fu_ftdi_mpsse_pins_merge(fu, high, low, input) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_refresh_nosync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_pins_refresh_nosync");
	if(fu_ftdi_mpsse_pins_refresh_nosync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_refresh_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_pins_refresh");
	if(fu_ftdi_mpsse_pins_refresh(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_sync_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_pins_sync");
	if(fu_ftdi_mpsse_pins_sync(fu) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

static int fu_ftdi_mpsse_pins_get_jim_command(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint16_t pin, pins;
	fu_func(fu, "ftdi_mpsse_pins_get");
	if(argc < 3){
		fu_eprintf(fu, "ftdi_mpsse_pins_get variable pin");
		return(JIM_ERR);
	}
	pin = fu_ftdi_pin_get(fu, (char *)Jim_String(argv[2]));
	if(pin <= 255){
		if(fu_ftdi_mpsse_pins_get_low(fu, &pins) == FU_ERROR)return(JIM_ERR);
	}else{
		if(fu_ftdi_mpsse_pins_get_high(fu, &pins) == FU_ERROR)return(JIM_ERR);
	}
	if(pins & pin){
		jim_int2variable(jim, (char *)Jim_String(argv[1]), 1, "ftdi_mpsse_pins_get");
	}else{
		jim_int2variable(jim, (char *)Jim_String(argv[1]), 0, "ftdi_mpsse_pins_get");
	}
	return(JIM_OK);
}

int fu_ftdi_mpsse_pins_jim_init(fu_t *fu){
	int ret;
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_init_norefresh", fu_ftdi_mpsse_pins_init_norefresh_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_init_nosync", fu_ftdi_mpsse_pins_init_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_init", fu_ftdi_mpsse_pins_init_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_set_norefresh", fu_ftdi_mpsse_pins_set_norefresh_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_set_nosync", fu_ftdi_mpsse_pins_set_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_set", fu_ftdi_mpsse_pins_set_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_merge_norefresh", fu_ftdi_mpsse_pins_merge_norefresh_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_merge_nosync", fu_ftdi_mpsse_pins_merge_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_merge", fu_ftdi_mpsse_pins_merge_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_refresh_nosync", fu_ftdi_mpsse_pins_refresh_nosync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_refresh", fu_ftdi_mpsse_pins_refresh_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_sync", fu_ftdi_mpsse_pins_sync_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_pins_get", fu_ftdi_mpsse_pins_get_jim_command, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	return(FU_OK);
}

