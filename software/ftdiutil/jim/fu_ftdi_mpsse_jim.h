#ifndef _FU_FTDI_MPSSE_JIM_H_
#define _FU_FTDI_MPSSE_JIM_H_

#include <stdint.h>
#include <jim.h>
#include "fu.h"

int fu_ftdi_mpsse_jim_init(fu_t *fu);
uint8_t fu_ftdi_mpsse_flags_jim(fu_t *fu, Jim_Obj *obj);

#endif

