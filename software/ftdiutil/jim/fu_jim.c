#include "fu_jim.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libgen.h>
#include <jim.h>
#include "fu.h"

int fu_jim_source(fu_jim_t *fj, char *filename){
	int r, l, fd;
	char buffer[512];
	fu_func(fj->fu, "jim_source");
	fd = open(filename, O_RDONLY);
	if(fd < 0){
		l = readlink("/proc/self/exe", buffer, 512);
		buffer[l] = 0;
		strcpy(buffer, dirname(buffer));
		strcat(buffer, "/");
		strcat(buffer, filename);
		fd = open(buffer, O_RDONLY);
	}else{
		strcpy(buffer, filename);
	}
	if(fd >= 0){
		close(fd);
		r = Jim_EvalFile(fj->jim, buffer);
		if(r != JIM_OK){
			fu_eprintf(fj->fu, "%s", Jim_GetString(Jim_GetResult(fj->jim), NULL));
		}
	}else{
		fu_eprintf(fj->fu, "couldn't read file \"%s\"", buffer);
	}
	return(FU_OK);
}

int fu_jim_command(fu_jim_t *fj, char *command){
	int r;
	fu_func(fj->fu, "jim_command");
	r = Jim_Eval(fj->jim, command);
	if(r != JIM_OK){
		fu_eprintf(fj->fu, "%s", Jim_GetString(Jim_GetResult(fj->jim), NULL));
	}
	return(FU_OK);
}

int fu_jim_nop_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	return(JIM_OK);
}

static int fu_jim_adapter_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	const char *subcmd;
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "adapter");
	if(argc < 2){
		fu_eprintf(fu, "adapter subcommand (args)");
		return(JIM_ERR);
	}
	subcmd = Jim_String(argv[1]);
	if(!strcmp(subcmd, "serial")){
		if(argc < 3){
			fu_eprintf(fu, "adapter serial serial_string");
			return(JIM_ERR);
		}
		if(fu_ftdi_serial(fu, Jim_String(argv[2])) == FU_ERROR)return(JIM_ERR);
		return(JIM_OK);
	}
	return(JIM_OK);
}


static int fu_jim_echo_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	int i, j, nl, eol;
	Jim_Obj *name, *value;
	nl = eol = 1;
	for(i = 1; i < argc; i++){
		if(Jim_IsList(argv[i])){
			if(!nl)printf("\n");
			printf("LIST:");
			for(j = 0; j < Jim_ListLength(jim, argv[i]); j += 2){
				Jim_ListIndex(jim, argv[i], j, &name, JIM_NONE);
				Jim_ListIndex(jim, argv[i], j + 1, &value, JIM_NONE);
				printf(" (%s, %s)", Jim_String(name), Jim_String(value));
			}
			printf("\n");
			nl = 1;
		}else{
			if((i == 1) && !strcmp((char *)Jim_String(argv[i]), "-n")){
				eol = 0;
			}else{
				if(!nl)printf(" ");
				printf("%s", Jim_String(argv[i]));
				nl = 0;
			}
		}
	}
	if((!nl || (argc == 1)) && eol)printf("\n");
	fflush(stdout);
	return(JIM_OK);
}

static int fu_jim_error_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	if(argc > 1){
		fu_eprintf(fu, "%s", Jim_String(argv[1]));
	}else{
		fu_eprintf(fu, "");
	}
	return(JIM_OK);
}

static int fu_jim_shutdown_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	if(fu->error_fatal)exit(0);
	return(JIM_OK);
}

void jim_buf2array(Jim_Interp *jim, char *variable, void *buf, int count, int start_index){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	int i;
	char str[32];
	Jim_Obj *array, *key, *value;

	if (!strcmp(variable, "ECHO")) {
		printf("%s:", fu->func);
		for (i = 0; i < count; i++)
			printf(" %02X", (unsigned int)((uint8_t *)buf)[i]);

		printf("\n");

		return;
	}

	array = Jim_GetVariableStr(jim, variable, 0);
	if(array == NULL){
		fu_eprintf(fu, "no such variable \"%s\"", variable);
	}
	for(i = 0; i < count; i++){
		snprintf(str, 32, "%d", (start_index + i));
		str[31] = 0;
		key = Jim_NewStringObj(jim, str, -1);
		if(key == NULL){
			fu_eprintf(fu, "%s");
		}
		snprintf(str, 32, "0x%02X", (unsigned int)((uint8_t *)buf)[i]);
		str[31] = 0;
		value = Jim_NewStringObj(jim, str, -1);
		if(value == NULL){
			fu_eprintf(fu, "%s");
		}
		Jim_ListAppendElement(jim, array, key);
		Jim_ListAppendElement(jim, array, value);
	}
}

void jim_uchar2variable(Jim_Interp *jim, char *variable, uint8_t c, char *func){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	Jim_Obj *value;
	char str[8];
	snprintf(str, 8, "0x%02X", (unsigned int)c);
	str[4] = 0;
	value = Jim_NewStringObj(jim, str, -1);
	if(value == NULL){
		fu_eprintf(fu, "%s", func);
	}
	if(Jim_SetVariableStr(jim, variable, value) != JIM_OK){
		fu_eprintf(fu, "%s: %s", func, Jim_GetString(Jim_GetResult(jim), NULL));
	}
}

void jim_int2variable(Jim_Interp *jim, char *variable, int i, char *func){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	Jim_Obj *value;
	char str[32];
	snprintf(str, 32, "%d", i);
	str[31] = 0;
	value = Jim_NewStringObj(jim, str, -1);
	if(value == NULL){
		fu_eprintf(fu, "%s", func);
	}
	if(Jim_SetVariableStr(jim, variable, value) != JIM_OK){
		fu_eprintf(fu, "%s: %s", func, Jim_GetString(Jim_GetResult(jim), NULL));
	}
}

int fu_jim_init(fu_jim_t *fj, fu_t *fu, void *user){
	int ret;

	fj->fu = fu;
	fj->user = user;
	fj->jim = Jim_CreateInterp();
	fu->user = fj->jim;
	Jim_RegisterCoreCommands(fj->jim);
	Jim_InitStaticExtensions(fj->jim);

	ret = Jim_CreateCommand(fj->jim, "interface", fu_jim_nop_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand(fj->jim, "adapter", fu_jim_adapter_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand(fj->jim, "echo", fu_jim_echo_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand(fj->jim, "error", fu_jim_error_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	ret = Jim_CreateCommand(fj->jim, "shutdown", fu_jim_shutdown_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);

	fu_ftdi_jim_init(fu);
	fu_ftdi_mpsse_jim_init(fu);
	fu_ftdi_mpsse_pins_jim_init(fu);
	fu_jim_ftdi_mpsse_spi_init(fu);
	fu_jim_ftdi_mpsse_spi_flash_init(fu);
	fu_jim_ftdi_mpsse_i2c_init(fu);

	return(FU_OK);
}

