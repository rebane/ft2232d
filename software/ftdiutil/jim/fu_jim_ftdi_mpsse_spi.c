#include "fu_jim_ftdi_mpsse_spi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu_jim.h"
#include "fu.h"

static int fu_jim_ftdi_mpsse_spi_cs_pin_jc(Jim_Interp *jim, int argc, Jim_Obj *const *argv){
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	fu_func(fu, "ftdi_mpsse_spi_cs_pin");
	if(argc < 2){
		fu_eprintf(fu, "ftdi_mpsse_spi_cs_pin: variable");
		return(JIM_ERR);
	}
	if(fu_ftdi_mpsse_spi_cs_pin(fu, (char *)Jim_String(argv[1])) == FU_ERROR)return(JIM_ERR);
	return(JIM_OK);
}

int fu_jim_ftdi_mpsse_spi_init(fu_t *fu){
	int ret;
	ret = Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_spi_cs_pin", fu_jim_ftdi_mpsse_spi_cs_pin_jc, fu, NULL);
	if(ret != JIM_OK)return(FU_ERROR);
	return(FU_OK);
}

