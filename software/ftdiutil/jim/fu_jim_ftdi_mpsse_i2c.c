#include "fu_jim_ftdi_mpsse_i2c.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <jim.h>
#include "fu_jim.h"
#include "fu.h"

static int fu_ftdi_mpsse_i2c_init_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;

	fu_func(fu, "ftdi_mpsse_i2c_init");

	if (fu_ftdi_mpsse_i2c_init(fu) == FU_ERROR)
		return JIM_ERR;

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_start_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;

	fu_func(fu, "ftdi_mpsse_i2c_start");

	if (fu_ftdi_mpsse_i2c_start(fu) == FU_ERROR)
		return JIM_ERR;

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_read_bit_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint8_t bit;

	fu_func(fu, "ftdi_mpsse_i2c_read_bit");

	if (argc < 2) {
		fu_eprintf(fu, "%s: variable", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_read_bit(fu, &bit) == FU_ERROR)
		return JIM_ERR;

	jim_buf2array(jim, (char *)Jim_String(argv[1]), &bit, 1, 0);

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_write_bit_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;

	fu_func(fu, "ftdi_mpsse_i2c_write_bit");

	if (argc < 2) {
		fu_eprintf(fu, "%s (1/0)", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_write_bit(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)
		return JIM_ERR;

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_stop_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;

	fu_func(fu, "ftdi_mpsse_i2c_stop");

	if (fu_ftdi_mpsse_i2c_stop(fu) == FU_ERROR)
		return JIM_ERR;

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_read_byte_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint8_t byte;

	fu_func(fu, "ftdi_mpsse_i2c_read_byte");

	if (argc < 3) {
		fu_eprintf(fu, "%s: variable ack", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_read_byte(fu, strtol(Jim_String(argv[2]), NULL, 0), &byte) == FU_ERROR)
		return JIM_ERR;

	jim_buf2array(jim, (char *)Jim_String(argv[1]), &byte, 1, 0);

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_write_byte_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;

	fu_func(fu, "ftdi_mpsse_i2c_write_byte");

	if (argc < 2) {
		fu_eprintf(fu, "%s: byte", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_write_byte(fu, strtol(Jim_String(argv[1]), NULL, 0)) == FU_ERROR)
		return JIM_ERR;

	return JIM_OK;
}

static int fu_ftdi_mpsse_i2c_read_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint8_t *buf;
	int count;

	fu_func(fu, "ftdi_mpsse_i2c_read");

	if (argc < 4) {
		fu_eprintf(fu, "%s: i2c_address variable count", fu->func);
		return JIM_ERR;
	}

	count = strtoul(Jim_String(argv[3]), NULL, 0);
	buf = malloc(count);
	if (buf == NULL) {
		fu_eprintf(fu, "%s: out of memory", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_read(fu, strtol(Jim_String(argv[1]), NULL, 0), buf, count) == FU_ERROR) {
		free(buf);
		return JIM_ERR;
	}

	jim_buf2array(jim, (char *)Jim_String(argv[2]), buf, count, 0);

	free(buf);

	return FU_OK;
}

static int fu_ftdi_mpsse_i2c_write_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	Jim_Obj *value;
	int i, count;
	uint8_t *buf;

	fu_func(fu, "ftdi_mpsse_i2c_write");

	if (argc < 3) {
		fu_eprintf(fu, "%s: i2c_address data", fu->func);
		return JIM_ERR;
	}

	if (Jim_ListLength((Jim_Interp *)fu->user, argv[2]) > 1) {
		count = Jim_ListLength((Jim_Interp *)fu->user, argv[2]);
		buf = malloc(count);
		if (buf == NULL) {
			fu_eprintf(fu, "%s: out of memory", fu->func);
			return JIM_ERR;
		}
		for (i = 0; i < count; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, argv[2], i, &value, JIM_NONE);
			buf[i] = strtoul(Jim_String(value), NULL, 0);
			printf("F: %02X\n", (unsigned int)buf[i]);
		}
	} else {
		count = argc - 2;
		buf = malloc(count);
		if (buf == NULL) {
			fu_eprintf(fu, "%s: out of memory", fu->func);
			return JIM_ERR;
		}
		for (i = 0; i < count; i++)
			buf[i] = strtoul(Jim_String(argv[2 + i]), NULL, 0);
	}

	if (fu_ftdi_mpsse_i2c_write(fu, strtol(Jim_String(argv[1]), NULL, 0), buf, count) == FU_ERROR)
		return JIM_ERR;

	return FU_OK;
}

static int fu_ftdi_mpsse_i2c_read_smb_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	uint8_t *buf;
	int count;

	fu_func(fu, "ftdi_mpsse_i2c_read_smb");

	if (argc < 5) {
		fu_eprintf(fu, "%s: i2c_address address variable count", fu->func);
		return JIM_ERR;
	}

	count = strtoul(Jim_String(argv[4]), NULL, 0);
	buf = malloc(count);
	if (buf == NULL) {
		fu_eprintf(fu, "%s: out of memory", fu->func);
		return JIM_ERR;
	}

	if (fu_ftdi_mpsse_i2c_read_smb(fu, strtol(Jim_String(argv[1]), NULL, 0), strtol(Jim_String(argv[2]), NULL, 0), buf, count) == FU_ERROR) {
		free(buf);
		return JIM_ERR;
	}

	jim_buf2array(jim, (char *)Jim_String(argv[3]), buf, count, 0);

	free(buf);

	return FU_OK;
}

static int fu_ftdi_mpsse_i2c_write_smb_jim(Jim_Interp *jim, int argc, Jim_Obj *const *argv)
{
	fu_t *fu = (fu_t *)jim->cmdPrivData;
	Jim_Obj *value;
	int i, count;
	uint8_t *buf;

	fu_func(fu, "ftdi_mpsse_i2c_write_smb");

	if (argc < 4) {
		fu_eprintf(fu, "%s: i2c_address address data", fu->func);
		return JIM_ERR;
	}

	if (Jim_ListLength((Jim_Interp *)fu->user, argv[3]) > 1) {
		count = Jim_ListLength((Jim_Interp *)fu->user, argv[3]);
		buf = malloc(count);
		if (buf == NULL) {
			fu_eprintf(fu, "%s: out of memory", fu->func);
			return JIM_ERR;
		}
		for (i = 0; i < count; i++){
			Jim_ListIndex((Jim_Interp *)fu->user, argv[3], i, &value, JIM_NONE);
			buf[i] = strtoul(Jim_String(value), NULL, 0);
		}
	} else {
		count = argc - 3;
		buf = malloc(count);
		if (buf == NULL) {
			fu_eprintf(fu, "%s: out of memory", fu->func);
			return JIM_ERR;
		}
		for (i = 0; i < count; i++)
			buf[i] = strtoul(Jim_String(argv[3 + i]), NULL, 0);
	}

	if (fu_ftdi_mpsse_i2c_write_smb(fu, strtol(Jim_String(argv[1]), NULL, 0), strtol(Jim_String(argv[2]), NULL, 0), buf, count) == FU_ERROR)
		return JIM_ERR;

	return FU_OK;
}

int fu_jim_ftdi_mpsse_i2c_init(fu_t *fu)
{
	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_init", fu_ftdi_mpsse_i2c_init_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_start", fu_ftdi_mpsse_i2c_start_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_read_bit", fu_ftdi_mpsse_i2c_read_bit_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_write_bit", fu_ftdi_mpsse_i2c_write_bit_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_stop", fu_ftdi_mpsse_i2c_stop_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_read_byte", fu_ftdi_mpsse_i2c_read_byte_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_write_byte", fu_ftdi_mpsse_i2c_write_byte_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_read", fu_ftdi_mpsse_i2c_read_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_write", fu_ftdi_mpsse_i2c_write_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_read_smb", fu_ftdi_mpsse_i2c_read_smb_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	if (Jim_CreateCommand((Jim_Interp *)fu->user, "ftdi_mpsse_i2c_write_smb", fu_ftdi_mpsse_i2c_write_smb_jim, fu, NULL) != JIM_OK)
		return FU_ERROR;

	return FU_OK;
}

