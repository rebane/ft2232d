#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include "fu.h"
#include "spi.h"

int main(){
	fu_t fu;
	uint8_t buffer[256];
//	spi_init(&fu, "54aa0699-6e9b-4262-b15d-9270dd565b15"); // 16
	spi_init(&fu, "66c62282-230e-43f7-b20d-55faef6c5e4e"); // 18
	// rs/dc - ad6

	while(1){
		fu_ftdi_mpsse_pins_merge(&fu, fu_ftdi_pin_get(&fu, "ad6"), 0, 0);
		usleep(1000000);
		fu_ftdi_mpsse_pins_merge(&fu, 0, fu_ftdi_pin_get(&fu, "ad6"), 0);
		usleep(1000000);
	}

	spi_transfer_half(&fu, "\x20\x64", 2, &buffer[0], 2);
	spi_transfer_half(&fu, "\x20\x62", 2, &buffer[2], 2);
	spi_transfer_half(&fu, "\x20\x60", 2, &buffer[4], 2);
	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", (unsigned int)buffer[0], (unsigned int)buffer[1], (unsigned int)buffer[2], (unsigned int)buffer[3], (unsigned int)buffer[4], (unsigned int)buffer[5]);

//	fu_ftdi_mpsse_pins_set(&fu, 0, fu_ftdi_pin_get(&fu, "rst"));
//	usleep(100000);
//	fu_ftdi_mpsse_pins_set(&fu, fu_ftdi_pin_get(&fu, "rst"), 0);
}

