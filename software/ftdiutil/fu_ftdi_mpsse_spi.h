#ifndef _FU_FTDI_MPSSE_SPI_H_
#define _FU_FTDI_MPSSE_SPI_H_

#include "fu.h"

int fu_ftdi_mpsse_spi_init(fu_t *fu);
int fu_ftdi_mpsse_spi_test_init(fu_t *fu);
int fu_ftdi_mpsse_spi_cs_pin(fu_t *fu, char *pin);

int fu_ftdi_mpsse_spi_transfer_half_nosync(fu_t *fu, uint8_t mpsse_flags_out, uint8_t mpsse_flags_in, void *out, int out_len, int in_len);
int fu_ftdi_mpsse_spi_transfer_half(fu_t *fu, uint8_t mpsse_flags_out, uint8_t mpsse_flags_in, void *out, int out_len, int in_len);

int fu_ftdi_mpsse_spi_transfer_full_nosync(fu_t *fu, uint8_t mpsse_flags, void *out, int len);
int fu_ftdi_mpsse_spi_transfer_full(fu_t *fu, uint8_t mpsse_flags, void *out, int len);

#endif

