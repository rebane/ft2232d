#include <stdio.h>
#include <unistd.h>
#include "fu.h"
#include "fu_jim.h"

int main(int argc, char *argv[]){
	int c;
	fu_t fu;
	fu_jim_t fj;

	fu_init(&fu, NULL);
	fu_jim_init(&fj, &fu, NULL);

	opterr = 0;
	while((c = getopt(argc, argv, "hf:c:")) != -1){
		if(c == 'h'){
			printf("help\n");
			return(0);
		}else if(c == 'f'){
			if(fu_jim_source(&fj, optarg))return(1);
		}else if(c == 'c'){
			if(fu_jim_command(&fj, optarg))return(1);
		}
	}

	if(fu_ftdi_close(&fu))return(1);
	return(0);
}

