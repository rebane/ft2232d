#define _GNU_SOURCE
#include "fu.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int fu_init(fu_t *fu, void *user){
	fu->func = NULL;
	fu->error = NULL;
	fu->error_print = 1;
	fu->error_fatal = 1;
	fu->user = user;
	if(fu_ftdi_init(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

void fu_func(fu_t *fu, char *func){
	if(fu->func != NULL){
		free(fu->func);
		fu->func = NULL;
	}
	fu->func = strdup(func);
}

void fu_eprintf(fu_t *fu, char *fmt, ...){
	va_list args;
	if(fu->error != NULL){
		free(fu->error);
		fu->error = NULL;
	}
	va_start(args, fmt);
	vasprintf(&fu->error, fmt, args);
	va_end(args);
	if(fu->error_print){
		if(fu->func != NULL){
			fprintf(stderr, "error: %s: %s\n", fu->func, fu->error);
		}else{
			fprintf(stderr, "error: %s\n", fu->error);
		}
	}
	if(fu->error_fatal){
		exit(1);
	}
}

