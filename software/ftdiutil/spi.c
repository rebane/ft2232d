#include "spi.h"
#include <stdio.h>
#include "fu.h"

void spi_init(fu_t *fu, char *serial)
{
	fu_init(fu, NULL);
	fu_ftdi_serial(fu, serial);
	fu_ftdi_channel(fu, FU_FTDI_CHANNEL_0);
	fu_ftdi_open(fu);
	fu_ftdi_set_bitmode(fu, FU_FTDI_BITMODE_MPSSE, 0);

	fu_ftdi_mpsse_pins_set(fu, fu_ftdi_pin_get(fu, "cs"), fu_ftdi_pin_get(fu, "sck") | fu_ftdi_pin_get(fu, "mosi"));

	fu_ftdi_mpsse_set_loopback(fu, 0);
	fu_ftdi_sync(fu);
}

void spi_transfer_half(fu_t *fu, void *out, int out_len, void *in, int in_len)
{
	fu_ftdi_mpsse_pins_merge_nosync(fu, 0, fu_ftdi_pin_get(fu, "cs"), 0);
	fu_ftdi_mpsse_spi_transfer_half_nosync(fu, FU_FTDI_MPSSE_FLAG_WRITE_NEG, 0, out, out_len, in_len);
	fu_ftdi_mpsse_pins_merge_nosync(fu, fu_ftdi_pin_get(fu, "cs"), 0, 0);
	fu_ftdi_sync(fu);
	fu_ftdi_read(fu, in, in_len);
}

void spi_cs(fu_t *fu, int active)
{
	if (active)
		fu_ftdi_mpsse_pins_merge(fu, 0, fu_ftdi_pin_get(fu, "cs"), 0);
	else
		fu_ftdi_mpsse_pins_merge(fu, fu_ftdi_pin_get(fu, "cs"), 0, 0);
}

void spi_transfer(fu_t *fu, void *out, void *in, int len)
{
	fu_ftdi_mpsse_spi_transfer_full(fu, FU_FTDI_MPSSE_FLAG_WRITE_NEG, out, len);
	fu_ftdi_read(fu, in, len);
}

