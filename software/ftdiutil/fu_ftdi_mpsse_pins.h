#ifndef _FU_FTDI_MPSSE_PINS_H_
#define _FU_FTDI_MPSSE_PINS_H_

#include <stdint.h>
#include "fu.h"

int fu_ftdi_mpsse_pins_init(fu_t *fu);
int fu_ftdi_mpsse_pins_test_init(fu_t *fu);

int fu_ftdi_mpsse_pins_set_norefresh(fu_t *fu, uint16_t high, uint16_t low);
int fu_ftdi_mpsse_pins_set_nosync(fu_t *fu, uint16_t high, uint16_t low);
int fu_ftdi_mpsse_pins_set(fu_t *fu, uint16_t high, uint16_t low);

int fu_ftdi_mpsse_pins_merge_norefresh(fu_t *fu, uint16_t high, uint16_t low, uint16_t input);
int fu_ftdi_mpsse_pins_merge_nosync(fu_t *fu, uint16_t high, uint16_t low, uint16_t input);
int fu_ftdi_mpsse_pins_merge(fu_t *fu, uint16_t high, uint16_t low, uint16_t input);

int fu_ftdi_mpsse_pins_refresh_nosync(fu_t *fu);
int fu_ftdi_mpsse_pins_refresh(fu_t *fu);

int fu_ftdi_mpsse_pins_sync(fu_t *fu);

int fu_ftdi_mpsse_pins_get_low(fu_t *fu, uint16_t *pins);
int fu_ftdi_mpsse_pins_get_high(fu_t *fu, uint16_t *pins);

#endif

