#ifndef _FU_H_
#define _FU_H_

#include <stdarg.h>
#include <stdint.h>

typedef struct fu_struct fu_t;

#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"
#include "fu_ftdi_mpsse_pins.h"
#include "fu_ftdi_mpsse_spi.h"
#include "fu_ftdi_mpsse_spi_flash.h"
#include "fu_ftdi_mpsse_i2c.h"

#define FU_OK    (0)
#define FU_ERROR (-1)

struct fu_struct{
	struct{
		int debug;
		char *serial;
		char *desc;
		uint16_t vid;
		uint16_t pid;
		int channel;
		struct ftdi_context *ftdi;
		int bitmode;
		int rebind_on_exit;
		unsigned int buffer_len;
		unsigned int buffer_loc;
		uint8_t *buffer;
	}ftdi;
	struct{
	}ftdi_mpsse;
	struct{
		uint16_t dir;
		uint16_t data;
		uint16_t dir_wanted;
		uint16_t data_wanted;
		int refresh_needed;
		int sync_needed;
		int ready;
	}ftdi_mpsse_pins;
	struct{
		uint16_t cs_low;
		uint16_t cs_high;
		uint16_t write_flags;
		uint16_t read_flags;
		int address_bytes;
	}ftdi_mpsse_spi_flash;
	struct{
		uint16_t scl;
		uint16_t sda_out;
		uint16_t sda_in;
	}ftdi_mpsse_i2c;
	char *func;
	char *error;
	int error_print;
	int error_fatal;
	void *user;
};

int fu_init(fu_t *fu, void *user);
int fu_source(fu_t *fu, char *filename);
int fu_command(fu_t *fu, char *command);

void fu_func(fu_t *fu, char *func);
void fu_eprintf(fu_t *fu, char *fmt, ...);

#endif

