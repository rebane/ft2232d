#include "fu_ftdi_mpsse_spi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"
#include "fu_ftdi_mpsse_spi_flash.h"

static char *fu_ftdi_mpsse_spi_cs = NULL;

int fu_ftdi_mpsse_spi_init(fu_t *fu){
	if(fu_ftdi_mpsse_spi_flash_init(fu, fu_ftdi_mpsse_spi_cs) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_test_init(fu_t *fu){
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_cs_pin(fu_t *fu, char *pin){
	if (fu_ftdi_mpsse_spi_cs != NULL)
		free(fu_ftdi_mpsse_spi_cs);

	fu_ftdi_mpsse_spi_cs = strdup(pin);
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_transfer_half_nosync(fu_t *fu, uint8_t mpsse_flags_out, uint8_t mpsse_flags_in, void *out, int out_len, int in_len){
	if(out_len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_half_nosync: unsupported transfer size: %d", out_len);
		return(FU_ERROR);
	}
	if(in_len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_half_nosync: unsupported transfer size: %d", in_len);
		return(FU_ERROR);
	}
	if(out_len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags_out | FU_FTDI_MPSSE_FLAG_DO_WRITE, out_len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_write_nosync(fu, out, out_len) == FU_ERROR)return(FU_ERROR);
	}
	if(in_len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags_in | FU_FTDI_MPSSE_FLAG_DO_READ, in_len) == FU_ERROR)return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_transfer_half(fu_t *fu, uint8_t mpsse_flags_out, uint8_t mpsse_flags_in, void *out, int out_len, int in_len){
	if(out_len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_half: unsupported transfer size: %d", out_len);
		return(FU_ERROR);
	}
	if(in_len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_half: unsupported transfer size: %d", in_len);
		return(FU_ERROR);
	}
	if(out_len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags_out | FU_FTDI_MPSSE_FLAG_DO_WRITE, out_len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_write_nosync(fu, out, out_len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	}
	if(in_len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags_in | FU_FTDI_MPSSE_FLAG_DO_READ, in_len) == FU_ERROR)return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_transfer_full_nosync(fu_t *fu, uint8_t mpsse_flags, void *out, int len){
	if(len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_full_nosync: unsupported transfer size: %d", len);
		return(FU_ERROR);
	}
	if(len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags | FU_FTDI_MPSSE_FLAG_DO_WRITE | FU_FTDI_MPSSE_FLAG_DO_READ, len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_write_nosync(fu, out, len) == FU_ERROR)return(FU_ERROR);
	}
	return(FU_OK);
}

int fu_ftdi_mpsse_spi_transfer_full(fu_t *fu, uint8_t mpsse_flags, void *out, int len){
	if(len > 65536){
		fu_eprintf(fu, "ftdi_mpsse_spi_transfer_full: unsupported transfer size: %d", len);
		return(FU_ERROR);
	}
	if(len){
		if(fu_ftdi_mpsse_transfer_nosync(fu, mpsse_flags | FU_FTDI_MPSSE_FLAG_DO_WRITE | FU_FTDI_MPSSE_FLAG_DO_READ, len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_write_nosync(fu, out, len) == FU_ERROR)return(FU_ERROR);
		if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	}
	return(FU_OK);
}

