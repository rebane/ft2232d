#ifndef _SPI_H_
#define _SPI_H_

#include "fu.h"

void spi_init(fu_t *fu, char *serial);
void spi_transfer_half(fu_t *fu, void *out, int out_len, void *in, int in_len);
void spi_cs(fu_t *fu, int active);
void spi_transfer(fu_t *fu, void *out, void *in, int len);

#endif

