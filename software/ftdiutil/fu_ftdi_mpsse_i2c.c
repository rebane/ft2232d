#include "fu_ftdi_mpsse_i2c.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse.h"
#include "fu_ftdi_mpsse_pins.h"

int fu_ftdi_mpsse_i2c_init(fu_t *fu)
{
	fu->ftdi_mpsse_i2c.scl = fu_ftdi_pin_get(fu, "tck");
	fu->ftdi_mpsse_i2c.sda_out = fu_ftdi_pin_get(fu, "tdi");
	fu->ftdi_mpsse_i2c.sda_in = fu_ftdi_pin_get(fu, "tdo");
	fu_ftdi_mpsse_pins_merge(fu, fu->ftdi_mpsse_i2c.scl | fu->ftdi_mpsse_i2c.sda_out, 0, fu->ftdi_mpsse_i2c.sda_in);
	return FU_OK;
}

int fu_ftdi_mpsse_i2c_test_init(fu_t *fu)
{
	return FU_OK;
}

void fu_ftdi_mpsse_i2c_delay()
{
	usleep(1);
}

void fu_ftdi_mpsse_i2c_scl(fu_t *fu, int on)
{
	if (on)
		fu_ftdi_mpsse_pins_merge(fu, fu->ftdi_mpsse_i2c.scl, 0, fu->ftdi_mpsse_i2c.sda_in);
	else
		fu_ftdi_mpsse_pins_merge(fu, 0, fu->ftdi_mpsse_i2c.scl, fu->ftdi_mpsse_i2c.sda_in);
}

void fu_ftdi_mpsse_i2c_sda(fu_t *fu, int on)
{
	if (on)
		fu_ftdi_mpsse_pins_merge(fu, fu->ftdi_mpsse_i2c.sda_out, 0, fu->ftdi_mpsse_i2c.sda_in);
	else
		fu_ftdi_mpsse_pins_merge(fu, 0, fu->ftdi_mpsse_i2c.sda_out, fu->ftdi_mpsse_i2c.sda_in);
}

/*int fu_ftdi_mpsse_i2c_reset(fu_t *fu)
	uint16_t i;
	int16_t err;
	i2c_bitbang->scl_set(1);
	i2c_bitbang->sda_set(1);
	for(i = 0; i < 24; i++){
		if(i2c_bitbang->sda())break;
		i2c_bitbang->scl_set(0);
		i2c_bitbang->delay();
		i2c_bitbang->scl_set(1);
		i2c_bitbang->delay();
	}
	err = i2c_bitbang_start(i2c_bitbang);
	if(err < 0){
		i2c_bitbang_stop(i2c_bitbang);
		return(err);
	}
	err = i2c_bitbang_stop(i2c_bitbang);
	if(err < 0)return(err);
	return(0);
}*/

int fu_ftdi_mpsse_i2c_start(fu_t *fu)
{
	fu_ftdi_mpsse_i2c_sda(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_sda(fu, 0);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 0);
	fu_ftdi_mpsse_i2c_delay();

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_read_bit(fu_t *fu, uint8_t *bit)
{
	uint16_t pins = 0;

	fu_ftdi_mpsse_i2c_sda(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_pins_get_low(fu, &pins);
	*bit = !!(pins & fu->ftdi_mpsse_i2c.sda_in);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 0);
	fu_ftdi_mpsse_i2c_delay();

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_write_bit(fu_t *fu, uint8_t bit)
{
	fu_ftdi_mpsse_i2c_sda(fu, bit);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 0);
	fu_ftdi_mpsse_i2c_delay();

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_stop(fu_t *fu)
{
	fu_ftdi_mpsse_i2c_sda(fu, 0);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_sda(fu, 1);
	fu_ftdi_mpsse_i2c_delay();
	fu_ftdi_mpsse_i2c_scl(fu, 0);
	fu_ftdi_mpsse_i2c_delay();

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_read_byte(fu_t *fu, int ack, uint8_t *byte)
{
	uint8_t bit;
	int i;

	*byte = 0;
	for (i = 0; i < 8; i++) {
		fu_ftdi_mpsse_i2c_read_bit(fu, &bit);
		if (bit)
			*byte |= (0x80 >> i);
	}

	if (ack)
		fu_ftdi_mpsse_i2c_write_bit(fu, 0);
	else
		fu_ftdi_mpsse_i2c_write_bit(fu, 1);

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_write_byte(fu_t *fu, uint8_t byte)
{
	uint8_t bit;
	int i;

	for (i = 0; i < 8; i++)
		fu_ftdi_mpsse_i2c_write_bit(fu, byte & (0x80 >> i));

	fu_ftdi_mpsse_i2c_read_bit(fu, &bit);
	if (bit) {
		printf("NO ACK\n");
		return FU_ERROR;
	}

	return FU_OK;
}

int fu_ftdi_mpsse_i2c_read(fu_t *fu, uint8_t i2c_address, uint8_t *bytes, int len)
{
	int i;

	if (fu_ftdi_mpsse_i2c_start(fu) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, (i2c_address << 1) | 0x01) != FU_OK)
		return FU_ERROR;

	for (i = 0; i < len; i++) {
		if ((i + 1) == len) {
			if (fu_ftdi_mpsse_i2c_read_byte(fu, 0, &bytes[i]) != FU_OK)
				return FU_ERROR;
		} else {
			if (fu_ftdi_mpsse_i2c_read_byte(fu, 1, &bytes[i]) != FU_OK)
				return FU_ERROR;
		}
	}

	return fu_ftdi_mpsse_i2c_stop(fu);
}

int fu_ftdi_mpsse_i2c_write(fu_t *fu, uint8_t i2c_address, uint8_t *bytes, int len)
{
	int i;

	if (fu_ftdi_mpsse_i2c_start(fu) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, (i2c_address << 1)) != FU_OK)
		return FU_ERROR;

	for (i = 0; i < len; i++) {
		if (fu_ftdi_mpsse_i2c_write_byte(fu, bytes[i]) != FU_OK)
				return FU_ERROR;
	}

	return fu_ftdi_mpsse_i2c_stop(fu);
}

int fu_ftdi_mpsse_i2c_read_smb(fu_t *fu, uint8_t i2c_address, uint8_t address, uint8_t *bytes, int len)
{
	int i;

	if (fu_ftdi_mpsse_i2c_start(fu) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, (i2c_address << 1)) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, address) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_start(fu) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, (i2c_address << 1) | 0x01) != FU_OK)
		return FU_ERROR;

	for (i = 0; i < len; i++) {
		if ((i + 1) == len) {
			if (fu_ftdi_mpsse_i2c_read_byte(fu, 0, &bytes[i]) != FU_OK)
				return FU_ERROR;
		} else {
			if (fu_ftdi_mpsse_i2c_read_byte(fu, 1, &bytes[i]) != FU_OK)
				return FU_ERROR;
		}
	}

	return fu_ftdi_mpsse_i2c_stop(fu);
}

int fu_ftdi_mpsse_i2c_write_smb(fu_t *fu, uint8_t i2c_address, uint8_t address, uint8_t *bytes, int len)
{
	int i;

	if (fu_ftdi_mpsse_i2c_start(fu) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, (i2c_address << 1)) != FU_OK)
		return FU_ERROR;

	if (fu_ftdi_mpsse_i2c_write_byte(fu, address) != FU_OK)
		return FU_ERROR;

	for (i = 0; i < len; i++) {
		if (fu_ftdi_mpsse_i2c_write_byte(fu, bytes[i]) != FU_OK)
				return FU_ERROR;
	}

	return fu_ftdi_mpsse_i2c_stop(fu);
}

