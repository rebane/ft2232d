#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include "fu.h"

int main(int argc, char *argv[]){
	fu_t fu;
	uint8_t buffer[256];

	fu_init(&fu, NULL);
	fu_ftdi_serial(&fu, "54aa0699-6e9b-4262-b15d-9270dd565b15");
	fu_ftdi_channel(&fu, FU_FTDI_CHANNEL_0);
	fu_ftdi_open(&fu);
	fu_ftdi_set_bitmode(&fu, FU_FTDI_BITMODE_MPSSE, 0);

	fu_ftdi_mpsse_pins_set(&fu, 0, fu_ftdi_pin_get(&fu, "rst"));
	usleep(100000);

	fu_ftdi_mpsse_set_loopback(&fu, 0);
	fu_ftdi_sync(&fu);

	fu_ftdi_mpsse_pins_merge(&fu, fu_ftdi_pin_get(&fu, "cs"), fu_ftdi_pin_get(&fu, "sck") | fu_ftdi_pin_get(&fu, "mosi"), 0);

	fu_ftdi_mpsse_spi_flash_jedec(&fu, buffer);
	printf("JEDEC: 0x%02X, 0x%02X, 0x%02X\n", (unsigned int)buffer[0], (unsigned int)buffer[1], (unsigned int)buffer[2]);

	fu_ftdi_mpsse_spi_flash_set_address_bytes(&fu, 4);
	printf("4 byte mode ok\n");

	fu_ftdi_mpsse_spi_flash_erase_write_file(&fu, 0x00000, 0, "bootloader.bin", -1);
	printf("erase_write ok\n");

//	fu_ftdi_mpsse_spi_flash_erase_write_file(&fu, 0x50000, 0, "plc2_kernel5.itb", -1);
//	printf("erase_write ok\n");

	fu_ftdi_mpsse_spi_flash_set_address_bytes(&fu, 3);
	printf("3 byte mode ok\n");

	fu_ftdi_mpsse_pins_set(&fu, 0, fu_ftdi_pin_get(&fu, "rst"));
	usleep(100000);
	fu_ftdi_mpsse_pins_set(&fu, fu_ftdi_pin_get(&fu, "rst"), 0);

	return(0);
}

