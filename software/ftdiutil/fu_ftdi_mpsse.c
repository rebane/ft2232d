#include "fu_ftdi_mpsse.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ftdi.h>
#include "fu.h"
#include "fu_ftdi.h"
#include "fu_ftdi_mpsse_pins.h"
#include "fu_ftdi_mpsse_spi.h"

static uint32_t fu_ftdi_mpsse_divisor2hz(uint32_t divisor);
static uint32_t fu_ftdi_mpsse_diff(uint32_t a, uint32_t b);

int fu_ftdi_mpsse_init(fu_t *fu){
	fu_ftdi_mpsse_pins_init(fu);
	fu_ftdi_mpsse_spi_init(fu);
	return(FU_OK);
}

int fu_ftdi_mpsse_test_init(fu_t *fu){
	return(FU_OK);
}

int fu_ftdi_mpsse_set_clock(fu_t *fu, uint32_t clock_requested, uint32_t *clock_actual){
	uint32_t divisor, divisor_new, diff, diff2;
	uint8_t b[8];
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	if(clock_requested == 30000000LLU){
		b[0] = DIS_DIV_5;
		b[1] = DIS_ADAPTIVE;
		b[2] = DIS_3_PHASE;
		b[3] = TCK_DIVISOR;
		b[4] = 0x00;
		b[5] = 0x00;
		*clock_actual = 30000000LLU;
		if(fu_ftdi_write_nosync(fu, b, 6) == FU_ERROR)return(FU_ERROR);
		return(FU_OK);
	}
	if(clock_requested > 6000000LLU)clock_requested = 6000000LLU;
	divisor = (12000000LLU / (clock_requested * 2LLU)) - 1LLU;
	if(divisor > 0xFFFF)divisor = 0xFFFF;
	divisor_new = divisor;
	diff = fu_ftdi_mpsse_diff(fu_ftdi_mpsse_divisor2hz(divisor), clock_requested);
	if(divisor > 0){
		diff2 = fu_ftdi_mpsse_diff(fu_ftdi_mpsse_divisor2hz(divisor - 1), clock_requested);
		if(diff2 < diff){
			diff = diff2;
			divisor_new = divisor - 1;
		}
	}
	if(divisor < 0xFFFF){
		diff2 = fu_ftdi_mpsse_diff(fu_ftdi_mpsse_divisor2hz(divisor + 1), clock_requested);
		if(diff2 < diff){
			diff = diff2;
			divisor_new = divisor + 1;
		}
	}
	b[0] = TCK_DIVISOR;
	b[1] = ((divisor_new >> 0) & 0xFF);
	b[2] = ((divisor_new >> 8) & 0xFF);
	*clock_actual = fu_ftdi_mpsse_divisor2hz(divisor_new);
	if(fu_ftdi_write_nosync(fu, b, 3) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_set_loopback(fu_t *fu, int on){
	uint8_t b[1];
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	if(on){
		b[0] = LOOPBACK_START;
	}else{
		b[0] = LOOPBACK_END;
	}
	if(fu_ftdi_write_nosync(fu, b, 1) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_get_bits_low(fu_t *fu){
	uint8_t c;
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	c = GET_BITS_LOW;
	if(fu_ftdi_write_nosync(fu, &c, 1) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_get_bits_high(fu_t *fu){
	uint8_t c;
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	c = GET_BITS_HIGH;
	if(fu_ftdi_write_nosync(fu, &c, 1) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_set_bits_low(fu_t *fu, uint8_t data, uint8_t dir){
	uint8_t b[3];
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	b[0] = SET_BITS_LOW;
	b[1] = data;
	b[2] = dir;
	if(fu_ftdi_write_nosync(fu, b, 3) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_set_bits_high(fu_t *fu, uint8_t data, uint8_t dir){
	uint8_t b[3];
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	b[0] = SET_BITS_HIGH;
	b[1] = data;
	b[2] = dir;
	if(fu_ftdi_write_nosync(fu, b, 3) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_transfer_nosync(fu_t *fu, uint8_t flags, int count){
	uint8_t b[3];
	if(fu_ftdi_test_bitmode(fu, BITMODE_MPSSE) == FU_ERROR)return(FU_ERROR);
	if(count < 1){
		fu_eprintf(fu, "ftdi_mpsse_transfer: invalid transfer size (%d)\n", count);
		return(FU_ERROR);
	}
	if(!(flags & FU_FTDI_MPSSE_FLAG_DO_WRITE) && !(flags & FU_FTDI_MPSSE_FLAG_DO_READ)){
		fu_eprintf(fu, "ftdi_mpsse_transfer: no read or write\n");
		return(FU_ERROR);
	}
	b[0] = 0;
	if(flags & FU_FTDI_MPSSE_FLAG_WRITE_NEG)b[0] |= MPSSE_WRITE_NEG;
	if(flags & FU_FTDI_MPSSE_FLAG_BITMODE)b[0] |= MPSSE_BITMODE;
	if(flags & FU_FTDI_MPSSE_FLAG_READ_NEG)b[0] |= MPSSE_READ_NEG;
	if(flags & FU_FTDI_MPSSE_FLAG_LSB)b[0] |= MPSSE_LSB;
	if(flags & FU_FTDI_MPSSE_FLAG_DO_WRITE)b[0] |= MPSSE_DO_WRITE;
	if(flags & FU_FTDI_MPSSE_FLAG_DO_READ)b[0] |= MPSSE_DO_READ;
	if(flags & FU_FTDI_MPSSE_FLAG_WRITE_TMS)b[0] |= MPSSE_WRITE_TMS;
	b[1] = (((count - 1) >> 0) & 0xFF);
	b[2] = (((count - 1) >> 8) & 0xFF);
	if(fu_ftdi_write_nosync(fu, b, 3) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

int fu_ftdi_mpsse_transfer(fu_t *fu, uint8_t flags, int count){
	if(fu_ftdi_mpsse_transfer_nosync(fu, flags, count) == FU_ERROR)return(FU_ERROR);
	if(fu_ftdi_sync(fu) == FU_ERROR)return(FU_ERROR);
	return(FU_OK);
}

static uint32_t fu_ftdi_mpsse_divisor2hz(uint32_t divisor){
	return(12000000LLU / ((1LLU + divisor) * 2LLU));
}

static uint32_t fu_ftdi_mpsse_diff(uint32_t a, uint32_t b){
	if(a > b)return(a - b);
	return(b - a);
}

