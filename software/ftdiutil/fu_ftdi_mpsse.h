#ifndef _FU_FTDI_MPSSE_H_
#define _FU_FTDI_MPSSE_H_

#include <stdint.h>
#include "fu.h"

#define FU_FTDI_MPSSE_FLAG_WRITE_NEG 0x01
#define FU_FTDI_MPSSE_FLAG_BITMODE   0x02
#define FU_FTDI_MPSSE_FLAG_READ_NEG  0x04
#define FU_FTDI_MPSSE_FLAG_LSB       0x08
#define FU_FTDI_MPSSE_FLAG_DO_WRITE  0x10
#define FU_FTDI_MPSSE_FLAG_DO_READ   0x20
#define FU_FTDI_MPSSE_FLAG_WRITE_TMS 0x40

int fu_ftdi_mpsse_init(fu_t *fu);
int fu_ftdi_mpsse_test_init(fu_t *fu);
int fu_ftdi_mpsse_set_clock(fu_t *fu, uint32_t clock_requested, uint32_t *clock_actual);
int fu_ftdi_mpsse_set_loopback(fu_t *fu, int on);
int fu_ftdi_mpsse_get_bits_low(fu_t *fu);
int fu_ftdi_mpsse_get_bits_high(fu_t *fu);

int fu_ftdi_mpsse_set_bits_low_nosync(fu_t *fu, uint8_t data, uint8_t dir);
int fu_ftdi_mpsse_set_bits_low(fu_t *fu, uint8_t data, uint8_t dir);

int fu_ftdi_mpsse_set_bits_high_nosync(fu_t *fu, uint8_t data, uint8_t dir);
int fu_ftdi_mpsse_set_bits_high(fu_t *fu, uint8_t data, uint8_t dir);

int fu_ftdi_mpsse_transfer_nosync(fu_t *fu, uint8_t flags, int count);
int fu_ftdi_mpsse_transfer(fu_t *fu, uint8_t flags, int count);

#endif

