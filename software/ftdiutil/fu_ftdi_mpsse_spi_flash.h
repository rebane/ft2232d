#ifndef _FU_FTDI_MPSSE_SPI_FLASH_H_
#define _FU_FTDI_MPSSE_SPI_FLASH_H_

#include <stdint.h>
#include "fu.h"

int fu_ftdi_mpsse_spi_flash_init(fu_t *fu, char *pin);
int fu_ftdi_mpsse_spi_flash_test_init(fu_t *fu);
int fu_ftdi_mpsse_spi_flash_transfer(fu_t *fu, void *out, int out_len, void *in, int in_len);
int fu_ftdi_mpsse_spi_flash_status(fu_t *fu, uint8_t *status);
int fu_ftdi_mpsse_spi_flash_write_status(fu_t *fu, uint8_t status);
int fu_ftdi_mpsse_spi_flash_wait_busy(fu_t *fu);
int fu_ftdi_mpsse_spi_flash_wait_wel(fu_t *fu);
int fu_ftdi_mpsse_spi_flash_jedec(fu_t *fu, void *jedec);
int fu_ftdi_mpsse_spi_flash_write_enable(fu_t *fu, int on);
int fu_ftdi_mpsse_spi_flash_set_address_bytes(fu_t *fu, int address_bytes);
int fu_ftdi_mpsse_spi_flash_write_extended_address(fu_t *fu, uint8_t ea);
int fu_ftdi_mpsse_spi_flash_erase(fu_t *fu, uint8_t command, uint32_t offset);
int fu_ftdi_mpsse_spi_flash_erase_area(fu_t *fu, uint8_t command, uint32_t step, uint32_t offset, uint32_t len);
int fu_ftdi_mpsse_spi_flash_write(fu_t *fu, uint32_t offset, void *data, uint32_t len);
int fu_ftdi_mpsse_spi_flash_write_fd(fu_t *fu, uint32_t offset, int fd, uint32_t len);
int fu_ftdi_mpsse_spi_flash_erase_write_file(fu_t *fu, uint32_t flash_offset, uint32_t file_offset, char *filename, int32_t len);
int fu_ftdi_mpsse_spi_flash_read(fu_t *fu, uint32_t offset, void *data, uint32_t len);
int fu_ftdi_mpsse_spi_flash_read_fd(fu_t *fu, uint32_t offset, int fd, uint32_t len);
int fu_ftdi_mpsse_spi_flash_read_file(fu_t *fu, uint32_t flash_offset, uint32_t file_offset, char *filename, int32_t len);

#endif

