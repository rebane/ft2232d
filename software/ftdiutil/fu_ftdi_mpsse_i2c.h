#ifndef _FU_FTDI_MPSSE_I2C_H_
#define _FU_FTDI_MPSSE_I2C_H_

#include "fu.h"

int fu_ftdi_mpsse_i2c_init(fu_t *fu);
int fu_ftdi_mpsse_i2c_test_init(fu_t *fu);
void fu_ftdi_mpsse_i2c_delay();
void fu_ftdi_mpsse_i2c_scl(fu_t *fu, int on);
void fu_ftdi_mpsse_i2c_sda(fu_t *fu, int on);
int fu_ftdi_mpsse_i2c_reset(fu_t *fu);
int fu_ftdi_mpsse_i2c_start(fu_t *fu);
int fu_ftdi_mpsse_i2c_read_bit(fu_t *fu, uint8_t *bit);
int fu_ftdi_mpsse_i2c_write_bit(fu_t *fu, uint8_t bit);
int fu_ftdi_mpsse_i2c_stop(fu_t *fu);
int fu_ftdi_mpsse_i2c_read_byte(fu_t *fu, int ack, uint8_t *byte);
int fu_ftdi_mpsse_i2c_write_byte(fu_t *fu, uint8_t byte);
int fu_ftdi_mpsse_i2c_read(fu_t *fu, uint8_t i2c_address, uint8_t *bytes, int len);
int fu_ftdi_mpsse_i2c_write(fu_t *fu, uint8_t i2c_address, uint8_t *bytes, int len);
int fu_ftdi_mpsse_i2c_read_smb(fu_t *fu, uint8_t i2c_address, uint8_t address, uint8_t *bytes, int len);
int fu_ftdi_mpsse_i2c_write_smb(fu_t *fu, uint8_t i2c_address, uint8_t address, uint8_t *bytes, int len);

#endif

