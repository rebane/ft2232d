#include "getpath.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/sysmacros.h>

char *getpath(int major, int minor){
	DIR *d;
	struct dirent *entry;
	struct stat s;
	char buffer[512], *path;
	d = opendir("/dev");
	if(d == NULL)return(NULL);
	while((entry = readdir(d))){
		snprintf(buffer, 511, "/dev/%s", entry->d_name);
		buffer[511] = 0;
		if(stat(buffer, &s) < 0)continue;
		if(!S_ISCHR(s.st_mode))continue;
		if(major(s.st_rdev) != major)continue;
		if(minor(s.st_rdev) != minor)continue;
		closedir(d);
		path = strdup(buffer);
		return(path);
	}
	return(NULL);
}

