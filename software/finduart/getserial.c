#include "getserial.h"
#include <stdio.h>
#include <string.h>
#include <dirent.h>

char *getserial(char *dir, char *id){
	DIR *d;
	struct dirent *entry;
	FILE *f;
	char buffer[512], *serial;
	int i, s;
	d = opendir(dir);
	if(d == NULL)return(NULL);
	while((entry = readdir(d))){
		snprintf(buffer, 511, "%s/%s", dir, entry->d_name);
		buffer[511] = 0;
		f = fopen(buffer, "r");
		if(f == NULL)continue;
		while(!feof(f)){
			if(fgets(buffer, 511, f) == NULL)break;
			buffer[511] = 0;
			for(i = 0; buffer[i]; i++){
				if(buffer[i] == ' ')continue;
				if(buffer[i] == '\t')continue;
				break;
			}
			if(buffer[i] == '#')continue;
			if(buffer[i] == '\r')continue;
			if(buffer[i] == '\n')continue;
			s = i;
			i++;
			for( ; buffer[i]; i++){
				if(buffer[i] == ' ')break;
				if(buffer[i] == '\t')break;
				if(buffer[i] == '\r')break;
				if(buffer[i] == '\n')break;
			}
			if(buffer[i] == '\r')continue;
			if(buffer[i] == '\n')continue;
			buffer[i] = 0;
			if(strcmp(&buffer[s], id))continue;
			i++;
			for( ; buffer[i]; i++){
				if(buffer[i] == ' ')continue;
				if(buffer[i] == '\t')continue;
				break;
			}
			if(buffer[i] == '#')continue;
			if(buffer[i] == '\r')continue;
			if(buffer[i] == '\n')continue;
			s = i;
			i++;
			for( ; buffer[i]; i++){
				if(buffer[i] == ' ')break;
				if(buffer[i] == '\t')break;
				if(buffer[i] == '\r')break;
				if(buffer[i] == '\n')break;
			}
			buffer[i] = 0;
			serial = strdup(&buffer[s]);
			fclose(f);
			closedir(d);
			return(serial);
		}
		fclose(f);
	}
	closedir(d);
	return(NULL);
}

