#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "getserial.h"
#include "getdevice.h"
#include "getpath.h"

int main(int argc, char *argv[]){
	int c, interface;
	char *ftdi_id;
	char *serial_number, *path;
	int major, minor;

	interface = 1;
	ftdi_id = NULL;

	opterr = 0;
	while((c = getopt(argc, argv, "hI:i:")) != -1){
		if(c == 'h'){
			printf("help...\n");
			return(0);
		}else if(c == 'I'){
			interface = atoi(optarg);
		}else if(c == 'i'){
			ftdi_id = optarg;
		}
	}
	if(ftdi_id == NULL)ftdi_id = getenv("FTDI_ID");
	if(ftdi_id == NULL){
		fprintf(stderr, "FTDI_ID not set\n");
		return(1);
	}
	serial_number = getserial("/etc/ftdi_id_list.d", ftdi_id);
	if(serial_number == NULL){
		serial_number = strdup(ftdi_id);
//		fprintf(stderr, "cannot find serial number\n");
//		return(1);
	}
	if(!getdevice(serial_number, interface, &major, &minor)){
		free(serial_number);
		fprintf(stderr, "cannot find device\n");
		return(1);
	}
	free(serial_number);
	path = getpath(major, minor);
	if(path == NULL){
		fprintf(stderr, "cannot find path\n");
		return(1);
	}
	printf("%s\n", path);
	free(path);
	return(0);
}

