#include "getdevice.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libudev.h>

int getdevice(char *serial, int interface, int *major, int *minor){
	struct udev *udev;
	struct udev_enumerate *enumerate;
	struct udev_list_entry *devices, *devices_entry;
	struct udev_device *dev;
	const char *path, *name, *value, *dev_serial, *dev_interface, *dev_driver, *dev_major, *dev_minor;
	struct udev_list_entry *sysattrs, *sysattrs_entry;
	int i, ret;

	udev = udev_new();
	if(udev == NULL)return(0);

	enumerate = udev_enumerate_new(udev);
	if(enumerate == NULL){
		udev_unref(udev);
		return(0);
	}

	udev_enumerate_add_match_subsystem(enumerate, "tty");
	udev_enumerate_scan_devices(enumerate);

	devices = udev_enumerate_get_list_entry(enumerate);
	if(devices == NULL){
		udev_enumerate_unref(enumerate);
		udev_unref(udev);
		return(0);
	}

	ret = 0;

	dev = NULL;
	udev_list_entry_foreach(devices_entry, devices) {
		if(dev != NULL)udev_device_unref(dev);
		dev = NULL;
		path = udev_list_entry_get_name(devices_entry);
		if(path == NULL)continue;
		dev = udev_device_new_from_syspath(udev, path);
		if(dev == NULL)continue;
		sysattrs = udev_device_get_properties_list_entry(dev);
		if(sysattrs == NULL){
			udev_device_unref(dev);
			continue;
		}
		dev_serial = dev_interface = dev_driver = dev_major = dev_minor = NULL;
		udev_list_entry_foreach(sysattrs_entry, sysattrs){
			name = udev_list_entry_get_name(sysattrs_entry);
			if(name == NULL)continue;
			value = udev_list_entry_get_value(sysattrs_entry);
			if(value == NULL)continue;
			if(!strcmp(name, "ID_SERIAL_SHORT")){
				dev_serial = value;
			}else if(!strcmp(name, "ID_USB_INTERFACE_NUM")){
				dev_interface = value;
			}else if(!strcmp(name, "ID_USB_DRIVER")){
				dev_driver = value;
			}else if(!strcmp(name, "MAJOR")){
				dev_major = value;
			}else if(!strcmp(name, "MINOR")){
				dev_minor = value;
			}
		}
		if(dev_serial == NULL)continue;
		if(dev_interface == NULL)continue;
		if(dev_driver == NULL)continue;
		if(dev_major == NULL)continue;
		if(dev_minor == NULL)continue;
		if(strcmp(dev_serial, serial))continue;
		i = atoi(dev_interface);
		if(!strcmp(dev_driver, "ftdi_sio")){
			if(interface == 0){
				if(i != 0)continue;
			}else if(interface == 1){
				if(i != 1)continue;
			}
		}
		*major = atoi(dev_major);
		*minor = atoi(dev_minor);
		ret = 1;
		break;
	}
	if(dev != NULL)udev_device_unref(dev);
	udev_enumerate_unref(enumerate);
	udev_unref(udev);
	return(ret);
}

