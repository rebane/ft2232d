<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="15" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="11" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="13" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="punktiir" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun">
<description>Spark Fun Electronics' preferred foot prints. &lt;b&gt;Not to be used for commercial purposes.&lt;/b&gt; We've spent an enormous amount of time creating and checking these footprints and parts. If you enjoy using this library, please buy one of our products at www.sparkfun.com.</description>
<packages>
<package name="LQFP-48">
<wire x1="-3.375" y1="3.1" x2="-3.1" y2="3.375" width="0.254" layer="21"/>
<wire x1="-3.1" y1="3.375" x2="3.1" y2="3.375" width="0.254" layer="21"/>
<wire x1="3.1" y1="3.375" x2="3.375" y2="3.1" width="0.254" layer="21"/>
<wire x1="3.375" y1="3.1" x2="3.375" y2="-3.1" width="0.254" layer="21"/>
<wire x1="3.375" y1="-3.1" x2="3.1" y2="-3.375" width="0.254" layer="21"/>
<wire x1="3.1" y1="-3.375" x2="-3.1" y2="-3.375" width="0.254" layer="21"/>
<wire x1="-3.1" y1="-3.375" x2="-3.375" y2="-3.1" width="0.254" layer="21"/>
<wire x1="-3.375" y1="-3.1" x2="-3.375" y2="3.1" width="0.254" layer="21"/>
<circle x="-2" y="-2" radius="0.6" width="0.254" layer="21"/>
<smd name="1" x="-2.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="2" x="-2.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="3" x="-1.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="4" x="-1.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="5" x="-0.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="6" x="-0.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="7" x="0.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="8" x="0.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="9" x="1.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="10" x="1.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="11" x="2.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="12" x="2.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="13" x="4.25" y="-2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="14" x="4.25" y="-2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="15" x="4.25" y="-1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="16" x="4.25" y="-1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="17" x="4.25" y="-0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="18" x="4.25" y="-0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="19" x="4.25" y="0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="20" x="4.25" y="0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="21" x="4.25" y="1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="22" x="4.25" y="1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="23" x="4.25" y="2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="24" x="4.25" y="2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="25" x="2.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="26" x="2.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="27" x="1.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="28" x="1.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="29" x="0.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="30" x="0.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="31" x="-0.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="32" x="-0.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="33" x="-1.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="34" x="-1.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="35" x="-2.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="36" x="-2.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="37" x="-4.25" y="2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="38" x="-4.25" y="2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="39" x="-4.25" y="1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="40" x="-4.25" y="1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="41" x="-4.25" y="0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="42" x="-4.25" y="0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="43" x="-4.25" y="-0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="44" x="-4.25" y="-0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="45" x="-4.25" y="-1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="46" x="-4.25" y="-1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="47" x="-4.25" y="-2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="48" x="-4.25" y="-2.75" dx="1.143" dy="0.254" layer="1"/>
<text x="-3.81" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.85" y1="-4.5" x2="-2.65" y2="-3.45" layer="51"/>
<rectangle x1="-2.35" y1="-4.5" x2="-2.15" y2="-3.45" layer="51"/>
<rectangle x1="-1.85" y1="-4.5" x2="-1.65" y2="-3.45" layer="51"/>
<rectangle x1="-1.35" y1="-4.5" x2="-1.15" y2="-3.45" layer="51"/>
<rectangle x1="-0.85" y1="-4.5" x2="-0.65" y2="-3.45" layer="51"/>
<rectangle x1="-0.35" y1="-4.5" x2="-0.15" y2="-3.45" layer="51"/>
<rectangle x1="0.15" y1="-4.5" x2="0.35" y2="-3.45" layer="51"/>
<rectangle x1="0.65" y1="-4.5" x2="0.85" y2="-3.45" layer="51"/>
<rectangle x1="1.15" y1="-4.5" x2="1.35" y2="-3.45" layer="51"/>
<rectangle x1="1.65" y1="-4.5" x2="1.85" y2="-3.45" layer="51"/>
<rectangle x1="2.15" y1="-4.5" x2="2.35" y2="-3.45" layer="51"/>
<rectangle x1="2.65" y1="-4.5" x2="2.85" y2="-3.45" layer="51"/>
<rectangle x1="3.45" y1="-2.85" x2="4.5" y2="-2.65" layer="51"/>
<rectangle x1="3.45" y1="-2.35" x2="4.5" y2="-2.15" layer="51"/>
<rectangle x1="3.45" y1="-1.85" x2="4.5" y2="-1.65" layer="51"/>
<rectangle x1="3.45" y1="-1.35" x2="4.5" y2="-1.15" layer="51"/>
<rectangle x1="3.45" y1="-0.85" x2="4.5" y2="-0.65" layer="51"/>
<rectangle x1="3.45" y1="-0.35" x2="4.5" y2="-0.15" layer="51"/>
<rectangle x1="3.45" y1="0.15" x2="4.5" y2="0.35" layer="51"/>
<rectangle x1="3.45" y1="0.65" x2="4.5" y2="0.85" layer="51"/>
<rectangle x1="3.45" y1="1.15" x2="4.5" y2="1.35" layer="51"/>
<rectangle x1="3.45" y1="1.65" x2="4.5" y2="1.85" layer="51"/>
<rectangle x1="3.45" y1="2.15" x2="4.5" y2="2.35" layer="51"/>
<rectangle x1="3.45" y1="2.65" x2="4.5" y2="2.85" layer="51"/>
<rectangle x1="2.65" y1="3.45" x2="2.85" y2="4.5" layer="51"/>
<rectangle x1="2.15" y1="3.45" x2="2.35" y2="4.5" layer="51"/>
<rectangle x1="1.65" y1="3.45" x2="1.85" y2="4.5" layer="51"/>
<rectangle x1="1.15" y1="3.45" x2="1.35" y2="4.5" layer="51"/>
<rectangle x1="0.65" y1="3.45" x2="0.85" y2="4.5" layer="51"/>
<rectangle x1="0.15" y1="3.45" x2="0.35" y2="4.5" layer="51"/>
<rectangle x1="-0.35" y1="3.45" x2="-0.15" y2="4.5" layer="51"/>
<rectangle x1="-0.85" y1="3.45" x2="-0.65" y2="4.5" layer="51"/>
<rectangle x1="-1.35" y1="3.45" x2="-1.15" y2="4.5" layer="51"/>
<rectangle x1="-1.85" y1="3.45" x2="-1.65" y2="4.5" layer="51"/>
<rectangle x1="-2.35" y1="3.45" x2="-2.15" y2="4.5" layer="51"/>
<rectangle x1="-2.85" y1="3.45" x2="-2.65" y2="4.5" layer="51"/>
<rectangle x1="-4.5" y1="2.65" x2="-3.45" y2="2.85" layer="51"/>
<rectangle x1="-4.5" y1="2.15" x2="-3.45" y2="2.35" layer="51"/>
<rectangle x1="-4.5" y1="1.65" x2="-3.45" y2="1.85" layer="51"/>
<rectangle x1="-4.5" y1="1.15" x2="-3.45" y2="1.35" layer="51"/>
<rectangle x1="-4.5" y1="0.65" x2="-3.45" y2="0.85" layer="51"/>
<rectangle x1="-4.5" y1="0.15" x2="-3.45" y2="0.35" layer="51"/>
<rectangle x1="-4.5" y1="-0.35" x2="-3.45" y2="-0.15" layer="51"/>
<rectangle x1="-4.5" y1="-0.85" x2="-3.45" y2="-0.65" layer="51"/>
<rectangle x1="-4.5" y1="-1.35" x2="-3.45" y2="-1.15" layer="51"/>
<rectangle x1="-4.5" y1="-1.85" x2="-3.45" y2="-1.65" layer="51"/>
<rectangle x1="-4.5" y1="-2.35" x2="-3.45" y2="-2.15" layer="51"/>
<rectangle x1="-4.5" y1="-2.85" x2="-3.45" y2="-2.65" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FT2232D">
<wire x1="-15.24" y1="40.64" x2="17.78" y2="40.64" width="0.254" layer="94"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="-40.64" width="0.254" layer="94"/>
<wire x1="17.78" y1="-40.64" x2="-15.24" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-40.64" x2="-15.24" y2="40.64" width="0.254" layer="94"/>
<text x="-15.24" y="41.402" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="-43.18" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3V3OUT" x="-20.32" y="25.4" length="middle" direction="out"/>
<pin name="USBDM" x="-20.32" y="15.24" length="middle"/>
<pin name="USBDP" x="-20.32" y="12.7" length="middle"/>
<pin name="GND1" x="-20.32" y="-30.48" length="middle" direction="pwr"/>
<pin name="GND2" x="-20.32" y="-33.02" length="middle" direction="pwr"/>
<pin name="GND3" x="-20.32" y="-35.56" length="middle" direction="pwr"/>
<pin name="BDBUS0" x="22.86" y="0" length="middle" direction="out" rot="R180"/>
<pin name="BDBUS1" x="22.86" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="BDBUS2" x="22.86" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="BDBUS3" x="22.86" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="BDBUS4" x="22.86" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="BDBUS5" x="22.86" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="BDBUS6" x="22.86" y="-15.24" length="middle" direction="in" rot="R180"/>
<pin name="BDBUS7" x="22.86" y="-17.78" length="middle" direction="in" rot="R180"/>
<pin name="PWEN#" x="22.86" y="-38.1" length="middle" rot="R180"/>
<pin name="VCCIOA" x="-20.32" y="30.48" length="middle" direction="pwr"/>
<pin name="AGND" x="-20.32" y="-27.94" length="middle" direction="pwr"/>
<pin name="TEST" x="-20.32" y="-17.78" length="middle" direction="in"/>
<pin name="VCC2" x="-20.32" y="35.56" length="middle" direction="pwr"/>
<pin name="RSTOUT#" x="-20.32" y="7.62" length="middle"/>
<pin name="RESET#" x="-20.32" y="5.08" length="middle"/>
<pin name="XTIN" x="-20.32" y="0" length="middle"/>
<pin name="XTOUT" x="-20.32" y="-2.54" length="middle"/>
<pin name="EECS" x="-20.32" y="-10.16" length="middle"/>
<pin name="EESK" x="-20.32" y="-12.7" length="middle"/>
<pin name="EEDATA" x="-20.32" y="-15.24" length="middle"/>
<pin name="GND4" x="-20.32" y="-38.1" length="middle"/>
<pin name="SI/WUB" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="BCBUS3" x="22.86" y="-30.48" length="middle" rot="R180"/>
<pin name="BCBUS2" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="BCBUS1" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="BCBUS0" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="ACBUS2" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="ACBUS1" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="ACBUS0" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="ADBUS7" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="ADBUS6" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="ADBUS5" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="SI/WUA" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="ACBUS3" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ADBUS4" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="ADBUS3" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="ADBUS2" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="ADBUS1" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="ADBUS0" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="VCC1" x="-20.32" y="38.1" length="middle" direction="pwr"/>
<pin name="AVCC" x="-20.32" y="33.02" length="middle"/>
<pin name="VCCIOB" x="-20.32" y="27.94" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT2232D">
<gates>
<gate name="G$1" symbol="FT2232D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP-48">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="6"/>
<connect gate="G$1" pin="ACBUS0" pad="15"/>
<connect gate="G$1" pin="ACBUS1" pad="13"/>
<connect gate="G$1" pin="ACBUS2" pad="12"/>
<connect gate="G$1" pin="ACBUS3" pad="11"/>
<connect gate="G$1" pin="ADBUS0" pad="24"/>
<connect gate="G$1" pin="ADBUS1" pad="23"/>
<connect gate="G$1" pin="ADBUS2" pad="22"/>
<connect gate="G$1" pin="ADBUS3" pad="21"/>
<connect gate="G$1" pin="ADBUS4" pad="20"/>
<connect gate="G$1" pin="ADBUS5" pad="19"/>
<connect gate="G$1" pin="ADBUS6" pad="17"/>
<connect gate="G$1" pin="ADBUS7" pad="16"/>
<connect gate="G$1" pin="AGND" pad="45"/>
<connect gate="G$1" pin="AVCC" pad="46"/>
<connect gate="G$1" pin="BCBUS0" pad="30"/>
<connect gate="G$1" pin="BCBUS1" pad="29"/>
<connect gate="G$1" pin="BCBUS2" pad="28"/>
<connect gate="G$1" pin="BCBUS3" pad="27"/>
<connect gate="G$1" pin="BDBUS0" pad="40"/>
<connect gate="G$1" pin="BDBUS1" pad="39"/>
<connect gate="G$1" pin="BDBUS2" pad="38"/>
<connect gate="G$1" pin="BDBUS3" pad="37"/>
<connect gate="G$1" pin="BDBUS4" pad="36"/>
<connect gate="G$1" pin="BDBUS5" pad="35"/>
<connect gate="G$1" pin="BDBUS6" pad="33"/>
<connect gate="G$1" pin="BDBUS7" pad="32"/>
<connect gate="G$1" pin="EECS" pad="48"/>
<connect gate="G$1" pin="EEDATA" pad="2"/>
<connect gate="G$1" pin="EESK" pad="1"/>
<connect gate="G$1" pin="GND1" pad="18"/>
<connect gate="G$1" pin="GND2" pad="25"/>
<connect gate="G$1" pin="GND3" pad="34"/>
<connect gate="G$1" pin="GND4" pad="9"/>
<connect gate="G$1" pin="PWEN#" pad="41"/>
<connect gate="G$1" pin="RESET#" pad="4"/>
<connect gate="G$1" pin="RSTOUT#" pad="5"/>
<connect gate="G$1" pin="SI/WUA" pad="10"/>
<connect gate="G$1" pin="SI/WUB" pad="26"/>
<connect gate="G$1" pin="TEST" pad="47"/>
<connect gate="G$1" pin="USBDM" pad="8"/>
<connect gate="G$1" pin="USBDP" pad="7"/>
<connect gate="G$1" pin="VCC1" pad="3"/>
<connect gate="G$1" pin="VCC2" pad="42"/>
<connect gate="G$1" pin="VCCIOA" pad="14"/>
<connect gate="G$1" pin="VCCIOB" pad="31"/>
<connect gate="G$1" pin="XTIN" pad="43"/>
<connect gate="G$1" pin="XTOUT" pad="44"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE">
<packages>
<package name="0603[1608-METRIC]">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="37"/>
<wire x1="-0.8" y1="-0.6" x2="0.8" y2="-0.6" width="0.127" layer="37"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-0OHM-OFF">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90" cream="no"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90" cream="no"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="37"/>
<wire x1="-0.8" y1="-0.6" x2="0.8" y2="-0.6" width="0.127" layer="37"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<polygon width="0" layer="29">
<vertex x="-1.2" y="0.6"/>
<vertex x="1.2" y="0.6"/>
<vertex x="1.2" y="-0.6"/>
<vertex x="-1.2" y="-0.6"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="-0.4" y="0.4"/>
<vertex x="0" y="0"/>
<vertex x="-0.4" y="-0.4"/>
<vertex x="-1" y="-0.4"/>
<vertex x="-1" y="0.4"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0.15" y="0.4"/>
<vertex x="1" y="0.4"/>
<vertex x="1" y="-0.4"/>
<vertex x="0.15" y="-0.4"/>
<vertex x="0.45" y="-0.1"/>
<vertex x="0.45" y="0.1"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-0.3" y="0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.3" y="0.4"/>
</polygon>
</package>
<package name="0603[1608-METRIC]-0OHM">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="37"/>
<wire x1="-0.8" y1="-0.6" x2="0.8" y2="-0.6" width="0.127" layer="37"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<polygon width="0" layer="29">
<vertex x="-1.2" y="0.6"/>
<vertex x="1.2" y="0.6"/>
<vertex x="1.2" y="-0.6"/>
<vertex x="-1.2" y="-0.6"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="-0.4" y="0.4"/>
<vertex x="0" y="0"/>
<vertex x="-0.4" y="-0.4"/>
<vertex x="-1" y="-0.4"/>
<vertex x="-1" y="0.4"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0.15" y="0.4"/>
<vertex x="1" y="0.4"/>
<vertex x="1" y="-0.4"/>
<vertex x="0.15" y="-0.4"/>
<vertex x="0.45" y="-0.1"/>
<vertex x="0.45" y="0.1"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-0.3" y="0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.3" y="0.4"/>
</polygon>
</package>
<package name="5X5">
<smd name="1" x="-1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="1.85" y="0" dx="5" dy="1.8" layer="1" rot="R90"/>
<wire x1="-0.7" y1="2.4" x2="0.7" y2="2.4" width="0.2" layer="37"/>
<wire x1="-0.7" y1="-2.4" x2="0.7" y2="-2.4" width="0.2" layer="37"/>
</package>
<package name="SOT-23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="37"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="37"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="37"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="HC49">
<wire x1="-3.302" y1="-2.413" x2="3.302" y2="-2.413" width="0.254" layer="37"/>
<wire x1="-3.302" y1="2.413" x2="3.302" y2="2.413" width="0.254" layer="37"/>
<wire x1="-3.302" y1="2.413" x2="-3.302" y2="-2.413" width="0.254" layer="37" curve="180"/>
<wire x1="3.302" y1="-2.413" x2="3.302" y2="2.413" width="0.254" layer="37" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-1.624" y="-1.606" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-DIODE">
<smd name="C" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="A" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="37"/>
<wire x1="-0.8" y1="-0.6" x2="0.8" y2="-0.6" width="0.127" layer="37"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="0.3" x2="-1.2" y2="-0.3" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0.2" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.4" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.2" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0" y2="0.1" width="0.127" layer="37"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="37"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0" y1="-0.1" x2="-0.3" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0" x2="0" y2="0" width="0.127" layer="37"/>
</package>
<package name="TACTILE-FSM4JSMA">
<smd name="1" x="-2.25" y="4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="2" x="-2.25" y="-4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="3" x="2.25" y="4.55" dx="1.4" dy="2.1" layer="1"/>
<smd name="4" x="2.25" y="-4.55" dx="1.4" dy="2.1" layer="1"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.2032" layer="21"/>
<wire x1="3.5" y1="3" x2="3.5" y2="-3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="2" width="0.2032" layer="37"/>
<text x="-4.21" y="-2.64" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="TACTILE-FSMSM">
<smd name="1" x="-4.55" y="0" dx="2.11" dy="1.6" layer="1"/>
<smd name="2" x="4.55" y="0" dx="2.11" dy="1.6" layer="1"/>
<wire x1="-3.5" y1="1.8" x2="3.5" y2="1.8" width="0.3048" layer="37"/>
<wire x1="-3.5" y1="-1.8" x2="3.5" y2="-1.8" width="0.3048" layer="37"/>
</package>
<package name="12X12">
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="37"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="37"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="37"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="37"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="37"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="37"/>
<smd name="1" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1" rot="R180"/>
<smd name="2" x="0" y="4.95" dx="5.4" dy="2.9" layer="1" rot="R180"/>
<text x="-1.9" y="-0.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="1812[4532-METRIC]">
<smd name="P$1" x="-2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<smd name="P$2" x="2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<wire x1="-2.4" y1="2.1" x2="2.4" y2="2.1" width="0.127" layer="37"/>
<wire x1="-2.4" y1="-2.1" x2="2.4" y2="-2.1" width="0.127" layer="37"/>
<text x="-1.1" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="3.2X1.5">
<description>3.2x1.5mm crystal</description>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="37"/>
<smd name="P$1" x="-1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<smd name="P$2" x="1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<wire x1="-0.5" y1="-0.8" x2="0.5" y2="-0.8" width="0.127" layer="37"/>
<text x="-1.2" y="1.4" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="3.2X2.5">
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2032" layer="37"/>
<wire x1="2" y1="0.3" x2="2" y2="-0.3" width="0.2032" layer="37"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2032" layer="37"/>
<wire x1="-2" y1="0.3" x2="-2" y2="-0.3" width="0.2032" layer="37"/>
<smd name="1" x="-1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="3" x="1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<text x="-0.99" y="2.055" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="LED3MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="2.102378125" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="37"/>
</package>
<package name="LED5MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" ratio="10">&gt;Name</text>
<circle x="0" y="0" radius="3.257296875" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="37"/>
</package>
<package name="LED2MM-SMD">
<smd name="C" x="-1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="37"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="37"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="37"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="37"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="37"/>
<wire x1="-1.6" y1="0.9" x2="1.6" y2="0.9" width="0.127" layer="37"/>
<wire x1="-1.6" y1="-0.9" x2="1.6" y2="-0.9" width="0.127" layer="37"/>
<wire x1="-0.7" y1="0.7" x2="-0.7" y2="-0.7" width="0.127" layer="37"/>
<text x="-1.2" y="1.3" size="0.8128" layer="37" font="vector">&gt;Name</text>
</package>
<package name="TACTILE-TH">
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="37"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="37"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="37"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="37"/>
<circle x="0" y="0" radius="2" width="0.2032" layer="37"/>
<pad name="A@1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="A@2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@1" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="B@2" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-1.44" y="3.61" size="0.8128" layer="25" font="vector">&gt;Name</text>
</package>
<package name="UUSB">
<pad name="G1" x="-3.6" y="0" drill="1.5" diameter="1.9"/>
<pad name="G2" x="3.6" y="0" drill="1.5" diameter="1.9"/>
<wire x1="-5" y1="-1.5" x2="5" y2="-1.5" width="0.1" layer="21"/>
<smd name="G3" x="-1.15" y="0" dx="1.8" dy="1.9" layer="1"/>
<smd name="G4" x="1.15" y="0" dx="1.8" dy="1.9" layer="1"/>
<pad name="G5" x="-2.425" y="3" drill="0.85"/>
<pad name="G6" x="2.425" y="3" drill="0.85"/>
<smd name="1" x="-1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="2" x="-0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="3" x="0" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="4" x="0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="5" x="1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
</package>
<package name="SOIC8">
<wire x1="-2.54" y1="1.705" x2="2.54" y2="1.705" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.705" x2="2.54" y2="-1.705" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.705" x2="-2.54" y2="-1.705" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.705" x2="-2.54" y2="-1.705" width="0.127" layer="21"/>
<smd name="1" x="-1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<text x="-2.975" y="-1.94" size="0.8128" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="3.845" y="-2.74" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.54" y="-3.81" size="1.27" layer="37" font="vector" ratio="15" rot="R90">&gt;o</text>
<circle x="-1.9" y="-1.1" radius="0.282840625" width="0.127" layer="21"/>
</package>
<package name="DIP8">
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.2" layer="37" curve="180"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="-2.794" width="0.2" layer="37"/>
<wire x1="5.08" y1="-2.794" x2="5.08" y2="2.794" width="0.2" layer="37"/>
<wire x1="-5.08" y1="2.794" x2="-5.08" y2="0.635" width="0.2" layer="37"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-3.175" y="0.4064" size="1.397" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-1.7526" size="1.397" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-4.8" y1="2.8" x2="-5.08" y2="2.8" width="0.2" layer="37"/>
<wire x1="4.8" y1="2.8" x2="5.08" y2="2.8" width="0.2" layer="37"/>
<wire x1="4.8" y1="-2.8" x2="5.08" y2="-2.8" width="0.2" layer="37"/>
<wire x1="-4.8" y1="-2.8" x2="-5.08" y2="-2.8" width="0.2" layer="37"/>
</package>
<package name="1206[3216-METRIC]">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<text x="-1.27" y="1.27" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<wire x1="-1.6" y1="1.1" x2="1.6" y2="1.1" width="0.127" layer="37"/>
<wire x1="-1.6" y1="-1.1" x2="1.6" y2="-1.1" width="0.127" layer="37"/>
</package>
<package name="3.5/7.5">
<pad name="P$1" x="-1.75" y="0" drill="0.7"/>
<pad name="P$2" x="1.75" y="0" drill="0.7"/>
<circle x="0" y="0" radius="3.8" width="0.127" layer="37"/>
</package>
<package name="PS8D43">
<smd name="1" x="-4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="4" y="0" dx="3" dy="1.8" layer="1" rot="R90"/>
<circle x="0" y="0" radius="4.1" width="0.2" layer="21"/>
<wire x1="-3.7" y1="1.8" x2="3.7" y2="1.8" width="0.2" layer="37" curve="-130"/>
<wire x1="3.7" y1="-1.8" x2="-3.7" y2="-1.8" width="0.2" layer="37" curve="-130"/>
<text x="-3.6" y="4.7" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="7X7">
<smd name="1" x="-3.025" y="0" dx="2.35" dy="3.5" layer="1"/>
<smd name="2" x="3.025" y="0" dx="2.35" dy="3.5" layer="1"/>
<wire x1="-3.9" y1="2.1" x2="-3.9" y2="3.5" width="0.1" layer="37"/>
<wire x1="-3.9" y1="3.5" x2="3.9" y2="3.5" width="0.1" layer="37"/>
<wire x1="3.9" y1="3.5" x2="3.9" y2="2.1" width="0.1" layer="37"/>
<wire x1="-3.9" y1="-2.1" x2="-3.9" y2="-3.5" width="0.1" layer="37"/>
<wire x1="-3.9" y1="-3.5" x2="3.9" y2="-3.5" width="0.1" layer="37"/>
<wire x1="3.9" y1="-3.5" x2="3.9" y2="-2.1" width="0.1" layer="37"/>
</package>
<package name="3X3">
<smd name="1" x="-1.15" y="0" dx="1.5" dy="4" layer="1"/>
<smd name="2" x="1.15" y="0" dx="1.5" dy="4" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-0.762" y1="-2.032" x2="-0.254" y2="2.032" layer="94"/>
<rectangle x1="0.254" y1="-2.032" x2="0.762" y2="2.032" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="RESISTOR-0OHM">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="3.81" y="1.524" size="1.778" layer="95" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;Value</text>
<wire x1="-0.762" y1="0.762" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
</symbol>
<symbol name="RESISTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="3.81" y="1.524" size="1.778" layer="95" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="FERRITE/INDUCTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="3.81" y="1.524" size="1.778" layer="95" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;Value</text>
<rectangle x1="-2.667" y1="-1.016" x2="2.667" y2="0.508" layer="94"/>
<rectangle x1="-2.667" y1="0.762" x2="2.667" y2="1.016" layer="94"/>
</symbol>
<symbol name="REGULATOR_EN_BYP">
<pin name="VIN" x="-10.16" y="2.54" length="short" direction="pwr" swaplevel="1"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<pin name="BYP" x="10.16" y="-2.54" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="VOUT" x="10.16" y="2.54" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="6.35" size="1.778" layer="95">&gt;Name</text>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96">&gt;Value</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<pin name="C" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-1.016" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.381" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.651" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.27" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.381" x2="1.651" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.651" y1="0.127" x2="2.032" y2="-0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="1.905" y2="0.127" width="0.254" layer="94"/>
<text x="-3.302" y="5.08" size="1.778" layer="95" rot="MR270">&gt;Name</text>
<text x="4.826" y="-3.048" size="1.778" layer="96" rot="R90">&gt;Value</text>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SWITCH">
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="B" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="95">&gt;Name</text>
</symbol>
<symbol name="UUSB">
<pin name="VCC" x="7.62" y="5.08" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="D-" x="7.62" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="D+" x="7.62" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="ID" x="7.62" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="7.62" y="-5.08" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="CHS" x="-2.54" y="-12.7" visible="off" length="middle" direction="pwr" rot="R90"/>
<wire x1="-5.08" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="8PIN-CHIP">
<pin name="4" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="5" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-10.16" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.858" y="4.064" size="1.778" layer="95">&gt;P1</text>
<text x="-6.858" y="1.524" size="1.778" layer="95">&gt;P2</text>
<text x="-6.858" y="-1.016" size="1.778" layer="95">&gt;P3</text>
<text x="-6.858" y="-3.556" size="1.778" layer="95">&gt;P4</text>
<text x="6.858" y="-3.556" size="1.778" layer="95" rot="MR0">&gt;P5</text>
<text x="-4.318" y="9.144" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.318" y="-8.636" size="1.778" layer="95">&gt;VALUE</text>
<pin name="6" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="7" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="8" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="6.858" y="-1.016" size="1.778" layer="95" rot="MR0">&gt;P6</text>
<text x="6.858" y="1.524" size="1.778" layer="95" rot="MR0">&gt;P7</text>
<text x="6.858" y="4.064" size="1.778" layer="95" rot="MR0">&gt;P8</text>
<wire x1="-7.62" y1="6.35" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="C" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1812[4532-METRIC]" package="1812[4532-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603[1608-METRIC]-0OHM" package="0603[1608-METRIC]-0OHM">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR-0OHM">
<gates>
<gate name="G$1" symbol="RESISTOR-0OHM" x="0" y="0"/>
</gates>
<devices>
<device name="-ON" package="0603[1608-METRIC]-0OHM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-OFF" package="0603[1608-METRIC]-0OHM-OFF">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>RESISTOR</description>
<gates>
<gate name="R" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603[1608-METRIC]-0OHM" package="0603[1608-METRIC]-0OHM">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE/INDUCTOR" prefix="L" uservalue="yes">
<description>FERRITE/INDUCTOR</description>
<gates>
<gate name="L" symbol="FERRITE/INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-12X12" package="12X12">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5X5" package="5X5">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.5/7.5" package="3.5/7.5">
<connects>
<connect gate="L" pin="P$1" pad="P$1"/>
<connect gate="L" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PS8D43" package="PS8D43">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X7" package="7X7">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3X3" package="3X3">
<connects>
<connect gate="L" pin="P$1" pad="1"/>
<connect gate="L" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR_EN_BYP" prefix="U" uservalue="yes">
<description>REGULATOR</description>
<gates>
<gate name="U" symbol="REGULATOR_EN_BYP" x="0" y="0"/>
</gates>
<devices>
<device name="-TPS7933" package="SOT-23-5">
<connects>
<connect gate="U" pin="BYP" pad="4"/>
<connect gate="U" pin="EN" pad="3"/>
<connect gate="U" pin="GND" pad="2"/>
<connect gate="U" pin="VIN" pad="1"/>
<connect gate="U" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>CRYSTAL</description>
<gates>
<gate name="Q" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="-HC49" package="HC49">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.2X1.5" package="3.2X1.5">
<connects>
<connect gate="Q" pin="1" pad="P$1"/>
<connect gate="Q" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>LED</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM" package="LED3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-SMD" package="LED2MM-SMD">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE-SWITCH" prefix="SW" uservalue="yes">
<description>TACTILE SWITCH,
FARNELL: 1703878</description>
<gates>
<gate name="SW" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="-TH" package="TACTILE-TH">
<connects>
<connect gate="SW" pin="A" pad="A@1 A@2"/>
<connect gate="SW" pin="B" pad="B@1 B@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FSM4JSMA" package="TACTILE-FSM4JSMA">
<connects>
<connect gate="SW" pin="A" pad="1 2"/>
<connect gate="SW" pin="B" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FSMSM" package="TACTILE-FSMSM">
<connects>
<connect gate="SW" pin="A" pad="1"/>
<connect gate="SW" pin="B" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UUSB">
<gates>
<gate name="G$1" symbol="UUSB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UUSB">
<connects>
<connect gate="G$1" pin="CHS" pad="G1 G2 G3 G4 G5 G6"/>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="8PIN-CHIP">
<gates>
<gate name="G$1" symbol="8PIN-CHIP" x="0" y="0"/>
</gates>
<devices>
<device name="-SOIC8" package="SOIC8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIP8" package="DIP8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_PINHEAD">
<packages>
<package name="PINHEADER-2.54-02X1">
<pad name="P$1" x="-1.27" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="1.27" y="0" drill="1.016"/>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="37"/>
<wire x1="-1.778" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.127" layer="37"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="37"/>
<text x="-2.54" y="1.778" size="1.27" layer="25">&gt;Name</text>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="38"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.127" layer="38"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="38"/>
<wire x1="-1.778" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="38"/>
</package>
<package name="PINHEADER-2.54-02X1_N">
<pad name="P$1" x="-1.27" y="0" drill="1.016"/>
<pad name="P$2" x="1.27" y="0" drill="1.016"/>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="37"/>
<wire x1="-1.778" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.127" layer="37"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="37"/>
<text x="-2.54" y="1.778" size="1.27" layer="25">&gt;Name</text>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="38"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="-0.508" width="0.127" layer="38"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="38"/>
<wire x1="-1.778" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="38"/>
</package>
<package name="PINHEADER-2.54-03X1">
<pad name="P$1" x="-2.54" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="0" y="0" drill="1.016"/>
<wire x1="-3.048" y1="1.27" x2="3.048" y2="1.27" width="0.127" layer="37"/>
<wire x1="-3.048" y1="-1.27" x2="3.048" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-3.81" y1="0.508" x2="-3.81" y2="-0.508" width="0.127" layer="37"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.127" layer="37"/>
<text x="-1.27" y="1.778" size="1.27" layer="25">&gt;Name</text>
<pad name="P$3" x="2.54" y="0" drill="1.016"/>
<wire x1="-3.048" y1="1.27" x2="3.048" y2="1.27" width="0.127" layer="38"/>
<wire x1="-3.81" y1="0.508" x2="-3.81" y2="-0.508" width="0.127" layer="38"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.127" layer="38"/>
<wire x1="-3.048" y1="-1.27" x2="3.048" y2="-1.27" width="0.127" layer="38"/>
</package>
<package name="PINHEADER-2.54-10X1">
<pad name="P$1" x="-11.43" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="-8.89" y="0" drill="1.016"/>
<pad name="P$3" x="-6.35" y="0" drill="1.016"/>
<pad name="P$4" x="-3.81" y="0" drill="1.016"/>
<wire x1="-11.938" y1="1.27" x2="11.938" y2="1.27" width="0.127" layer="37"/>
<wire x1="-11.938" y1="-1.27" x2="11.938" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-12.7" y1="-0.508" x2="-12.7" y2="0.508" width="0.127" layer="37"/>
<wire x1="12.7" y1="-0.508" x2="12.7" y2="0.508" width="0.127" layer="37"/>
<text x="-2.54" y="1.778" size="1.27" layer="25">&gt;Name</text>
<pad name="P$5" x="-1.27" y="0" drill="1.016"/>
<pad name="P$6" x="1.27" y="0" drill="1.016"/>
<pad name="P$7" x="3.81" y="0" drill="1.016"/>
<wire x1="-11.938" y1="1.27" x2="11.938" y2="1.27" width="0.127" layer="38"/>
<wire x1="-11.938" y1="-1.27" x2="11.938" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-12.7" y1="-0.508" x2="-12.7" y2="0.508" width="0.127" layer="38"/>
<wire x1="12.7" y1="-0.508" x2="12.7" y2="0.508" width="0.127" layer="38"/>
<pad name="P$8" x="6.35" y="0" drill="1.016"/>
<pad name="P$9" x="8.89" y="0" drill="1.016"/>
<pad name="P$10" x="11.43" y="0" drill="1.016"/>
</package>
<package name="PINHEADER-2.54-03X1_N">
<pad name="P$1" x="-2.54" y="0" drill="1.016"/>
<pad name="P$2" x="0" y="0" drill="1.016"/>
<wire x1="-3.048" y1="1.27" x2="3.048" y2="1.27" width="0.127" layer="37"/>
<wire x1="-3.048" y1="-1.27" x2="3.048" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-3.81" y1="0.508" x2="-3.81" y2="-0.508" width="0.127" layer="37"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.127" layer="37"/>
<text x="-1.27" y="1.778" size="1.27" layer="25">&gt;Name</text>
<pad name="P$3" x="2.54" y="0" drill="1.016"/>
<wire x1="-3.048" y1="1.27" x2="3.048" y2="1.27" width="0.127" layer="38"/>
<wire x1="-3.81" y1="0.508" x2="-3.81" y2="-0.508" width="0.127" layer="38"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.127" layer="38"/>
<wire x1="-3.048" y1="-1.27" x2="3.048" y2="-1.27" width="0.127" layer="38"/>
</package>
<package name="PINHEADER-2.54-04X1">
<pad name="P$1" x="-3.81" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="-1.27" y="0" drill="1.016"/>
<pad name="P$3" x="1.27" y="0" drill="1.016"/>
<pad name="P$4" x="3.81" y="0" drill="1.016"/>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="37"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="37"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="37"/>
<text x="-1.27" y="1.778" size="1.27" layer="25">&gt;Name</text>
<wire x1="-4.318" y1="1.27" x2="4.318" y2="1.27" width="0.127" layer="38"/>
<wire x1="-4.318" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="0.508" width="0.127" layer="38"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="0.508" width="0.127" layer="38"/>
</package>
<package name="PINHEADER-2.54-11X1">
<pad name="P$1" x="-12.7" y="0" drill="1.016" shape="square"/>
<pad name="P$2" x="-10.16" y="0" drill="1.016"/>
<pad name="P$3" x="-7.62" y="0" drill="1.016"/>
<pad name="P$4" x="-5.08" y="0" drill="1.016"/>
<wire x1="-13.208" y1="1.27" x2="13.208" y2="1.27" width="0.127" layer="37"/>
<wire x1="-13.208" y1="-1.27" x2="13.208" y2="-1.27" width="0.127" layer="37"/>
<wire x1="-13.97" y1="-0.508" x2="-13.97" y2="0.508" width="0.127" layer="37"/>
<wire x1="13.97" y1="-0.508" x2="13.97" y2="0.508" width="0.127" layer="37"/>
<text x="-3.81" y="1.778" size="1.27" layer="25">&gt;Name</text>
<pad name="P$5" x="-2.54" y="0" drill="1.016"/>
<pad name="P$6" x="0" y="0" drill="1.016"/>
<pad name="P$7" x="2.54" y="0" drill="1.016"/>
<wire x1="-13.208" y1="1.27" x2="13.208" y2="1.27" width="0.127" layer="38"/>
<wire x1="-13.208" y1="-1.27" x2="13.208" y2="-1.27" width="0.127" layer="38"/>
<wire x1="-13.97" y1="-0.508" x2="-13.97" y2="0.508" width="0.127" layer="38"/>
<wire x1="13.97" y1="-0.508" x2="13.97" y2="0.508" width="0.127" layer="38"/>
<pad name="P$8" x="5.08" y="0" drill="1.016"/>
<pad name="P$9" x="7.62" y="0" drill="1.016"/>
<pad name="P$10" x="10.16" y="0" drill="1.016"/>
<pad name="P$11" x="12.7" y="0" drill="1.016"/>
</package>
</packages>
<symbols>
<symbol name="PINHEADER-2.54-02X1">
<pin name="P$1" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">1</text>
<text x="-1.778" y="-1.016" size="1.778" layer="95">2</text>
</symbol>
<symbol name="PINHEADER-2.54-03X1">
<pin name="P$1" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">1</text>
<text x="-1.778" y="-1.016" size="1.778" layer="95">2</text>
<pin name="P$3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-3.556" size="1.778" layer="95">3</text>
</symbol>
<symbol name="PINHEADER-2.54-10X1">
<pin name="P$1" x="2.54" y="10.16" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
<text x="-2.54" y="13.97" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="9.144" size="1.778" layer="95">1</text>
<text x="-1.778" y="6.604" size="1.778" layer="95">2</text>
<pin name="P$3" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$4" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="4.064" size="1.778" layer="95">3</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">4</text>
<pin name="P$5" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$6" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95">5</text>
<text x="-1.778" y="-3.556" size="1.778" layer="95">6</text>
<pin name="P$7" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-6.096" size="1.778" layer="95">7</text>
<pin name="P$8" x="2.54" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$9" x="2.54" y="-10.16" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$10" x="2.54" y="-12.7" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-8.636" size="1.778" layer="95">8</text>
<text x="-1.778" y="-11.176" size="1.778" layer="95">9</text>
<text x="-2.794" y="-13.716" size="1.778" layer="95">10</text>
</symbol>
<symbol name="PINHEADER-2.54-04X1">
<pin name="P$1" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="8.89" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="4.064" size="1.778" layer="95">1</text>
<text x="-1.778" y="1.524" size="1.778" layer="95">2</text>
<pin name="P$3" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$4" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95">3</text>
<text x="-1.778" y="-3.556" size="1.778" layer="95">4</text>
</symbol>
<symbol name="PINHEADER-2.54-11X1">
<pin name="P$1" x="2.54" y="12.7" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$2" x="2.54" y="10.16" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="16.51" size="1.778" layer="95">&gt;Name</text>
<text x="-1.778" y="11.684" size="1.778" layer="95">1</text>
<text x="-1.778" y="9.144" size="1.778" layer="95">2</text>
<pin name="P$3" x="2.54" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$4" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="6.604" size="1.778" layer="95">3</text>
<text x="-1.778" y="4.064" size="1.778" layer="95">4</text>
<pin name="P$5" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$6" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="1.524" size="1.778" layer="95">5</text>
<text x="-1.778" y="-1.016" size="1.778" layer="95">6</text>
<pin name="P$7" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-3.556" size="1.778" layer="95">7</text>
<pin name="P$8" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$9" x="2.54" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P$10" x="2.54" y="-10.16" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="-6.096" size="1.778" layer="95">8</text>
<text x="-1.778" y="-8.636" size="1.778" layer="95">9</text>
<text x="-2.794" y="-11.176" size="1.778" layer="95">10</text>
<text x="-2.794" y="-13.716" size="1.778" layer="95">11</text>
<pin name="P$11" x="2.54" y="-12.7" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHEADER-2.54-02X1" prefix="JP" uservalue="yes">
<description>PINHEADER-2.54-02X1</description>
<gates>
<gate name="JP" symbol="PINHEADER-2.54-02X1" x="0" y="0"/>
</gates>
<devices>
<device name="02X1" package="PINHEADER-2.54-02X1">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="02X1N" package="PINHEADER-2.54-02X1_N">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEADER-2.54-03X1" prefix="JP" uservalue="yes">
<description>PINHEADER-2.54-03X1</description>
<gates>
<gate name="JP" symbol="PINHEADER-2.54-03X1" x="0" y="0"/>
</gates>
<devices>
<device name="03X1" package="PINHEADER-2.54-03X1">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
<connect gate="JP" pin="P$3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="03X1N" package="PINHEADER-2.54-03X1_N">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
<connect gate="JP" pin="P$3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEADER-2.54-10X1">
<description>PINHEADER-2.54-10X1</description>
<gates>
<gate name="G$1" symbol="PINHEADER-2.54-10X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEADER-2.54-10X1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEADER-2.54-04X1" prefix="JP" uservalue="yes">
<description>PINHEADER-2.54-04X1</description>
<gates>
<gate name="JP" symbol="PINHEADER-2.54-04X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEADER-2.54-04X1">
<connects>
<connect gate="JP" pin="P$1" pad="P$1"/>
<connect gate="JP" pin="P$2" pad="P$2"/>
<connect gate="JP" pin="P$3" pad="P$3"/>
<connect gate="JP" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEADER-2.54-11X1">
<gates>
<gate name="G$1" symbol="PINHEADER-2.54-11X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEADER-2.54-11X1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="prog">
<packages>
<package name="5X2">
<smd name="5" x="0" y="0" dx="3" dy="1.27" layer="1"/>
<smd name="3" x="0" y="2.54" dx="3" dy="1.27" layer="1"/>
<smd name="1" x="0" y="5.08" dx="3" dy="1.27" layer="1"/>
<smd name="7" x="0" y="-2.54" dx="3" dy="1.27" layer="1"/>
<smd name="9" x="0" y="-5.08" dx="3" dy="1.27" layer="1"/>
<smd name="10" x="0" y="-5.08" dx="3" dy="1.27" layer="16" rot="R180"/>
<smd name="8" x="0" y="-2.54" dx="3" dy="1.27" layer="16" rot="R180"/>
<smd name="6" x="0" y="0" dx="3" dy="1.27" layer="16" rot="R180"/>
<smd name="4" x="0" y="2.54" dx="3" dy="1.27" layer="16" rot="R180"/>
<smd name="2" x="0" y="5.08" dx="3" dy="1.27" layer="16" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="5X2">
<pin name="P$1" x="7.62" y="5.08" visible="off" length="middle" rot="R180"/>
<pin name="P$2" x="-7.62" y="5.08" visible="off" length="middle"/>
<pin name="P$3" x="7.62" y="2.54" visible="off" length="middle" rot="R180"/>
<pin name="P$4" x="-7.62" y="2.54" visible="off" length="middle"/>
<pin name="P$5" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<pin name="P$6" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="P$7" x="7.62" y="-2.54" visible="off" length="middle" rot="R180"/>
<pin name="P$8" x="-7.62" y="-2.54" visible="off" length="middle"/>
<pin name="P$9" x="7.62" y="-5.08" visible="off" length="middle" rot="R180"/>
<pin name="P$10" x="-7.62" y="-5.08" visible="off" length="middle"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5X2">
<gates>
<gate name="G$1" symbol="5X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="5X2">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_POWER">
<packages>
</packages>
<symbols>
<symbol name="+5V">
<pin name="+5V" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="SparkFun" deviceset="FT2232D" device=""/>
<part name="C9" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R9" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R10" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k5"/>
<part name="R11" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="C8" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="C7" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="C6" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="C5" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="C4" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C10" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="18pF"/>
<part name="C11" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="18pF"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R7" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k5"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="C3" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1"/>
<part name="R6" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="L1" library="REBANE" deviceset="FERRITE/INDUCTOR" device="-0603[1608-METRIC]"/>
<part name="R13" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R14" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R15" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R16" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R17" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R12" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R18" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R19" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="U1" library="REBANE" deviceset="REGULATOR_EN_BYP" device="-TPS7933"/>
<part name="JP4" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-02X1" device="02X1N"/>
<part name="JP3" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-03X1" device="03X1N"/>
<part name="C1" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="4.7uF"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="4.7uF"/>
<part name="J3" library="prog" deviceset="5X2" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="Q2" library="REBANE" deviceset="CRYSTAL" device="-HC49" value="6MHz"/>
<part name="J2" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-10X1" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R5" library="REBANE" deviceset="RESISTOR-0OHM" device="-OFF" value="N/A"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="LED1" library="REBANE" deviceset="LED" device="-0603[1608-METRIC]"/>
<part name="LED2" library="REBANE" deviceset="LED" device="-0603[1608-METRIC]"/>
<part name="LED3" library="REBANE" deviceset="LED" device="-0603[1608-METRIC]"/>
<part name="R1" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R2" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="R3" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="470"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="JP1" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-03X1" device="03X1"/>
<part name="SW1" library="REBANE" deviceset="TACTILE-SWITCH" device="-FSM4JSMA"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="JP2" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-04X1" device=""/>
<part name="J1" library="REBANE_PINHEAD" deviceset="PINHEADER-2.54-11X1" device=""/>
<part name="U$1" library="REBANE" deviceset="UUSB" device=""/>
<part name="P+8" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V6" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V1" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="P+4" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V7" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="P+9" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V8" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="P+5" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+7" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+1" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V2" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="P+3" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="U3" library="REBANE" deviceset="8PIN-CHIP" device="-SOIC8" value="M93C46">
<attribute name="P1" value="S"/>
<attribute name="P2" value="C"/>
<attribute name="P3" value="D"/>
<attribute name="P4" value="Q"/>
<attribute name="P5" value="VSS"/>
<attribute name="P6" value="ORG"/>
<attribute name="P7" value="DU"/>
<attribute name="P8" value="VCC"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="7.62" y1="7.62" x2="157.48" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="7.62" x2="157.48" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="27.94" x2="180.34" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="27.94" x2="180.34" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="180.34" y1="129.54" x2="7.62" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="129.54" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="172.72" x2="58.42" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="172.72" x2="58.42" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="134.62" x2="7.62" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="134.62" x2="7.62" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="63.5" y1="172.72" x2="134.62" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="172.72" x2="134.62" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="134.62" y1="134.62" x2="63.5" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="63.5" y1="134.62" x2="63.5" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="185.42" y1="129.54" x2="220.98" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="129.54" x2="220.98" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="220.98" y1="27.94" x2="185.42" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="185.42" y1="27.94" x2="185.42" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="139.7" y1="172.72" x2="254" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="172.72" x2="254" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="139.7" y1="134.62" x2="139.7" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="226.06" y1="129.54" x2="254" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="129.54" x2="254" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="254" y1="27.94" x2="226.06" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="226.06" y1="27.94" x2="226.06" y2="129.54" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="170.18" size="2.54" layer="94" font="vector" align="top-left">3V3 POWER</text>
<text x="66.04" y="170.18" size="2.54" layer="94" font="vector" align="top-left">JTAG HEADER</text>
<text x="187.96" y="127" size="2.54" layer="94" font="vector" align="top-left">LEDS</text>
<text x="142.24" y="170.18" size="2.54" layer="94" font="vector" align="top-left">JUMPERS</text>
<text x="228.6" y="127" size="2.54" layer="94" font="vector" align="top-left">IO HEADERS</text>
<text x="10.16" y="127" size="2.54" layer="94" font="vector" align="top-left">FT2232D</text>
</plain>
<instances>
<instance part="U2" gate="G$1" x="101.6" y="66.04" smashed="yes">
<attribute name="NAME" x="96.012" y="107.188" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="95.758" y="24.892" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C9" gate="C" x="66.04" y="91.44" smashed="yes">
<attribute name="NAME" x="64.77" y="91.694" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="91.694" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND1" gate="1" x="55.88" y="91.44" smashed="yes" rot="R270"/>
<instance part="GND3" gate="1" x="78.74" y="17.78" smashed="yes"/>
<instance part="R8" gate="R" x="48.26" y="81.28" smashed="yes">
<attribute name="NAME" x="45.212" y="81.534" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="51.308" y="81.534" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R9" gate="R" x="48.26" y="78.74" smashed="yes">
<attribute name="NAME" x="45.212" y="78.994" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="51.308" y="78.994" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R10" gate="R" x="66.04" y="73.66" smashed="yes">
<attribute name="NAME" x="62.992" y="74.168" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="69.088" y="74.168" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R11" gate="R" x="66.04" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="62.992" y="86.868" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="69.088" y="86.868" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C8" gate="C" x="66.04" y="93.98" smashed="yes">
<attribute name="NAME" x="64.77" y="94.234" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="94.234" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C7" gate="C" x="66.04" y="96.52" smashed="yes">
<attribute name="NAME" x="64.77" y="96.774" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="96.774" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="C" x="66.04" y="99.06" smashed="yes">
<attribute name="NAME" x="64.77" y="99.314" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="99.314" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="C" x="66.04" y="101.6" smashed="yes">
<attribute name="NAME" x="64.77" y="101.854" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="101.854" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C4" gate="C" x="66.04" y="104.14" smashed="yes">
<attribute name="NAME" x="64.77" y="104.394" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="104.394" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND4" gate="1" x="55.88" y="93.98" smashed="yes" rot="R270"/>
<instance part="GND5" gate="1" x="55.88" y="96.52" smashed="yes" rot="R270"/>
<instance part="GND6" gate="1" x="55.88" y="99.06" smashed="yes" rot="R270"/>
<instance part="GND7" gate="1" x="55.88" y="101.6" smashed="yes" rot="R270"/>
<instance part="GND8" gate="1" x="55.88" y="104.14" smashed="yes" rot="R270"/>
<instance part="C10" gate="C" x="66.04" y="68.58" smashed="yes">
<attribute name="NAME" x="64.77" y="69.088" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="69.088" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C11" gate="C" x="66.04" y="58.42" smashed="yes">
<attribute name="NAME" x="64.77" y="58.928" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="67.31" y="58.928" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND9" gate="1" x="55.88" y="68.58" smashed="yes" rot="R270"/>
<instance part="GND10" gate="1" x="55.88" y="58.42" smashed="yes" rot="R270"/>
<instance part="R7" gate="R" x="66.04" y="38.1" smashed="yes">
<attribute name="NAME" x="62.992" y="38.608" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="69.088" y="38.608" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND11" gate="1" x="15.24" y="17.78" smashed="yes"/>
<instance part="C3" gate="C" x="15.24" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="14.732" y="31.75" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="14.732" y="34.29" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R6" gate="R" x="35.56" y="53.34" smashed="yes">
<attribute name="NAME" x="32.512" y="53.848" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="38.608" y="53.848" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="L1" gate="L" x="40.64" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="40.132" y="101.092" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="43.942" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R13" gate="R" x="134.62" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="104.394" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="104.394" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R14" gate="R" x="134.62" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="101.854" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="101.854" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R15" gate="R" x="134.62" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="99.314" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="99.314" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R16" gate="R" x="134.62" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="96.774" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="96.774" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R17" gate="R" x="134.62" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="94.234" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="94.234" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R12" gate="R" x="147.32" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="146.812" y="111.252" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="146.812" y="117.348" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R18" gate="R" x="134.62" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="66.294" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="66.294" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R19" gate="R" x="134.62" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="131.572" y="63.754" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="137.668" y="63.754" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="U1" gate="U" x="33.02" y="152.4" smashed="yes">
<attribute name="NAME" x="27.94" y="158.75" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="JP4" gate="JP" x="157.48" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="156.718" y="125.222" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="JP3" gate="JP" x="228.6" y="152.4" smashed="yes">
<attribute name="NAME" x="226.06" y="158.75" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="C1" gate="C" x="15.24" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="14.732" y="143.51" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="14.732" y="146.05" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="15.24" y="137.16" smashed="yes"/>
<instance part="GND13" gate="1" x="33.02" y="137.16" smashed="yes"/>
<instance part="GND14" gate="1" x="50.8" y="137.16" smashed="yes"/>
<instance part="C2" gate="C" x="50.8" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="50.292" y="143.51" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="50.292" y="146.05" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="J3" gate="G$1" x="96.52" y="149.86" smashed="yes" rot="R180"/>
<instance part="GND15" gate="1" x="15.24" y="71.12" smashed="yes"/>
<instance part="GND17" gate="1" x="27.94" y="71.12" smashed="yes"/>
<instance part="GND18" gate="1" x="68.58" y="154.94" smashed="yes" rot="R270"/>
<instance part="Q2" gate="Q" x="76.2" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="73.914" y="64.008" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="73.914" y="61.722" size="1.778" layer="96" font="vector" rot="MR0"/>
</instance>
<instance part="J2" gate="G$1" x="233.68" y="58.42" smashed="yes">
<attribute name="NAME" x="231.14" y="72.39" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND2" gate="1" x="246.38" y="114.3" smashed="yes" rot="R90"/>
<instance part="GND19" gate="1" x="246.38" y="68.58" smashed="yes" rot="R90"/>
<instance part="R5" gate="G$1" x="33.02" y="81.28" smashed="yes" rot="R270">
<attribute name="NAME" x="32.512" y="78.232" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="32.512" y="84.328" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="33.02" y="71.12" smashed="yes"/>
<instance part="LED1" gate="LED" x="195.58" y="68.58" smashed="yes">
<attribute name="NAME" x="195.072" y="66.802" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="197.866" y="70.612" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED2" gate="LED" x="203.2" y="68.58" smashed="yes">
<attribute name="NAME" x="202.692" y="66.802" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="205.486" y="70.612" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED3" gate="LED" x="210.82" y="68.58" smashed="yes">
<attribute name="NAME" x="210.312" y="66.802" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="213.106" y="70.612" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="R" x="195.58" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="195.072" y="83.312" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="195.072" y="89.408" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R2" gate="R" x="203.2" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="202.692" y="83.312" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="202.692" y="89.408" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R3" gate="R" x="210.82" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="210.312" y="83.312" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="210.312" y="89.408" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND21" gate="1" x="195.58" y="40.64" smashed="yes"/>
<instance part="GND22" gate="1" x="203.2" y="40.64" smashed="yes"/>
<instance part="JP1" gate="JP" x="152.4" y="152.4" smashed="yes">
<attribute name="NAME" x="149.86" y="158.75" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="SW1" gate="SW" x="200.66" y="152.4" smashed="yes">
<attribute name="NAME" x="200.152" y="155.448" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND23" gate="1" x="193.04" y="154.94" smashed="yes" rot="R90"/>
<instance part="GND24" gate="1" x="165.1" y="154.94" smashed="yes" rot="R90"/>
<instance part="R4" gate="R" x="200.66" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="197.612" y="150.368" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="203.708" y="150.368" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="JP2" gate="JP" x="185.42" y="149.86" smashed="yes">
<attribute name="NAME" x="182.88" y="158.75" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="J1" gate="G$1" x="233.68" y="101.6" smashed="yes">
<attribute name="NAME" x="231.14" y="118.11" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="U$1" gate="G$1" x="17.78" y="91.44" smashed="yes"/>
<instance part="P+8" gate="1" x="15.24" y="165.1"/>
<instance part="+3V6" gate="G$1" x="50.8" y="165.1"/>
<instance part="+3V1" gate="G$1" x="129.54" y="144.78" rot="R270"/>
<instance part="P+4" gate="1" x="129.54" y="147.32" rot="R270"/>
<instance part="+3V7" gate="G$1" x="195.58" y="119.38"/>
<instance part="P+9" gate="1" x="248.92" y="154.94" rot="R270"/>
<instance part="+3V8" gate="G$1" x="248.92" y="149.86" rot="R270"/>
<instance part="P+5" gate="1" x="40.64" y="119.38"/>
<instance part="P+7" gate="1" x="78.74" y="119.38"/>
<instance part="P+1" gate="1" x="246.38" y="45.72" rot="R270"/>
<instance part="+3V2" gate="G$1" x="246.38" y="48.26" rot="R270"/>
<instance part="P+3" gate="1" x="15.24" y="63.5"/>
<instance part="U3" gate="G$1" x="35.56" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.528" y="48.768" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="30.988" y="35.052" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P1" x="42.418" y="44.704" size="1.778" layer="95" rot="MR0"/>
<attribute name="P2" x="42.418" y="42.164" size="1.778" layer="95" rot="MR0"/>
<attribute name="P3" x="42.418" y="39.624" size="1.778" layer="95" rot="MR0"/>
<attribute name="P4" x="42.418" y="37.084" size="1.778" layer="95" rot="MR0"/>
<attribute name="P5" x="28.702" y="37.084" size="1.778" layer="95"/>
<attribute name="P6" x="28.702" y="39.624" size="1.778" layer="95"/>
<attribute name="P7" x="28.702" y="42.164" size="1.778" layer="95"/>
<attribute name="P8" x="28.702" y="44.704" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3V3OUT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="3V3OUT"/>
<pinref part="C9" gate="C" pin="2"/>
<wire x1="68.58" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SI/WUA"/>
<wire x1="124.46" y1="71.12" x2="177.038" y2="71.12" width="0.1524" layer="91"/>
<label x="177.038" y="71.12" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SI/WUB"/>
<wire x1="124.46" y1="33.02" x2="177.038" y2="33.02" width="0.1524" layer="91"/>
<label x="177.038" y="33.02" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C9" gate="C" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="58.42" y1="91.44" x2="63.5" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="TEST"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="81.28" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="AGND"/>
<wire x1="78.74" y1="38.1" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="35.56" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<wire x1="78.74" y1="33.02" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<wire x1="81.28" y1="38.1" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND1"/>
<wire x1="81.28" y1="35.56" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND2"/>
<wire x1="81.28" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND3"/>
<wire x1="81.28" y1="30.48" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND4"/>
<wire x1="81.28" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<junction x="78.74" y="38.1"/>
<junction x="78.74" y="35.56"/>
<junction x="78.74" y="33.02"/>
<junction x="78.74" y="30.48"/>
<junction x="78.74" y="27.94"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C4" gate="C" pin="1"/>
<wire x1="58.42" y1="104.14" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="58.42" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C6" gate="C" pin="1"/>
<wire x1="58.42" y1="99.06" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C7" gate="C" pin="1"/>
<wire x1="58.42" y1="96.52" x2="63.5" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C8" gate="C" pin="1"/>
<wire x1="58.42" y1="93.98" x2="63.5" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C10" gate="C" pin="1"/>
<wire x1="58.42" y1="68.58" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C11" gate="C" pin="1"/>
<wire x1="58.42" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="22.86" y1="27.94" x2="15.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="15.24" y1="27.94" x2="15.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C3" gate="C" pin="1"/>
<wire x1="15.24" y1="30.48" x2="15.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="15.24" y="27.94"/>
<pinref part="U3" gate="G$1" pin="5"/>
<wire x1="25.4" y1="38.1" x2="22.86" y2="38.1" width="0.1524" layer="91"/>
<wire x1="22.86" y1="38.1" x2="22.86" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="15.24" y1="139.7" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="U" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="33.02" y1="139.7" x2="33.02" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="50.8" y1="139.7" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="15.24" y1="73.66" x2="15.24" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="CHS"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="25.4" y1="86.36" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$9"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="71.12" y1="154.94" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="236.22" y1="114.3" x2="243.84" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="243.84" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="P$2"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED1" gate="LED" pin="C"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="195.58" y1="43.18" x2="195.58" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED2" gate="LED" pin="C"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="203.2" y1="43.18" x2="203.2" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="190.5" y1="154.94" x2="187.96" y2="154.94" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="P$1"/>
</segment>
<segment>
<pinref part="JP1" gate="JP" pin="P$1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="162.56" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="76.2" y1="66.04" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="76.2" y1="68.58" x2="78.74" y2="68.58" width="0.1524" layer="91"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="XTIN"/>
<wire x1="78.74" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C10" gate="C" pin="2"/>
<wire x1="68.58" y1="68.58" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<junction x="76.2" y="68.58"/>
<pinref part="Q2" gate="Q" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="XTOUT"/>
<wire x1="81.28" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C11" gate="C" pin="2"/>
<wire x1="68.58" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<junction x="76.2" y="58.42"/>
<pinref part="Q2" gate="Q" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R8" gate="R" pin="P$2"/>
<pinref part="U2" gate="G$1" pin="USBDM"/>
<wire x1="53.34" y1="81.28" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R9" gate="R" pin="P$2"/>
<pinref part="U2" gate="G$1" pin="USBDP"/>
<wire x1="53.34" y1="78.74" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R10" gate="R" pin="P$1"/>
<wire x1="55.88" y1="78.74" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="73.66" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="55.88" y1="73.66" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<junction x="55.88" y="78.74"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="RSTOUT#"/>
<pinref part="R10" gate="R" pin="P$2"/>
<wire x1="81.28" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="RESET#"/>
<wire x1="81.28" y1="71.12" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="78.74" y1="71.12" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC1"/>
<wire x1="78.74" y1="83.82" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="101.6" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<wire x1="81.28" y1="104.14" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC2"/>
<wire x1="81.28" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="104.14" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="78.74" y="104.14"/>
<junction x="78.74" y="101.6"/>
<pinref part="R11" gate="R" pin="P$2"/>
<pinref part="C4" gate="C" pin="2"/>
<wire x1="68.58" y1="104.14" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C5" gate="C" pin="2"/>
<wire x1="68.58" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="78.74" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="83.82" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<junction x="78.74" y="83.82"/>
</segment>
<segment>
<pinref part="C3" gate="C" pin="2"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="15.24" y1="40.64" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<junction x="15.24" y="45.72"/>
<pinref part="R6" gate="R" pin="P$1"/>
<wire x1="15.24" y1="53.34" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<junction x="15.24" y="53.34"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="U3" gate="G$1" pin="8"/>
<wire x1="25.4" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="6"/>
<wire x1="25.4" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<junction x="15.24" y="40.64"/>
</segment>
<segment>
<pinref part="L1" gate="L" pin="P$2"/>
<wire x1="40.64" y1="109.22" x2="40.64" y2="111.76" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U1" gate="U" pin="EN"/>
<wire x1="22.86" y1="149.86" x2="15.24" y2="149.86" width="0.1524" layer="91"/>
<wire x1="15.24" y1="149.86" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="U" pin="VIN"/>
<wire x1="15.24" y1="154.94" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="154.94" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<junction x="15.24" y="154.94"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="15.24" y1="147.32" x2="15.24" y2="149.86" width="0.1524" layer="91"/>
<junction x="15.24" y="149.86"/>
<pinref part="P+8" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="JP3" gate="JP" pin="P$1"/>
<wire x1="241.3" y1="154.94" x2="231.14" y2="154.94" width="0.1524" layer="91"/>
<pinref part="P+9" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$10"/>
<wire x1="238.76" y1="45.72" x2="236.22" y2="45.72" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$4"/>
<wire x1="104.14" y1="147.32" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="AVCC"/>
<pinref part="R11" gate="R" pin="P$1"/>
<wire x1="81.28" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="76.2" y1="99.06" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="68.58" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="76.2" y="99.06"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EECS"/>
<wire x1="81.28" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="55.88" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
<wire x1="68.58" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EESK"/>
<wire x1="81.28" y1="53.34" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="43.18" x2="45.72" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EEDATA"/>
<wire x1="81.28" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="40.64" x2="45.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R7" gate="R" pin="P$2"/>
<wire x1="71.12" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="38.1" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<junction x="73.66" y="40.64"/>
<pinref part="U3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R7" gate="R" pin="P$1"/>
<pinref part="R6" gate="R" pin="P$2"/>
<wire x1="53.34" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="40.64" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="53.34" y="38.1"/>
<pinref part="U3" gate="G$1" pin="4"/>
<wire x1="45.72" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS0"/>
<pinref part="R13" gate="R" pin="P$2"/>
<wire x1="124.46" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS1"/>
<pinref part="R14" gate="R" pin="P$2"/>
<wire x1="124.46" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS2"/>
<pinref part="R15" gate="R" pin="P$2"/>
<wire x1="124.46" y1="99.06" x2="129.54" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS3"/>
<pinref part="R16" gate="R" pin="P$2"/>
<wire x1="124.46" y1="96.52" x2="129.54" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS4"/>
<pinref part="R17" gate="R" pin="P$2"/>
<wire x1="124.46" y1="93.98" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TCK/SWC/TX_A" class="0">
<segment>
<pinref part="R13" gate="R" pin="P$1"/>
<wire x1="139.7" y1="104.14" x2="177.038" y2="104.14" width="0.1524" layer="91"/>
<label x="177.038" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$7"/>
<wire x1="88.9" y1="152.4" x2="68.58" y2="152.4" width="0.1524" layer="91"/>
<label x="68.58" y="152.4" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="TDI/RX_A" class="0">
<segment>
<pinref part="R14" gate="R" pin="P$1"/>
<wire x1="139.7" y1="101.6" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
<label x="177.038" y="101.6" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="R12" gate="R" pin="P$1"/>
<wire x1="147.32" y1="101.6" x2="177.038" y2="101.6" width="0.1524" layer="91"/>
<wire x1="147.32" y1="109.22" x2="147.32" y2="101.6" width="0.1524" layer="91"/>
<junction x="147.32" y="101.6"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$10"/>
<wire x1="104.14" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<label x="121.92" y="154.94" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="TDO/SWD/RTS_A" class="0">
<segment>
<pinref part="R15" gate="R" pin="P$1"/>
<label x="177.038" y="99.06" size="1.778" layer="95" font="vector" rot="MR0"/>
<wire x1="139.7" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<pinref part="JP4" gate="JP" pin="P$1"/>
<wire x1="152.4" y1="99.06" x2="177.038" y2="99.06" width="0.1524" layer="91"/>
<wire x1="154.94" y1="119.38" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
<wire x1="152.4" y1="119.38" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<junction x="152.4" y="99.06"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$5"/>
<wire x1="88.9" y1="149.86" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<label x="68.58" y="149.86" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="TMS/CTS_A" class="0">
<segment>
<pinref part="R16" gate="R" pin="P$1"/>
<wire x1="139.7" y1="96.52" x2="177.038" y2="96.52" width="0.1524" layer="91"/>
<label x="177.038" y="96.52" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$8"/>
<wire x1="104.14" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="121.92" y="152.4" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="TRST/DTR_A" class="0">
<segment>
<pinref part="R17" gate="R" pin="P$1"/>
<wire x1="139.7" y1="93.98" x2="177.038" y2="93.98" width="0.1524" layer="91"/>
<label x="177.038" y="93.98" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$6"/>
<wire x1="104.14" y1="149.86" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
<label x="121.92" y="149.86" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="JP2" gate="JP" pin="P$4"/>
<wire x1="187.96" y1="147.32" x2="215.9" y2="147.32" width="0.1524" layer="91"/>
<label x="215.9" y="147.32" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS0"/>
<pinref part="R18" gate="R" pin="P$2"/>
<wire x1="124.46" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS1"/>
<pinref part="R19" gate="R" pin="P$2"/>
<wire x1="124.46" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_B" class="0">
<segment>
<pinref part="R18" gate="R" pin="P$1"/>
<wire x1="139.7" y1="66.04" x2="177.038" y2="66.04" width="0.1524" layer="91"/>
<label x="177.038" y="66.04" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$1"/>
<wire x1="88.9" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
<label x="68.58" y="144.78" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="RX_B" class="0">
<segment>
<pinref part="R19" gate="R" pin="P$1"/>
<wire x1="139.7" y1="63.5" x2="177.038" y2="63.5" width="0.1524" layer="91"/>
<label x="177.038" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$3"/>
<wire x1="88.9" y1="147.32" x2="68.58" y2="147.32" width="0.1524" layer="91"/>
<label x="68.58" y="147.32" size="1.778" layer="95" font="vector"/>
</segment>
</net>
<net name="VCCIO" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VCCIOA"/>
<wire x1="81.28" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C7" gate="C" pin="2"/>
<pinref part="U2" gate="G$1" pin="VCCIOB"/>
<wire x1="73.66" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="2"/>
<wire x1="73.66" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="73.66" y1="96.52" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="73.66" y="96.52"/>
<junction x="73.66" y="93.98"/>
</segment>
<segment>
<pinref part="JP3" gate="JP" pin="P$2"/>
<wire x1="231.14" y1="152.4" x2="248.92" y2="152.4" width="0.1524" layer="91"/>
<label x="248.92" y="152.4" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R12" gate="R" pin="P$2"/>
<wire x1="147.32" y1="119.38" x2="147.32" y2="121.92" width="0.1524" layer="91"/>
<pinref part="JP4" gate="JP" pin="P$2"/>
<wire x1="154.94" y1="121.92" x2="147.32" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="U" pin="VOUT"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="43.18" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="157.48" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<junction x="50.8" y="154.94"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="JP3" gate="JP" pin="P$3"/>
<wire x1="241.3" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$9"/>
<wire x1="238.76" y1="48.26" x2="236.22" y2="48.26" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R1" gate="R" pin="P$2"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="P$2"/>
<wire x1="104.14" y1="144.78" x2="121.92" y2="144.78" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="L1" gate="L" pin="P$1"/>
<wire x1="25.4" y1="96.52" x2="40.64" y2="96.52" width="0.1524" layer="91"/>
<wire x1="40.64" y1="96.52" x2="40.64" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="25.4" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<wire x1="40.64" y1="93.98" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R8" gate="R" pin="P$1"/>
<wire x1="40.64" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="D-"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="25.4" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="38.1" y1="91.44" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="P$1"/>
<wire x1="38.1" y1="78.74" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="D+"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="25.4" y1="88.9" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="P$1"/>
<wire x1="33.02" y1="88.9" x2="33.02" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="ID"/>
</segment>
</net>
<net name="AD5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS5"/>
<wire x1="124.46" y1="91.44" x2="177.038" y2="91.44" width="0.1524" layer="91"/>
<label x="177.038" y="91.44" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$8"/>
<wire x1="236.22" y1="50.8" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<label x="246.38" y="50.8" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="R2" gate="R" pin="P$2"/>
<wire x1="203.2" y1="91.44" x2="203.2" y2="119.38" width="0.1524" layer="91"/>
<label x="203.2" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
</net>
<net name="AD6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS6"/>
<wire x1="124.46" y1="88.9" x2="177.038" y2="88.9" width="0.1524" layer="91"/>
<label x="177.038" y="88.9" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$7"/>
<wire x1="236.22" y1="53.34" x2="246.38" y2="53.34" width="0.1524" layer="91"/>
<label x="246.38" y="53.34" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="AD7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS7"/>
<wire x1="124.46" y1="86.36" x2="177.038" y2="86.36" width="0.1524" layer="91"/>
<label x="177.038" y="86.36" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$6"/>
<wire x1="236.22" y1="55.88" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<label x="246.38" y="55.88" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="AC0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ACBUS0"/>
<wire x1="124.46" y1="81.28" x2="177.038" y2="81.28" width="0.1524" layer="91"/>
<label x="177.038" y="81.28" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$5"/>
<wire x1="236.22" y1="58.42" x2="246.38" y2="58.42" width="0.1524" layer="91"/>
<label x="246.38" y="58.42" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="AC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ACBUS1"/>
<wire x1="124.46" y1="78.74" x2="177.038" y2="78.74" width="0.1524" layer="91"/>
<label x="177.038" y="78.74" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$4"/>
<wire x1="236.22" y1="60.96" x2="246.38" y2="60.96" width="0.1524" layer="91"/>
<label x="246.38" y="60.96" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="AC2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ACBUS2"/>
<wire x1="124.46" y1="76.2" x2="177.038" y2="76.2" width="0.1524" layer="91"/>
<label x="177.038" y="76.2" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$3"/>
<wire x1="236.22" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<label x="246.38" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="AC3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ACBUS3"/>
<wire x1="124.46" y1="73.66" x2="177.038" y2="73.66" width="0.1524" layer="91"/>
<label x="177.038" y="73.66" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="P$2"/>
<wire x1="236.22" y1="66.04" x2="246.38" y2="66.04" width="0.1524" layer="91"/>
<label x="246.38" y="66.04" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="BD2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS2"/>
<wire x1="124.46" y1="60.96" x2="177.038" y2="60.96" width="0.1524" layer="91"/>
<label x="177.038" y="60.96" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="104.14" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
<label x="246.38" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$5"/>
</segment>
</net>
<net name="BD3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS3"/>
<wire x1="124.46" y1="58.42" x2="177.038" y2="58.42" width="0.1524" layer="91"/>
<label x="177.038" y="58.42" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="106.68" x2="246.38" y2="106.68" width="0.1524" layer="91"/>
<label x="246.38" y="106.68" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="BD4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS4"/>
<wire x1="124.46" y1="55.88" x2="177.038" y2="55.88" width="0.1524" layer="91"/>
<label x="177.038" y="55.88" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="P$11"/>
<wire x1="236.22" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<label x="246.38" y="88.9" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="BD5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS5"/>
<wire x1="124.46" y1="53.34" x2="177.038" y2="53.34" width="0.1524" layer="91"/>
<label x="177.038" y="53.34" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="91.44" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<label x="246.38" y="91.44" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$10"/>
</segment>
</net>
<net name="BD6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS6"/>
<wire x1="124.46" y1="50.8" x2="177.038" y2="50.8" width="0.1524" layer="91"/>
<label x="177.038" y="50.8" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="93.98" x2="246.38" y2="93.98" width="0.1524" layer="91"/>
<label x="246.38" y="93.98" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$9"/>
</segment>
</net>
<net name="BD7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS7"/>
<wire x1="124.46" y1="48.26" x2="177.038" y2="48.26" width="0.1524" layer="91"/>
<label x="177.038" y="48.26" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="96.52" x2="246.38" y2="96.52" width="0.1524" layer="91"/>
<label x="246.38" y="96.52" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="BC0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BCBUS0"/>
<wire x1="124.46" y1="43.18" x2="177.038" y2="43.18" width="0.1524" layer="91"/>
<label x="177.038" y="43.18" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="99.06" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<label x="246.38" y="99.06" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$7"/>
</segment>
</net>
<net name="BC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BCBUS1"/>
<wire x1="124.46" y1="40.64" x2="177.038" y2="40.64" width="0.1524" layer="91"/>
<label x="177.038" y="40.64" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="101.6" x2="246.38" y2="101.6" width="0.1524" layer="91"/>
<label x="246.38" y="101.6" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="BC2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BCBUS2"/>
<wire x1="124.46" y1="38.1" x2="177.038" y2="38.1" width="0.1524" layer="91"/>
<label x="177.038" y="38.1" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="109.22" x2="246.38" y2="109.22" width="0.1524" layer="91"/>
<label x="246.38" y="109.22" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="BC3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BCBUS3"/>
<wire x1="124.46" y1="35.56" x2="177.038" y2="35.56" width="0.1524" layer="91"/>
<label x="177.038" y="35.56" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="236.22" y1="111.76" x2="246.38" y2="111.76" width="0.1524" layer="91"/>
<label x="246.38" y="111.76" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="J1" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="LED1" gate="LED" pin="A"/>
<pinref part="R1" gate="R" pin="P$1"/>
<wire x1="195.58" y1="81.28" x2="195.58" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="LED2" gate="LED" pin="A"/>
<pinref part="R2" gate="R" pin="P$1"/>
<wire x1="203.2" y1="81.28" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LED3" gate="LED" pin="A"/>
<pinref part="R3" gate="R" pin="P$1"/>
<wire x1="210.82" y1="81.28" x2="210.82" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="SW1" gate="SW" pin="A"/>
<wire x1="195.58" y1="152.4" x2="187.96" y2="152.4" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="P$2"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="SW1" gate="SW" pin="B"/>
<wire x1="205.74" y1="152.4" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<wire x1="215.9" y1="152.4" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R4" gate="R" pin="P$1"/>
<wire x1="215.9" y1="149.86" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED+" class="0">
<segment>
<pinref part="R3" gate="R" pin="P$2"/>
<wire x1="210.82" y1="91.44" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.778" layer="95" font="vector" rot="MR270"/>
</segment>
<segment>
<pinref part="JP1" gate="JP" pin="P$3"/>
<wire x1="154.94" y1="149.86" x2="165.1" y2="149.86" width="0.1524" layer="91"/>
<label x="165.1" y="149.86" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="LED-" class="0">
<segment>
<pinref part="LED3" gate="LED" pin="C"/>
<wire x1="210.82" y1="66.04" x2="210.82" y2="40.64" width="0.1524" layer="91"/>
<label x="210.82" y="40.64" size="1.778" layer="95" font="vector" rot="R90"/>
</segment>
<segment>
<pinref part="JP1" gate="JP" pin="P$2"/>
<wire x1="154.94" y1="152.4" x2="165.1" y2="152.4" width="0.1524" layer="91"/>
<label x="165.1" y="152.4" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R4" gate="R" pin="P$2"/>
<wire x1="195.58" y1="149.86" x2="187.96" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="P$3"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
