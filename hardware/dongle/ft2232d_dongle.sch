<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="15" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="11" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="13" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="7" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="ATT_MISO" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="punktiir" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N" xrefpart="/%S.%C%R">
<libraries>
<library name="switch-dil">
<description>&lt;b&gt;DIL Switches and Code Switches&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="CTS-219-03J">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT "J" Bend&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<wire x1="-4.445" y1="3.225" x2="-3.89" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-3.225" x2="-4.445" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-3.89" y1="-3.225" x2="-3.195" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.195" y1="-3.225" x2="-4.445" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="2" x2="-1.79" y2="2" width="0.2032" layer="51"/>
<wire x1="-1.79" y1="2" x2="-1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="-2" x2="-3.29" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3.29" y1="-2" x2="-3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.42" y1="-2" x2="-3.195" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="4.42" y1="-3.225" x2="-4.445" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="4.42" y1="3.225" x2="4.42" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="4.42" y1="-3.225" x2="3.865" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="3.865" y1="3.225" x2="4.42" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="3.225" x2="4.42" y2="3.225" width="0.2032" layer="51"/>
<wire x1="1.19" y1="-3.225" x2="1.325" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-1.35" y1="-3.225" x2="-1.215" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.2032" layer="51"/>
<wire x1="0.75" y1="2" x2="0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="-2" x2="-0.75" y2="-2" width="0.2032" layer="51"/>
<wire x1="-0.75" y1="-2" x2="-0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="1.79" y1="2" x2="3.29" y2="2" width="0.2032" layer="51"/>
<wire x1="3.29" y1="2" x2="3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="-2" x2="1.79" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.79" y1="-2" x2="1.79" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.19" y1="3.225" x2="-1.325" y2="3.225" width="0.2032" layer="21"/>
<wire x1="1.35" y1="3.225" x2="1.215" y2="3.225" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="2" x="0" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="3" x="2.54" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="4" x="2.54" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="5" x="0" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="6" x="-2.54" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<text x="-5.31" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.04" y="2.275" size="0.6096" layer="51">ON</text>
<text x="-2.74" y="-2.85" size="0.6096" layer="51">1</text>
<text x="6.31" y="-3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.2" y="-2.85" size="0.6096" layer="51">2</text>
<text x="2.34" y="-2.85" size="0.6096" layer="51">3</text>
<rectangle x1="-3.29" y1="-4" x2="-1.79" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-2" x2="-1.79" y2="-0.5" layer="51"/>
<rectangle x1="1.79" y1="-4" x2="3.29" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-0.75" y1="-4" x2="0.75" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-0.75" y1="-2" x2="0.75" y2="-0.5" layer="51"/>
<rectangle x1="1.79" y1="-2" x2="3.29" y2="-0.5" layer="51"/>
<rectangle x1="1.79" y1="3.25" x2="3.29" y2="4" layer="51"/>
<rectangle x1="-3.29" y1="3.25" x2="-1.79" y2="4" layer="51"/>
<rectangle x1="-0.75" y1="3.25" x2="0.75" y2="4" layer="51"/>
</package>
<package name="CTS-219-03">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<wire x1="-4.445" y1="3.225" x2="-3.89" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-3.225" x2="-4.445" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-3.89" y1="-3.225" x2="-3.195" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.195" y1="-3.225" x2="-4.445" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="2" x2="-1.79" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="2" x2="-1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="-2" x2="-3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="-2" x2="-3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.42" y1="-2" x2="-3.195" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="4.42" y1="-3.225" x2="-4.445" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="4.42" y1="3.225" x2="4.42" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="4.42" y1="-3.225" x2="3.865" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="3.865" y1="3.225" x2="4.42" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="3.225" x2="4.42" y2="3.225" width="0.2032" layer="51"/>
<wire x1="-1.215" y1="3.225" x2="-1.35" y2="3.225" width="0.2032" layer="21"/>
<wire x1="1.19" y1="-3.225" x2="1.325" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="1.325" y1="3.225" x2="1.19" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-1.35" y1="-3.225" x2="-1.215" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="2" x2="0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="-2" x2="-0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="-2" x2="-0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="1.79" y1="2" x2="3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="2" x2="3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="-2" x2="1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="1.79" y1="-2" x2="1.79" y2="2" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="2" x="0" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="3" x="2.54" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="4" x="2.54" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="5" x="0" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="6" x="-2.54" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<text x="-5.31" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.04" y="2.275" size="0.6096" layer="21">ON</text>
<text x="-2.74" y="-2.85" size="0.6096" layer="21">1</text>
<text x="6.31" y="-3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.2" y="-2.85" size="0.6096" layer="21">2</text>
<text x="2.34" y="-2.85" size="0.6096" layer="21">3</text>
<rectangle x1="-3.29" y1="-4" x2="-1.79" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-2" x2="-1.79" y2="-0.5" layer="21"/>
<rectangle x1="1.79" y1="-4" x2="3.29" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-0.75" y1="-4" x2="0.75" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-0.75" y1="-2" x2="0.75" y2="-0.5" layer="21"/>
<rectangle x1="1.79" y1="-2" x2="3.29" y2="-0.5" layer="21"/>
<rectangle x1="1.79" y1="3.25" x2="3.29" y2="4" layer="51"/>
<rectangle x1="-3.29" y1="3.25" x2="-1.79" y2="4" layer="51"/>
<rectangle x1="-0.75" y1="3.25" x2="0.75" y2="4" layer="51"/>
<rectangle x1="-2.79" y1="4" x2="-2.29" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="-2.79" y1="-4.95" x2="-2.29" y2="-4" layer="51"/>
<rectangle x1="-0.25" y1="4" x2="0.25" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="-0.25" y1="-4.95" x2="0.25" y2="-4" layer="51"/>
<rectangle x1="2.29" y1="4" x2="2.79" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="2.29" y1="-4.95" x2="2.79" y2="-4" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="DIP03">
<wire x1="4.445" y1="5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="4.445" y1="5.08" x2="-4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="-4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-4.445" y1="-5.08" x2="4.445" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.032" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-2.54" x2="-0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-2.54" x2="-0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="2.54" x2="3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="2.032" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="6.985" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-2.794" y="-4.064" size="0.9906" layer="94" ratio="14">1</text>
<text x="-0.381" y="-4.064" size="0.9906" layer="94" ratio="14">2</text>
<text x="2.159" y="-4.064" size="0.9906" layer="94" ratio="14">3</text>
<text x="-3.302" y="3.048" size="0.9906" layer="94" ratio="14">ON</text>
<rectangle x1="-2.794" y1="-2.286" x2="-2.286" y2="0" layer="94"/>
<rectangle x1="-0.254" y1="-2.286" x2="0.254" y2="0" layer="94"/>
<rectangle x1="2.286" y1="-2.286" x2="2.794" y2="0" layer="94"/>
<pin name="4" x="2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="5" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="6" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="219-03*" prefix="S">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<gates>
<gate name="G$1" symbol="DIP03" x="0" y="0"/>
</gates>
<devices>
<device name="J" package="CTS-219-03J">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="CTS-219-03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE">
<packages>
<package name="0603[1608-METRIC]">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="SOT-23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;&lt;p&gt;
package type OT</description>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-2.1" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-1.6" y1="-1.9" x2="1.6" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.9" x2="1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.9" x2="0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="0.6" y1="1.9" x2="0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="0.6" y1="0.7" x2="-0.6" y2="0.7" width="0.2" layer="51"/>
<wire x1="-0.6" y1="0.7" x2="-0.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-0.6" y1="1.9" x2="-1.6" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.9" x2="-1.6" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-0.4" y1="0.8" x2="0.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.2" layer="21"/>
</package>
<package name="SOIC-8">
<smd name="1" x="-1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.635" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.905" y="-2.69" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="-0.635" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="-1.905" y="2.71" dx="0.6" dy="1.55" layer="1"/>
<text x="-2.9" y="-1.9" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="-2.7" y="-1.8" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
<wire x1="-2.5" y1="1.6" x2="2.5" y2="1.6" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.6" x2="2.5" y2="-1.6" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.6" x2="-2" y2="-1.6" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.5" y2="-1.605" width="0.2" layer="21"/>
<wire x1="-2.5" y1="1.6" x2="-2.5" y2="-1.1" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.1" x2="-2.5" y2="-1.605" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.5" y2="-1.1" width="0.2" layer="21"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.6" y1="-1.7" x2="-2.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.6" y1="1.7" x2="-2.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="-2.3" y1="1.7" x2="-2.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="-2.3" y1="3.5" x2="-1.6" y2="3.5" width="0.2" layer="51"/>
<wire x1="-1.6" y1="3.5" x2="-1.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="1.7" x2="-1" y2="1.7" width="0.2" layer="51"/>
<wire x1="-1" y1="1.7" x2="-1" y2="3.5" width="0.2" layer="51"/>
<wire x1="-1" y1="3.5" x2="-0.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="3.5" x2="-0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="-0.3" y1="1.7" x2="0.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="0.3" y1="1.7" x2="0.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="3.5" x2="1" y2="3.5" width="0.2" layer="51"/>
<wire x1="1" y1="3.5" x2="1" y2="1.7" width="0.2" layer="51"/>
<wire x1="1" y1="1.7" x2="1.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="1.7" x2="1.6" y2="3.5" width="0.2" layer="51"/>
<wire x1="1.6" y1="3.5" x2="2.3" y2="3.5" width="0.2" layer="51"/>
<wire x1="2.3" y1="3.5" x2="2.3" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.7" x2="2.6" y2="1.7" width="0.2" layer="51"/>
<wire x1="2.6" y1="1.7" x2="2.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2.6" y1="-1.7" x2="2.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.7" x2="2.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="2.3" y1="-3.5" x2="1.6" y2="-3.5" width="0.2" layer="51"/>
<wire x1="1.6" y1="-3.5" x2="1.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="1.6" y1="-1.7" x2="1" y2="-1.7" width="0.2" layer="51"/>
<wire x1="1" y1="-1.7" x2="1" y2="-3.5" width="0.2" layer="51"/>
<wire x1="1" y1="-3.5" x2="0.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="0.3" y1="-3.5" x2="0.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="0.3" y1="-1.7" x2="-0.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-1.7" x2="-0.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-0.3" y1="-3.5" x2="-1" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-1" y1="-3.5" x2="-1" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-1" y1="-1.7" x2="-1.6" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-1.7" x2="-1.6" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-1.6" y1="-3.5" x2="-2.3" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-3.5" x2="-2.3" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-1.7" x2="-2.6" y2="-1.7" width="0.2" layer="51"/>
<circle x="-2" y="-1.2" radius="0.282840625" width="0.2" layer="51"/>
</package>
<package name="7X5">
<wire x1="-3.9" y1="2.7" x2="3.9" y2="2.7" width="0.2032" layer="21"/>
<wire x1="3.9" y1="0.3" x2="3.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.7" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="0.3" x2="-3.9" y2="-0.3" width="0.2032" layer="21"/>
<smd name="1" x="-3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<smd name="3" x="3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="4" x="-3" y="1.27" dx="2" dy="1.4" layer="1"/>
<smd name="2" x="3" y="-1.27" dx="2" dy="1.4" layer="1"/>
<text x="-1.4" y="3.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="2.7" x2="3.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="-2.3" x2="-3.9" y2="-2.7" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="2.7" x2="-3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="-3.9" y1="-2.7" x2="3.9" y2="-2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="-2.7" x2="3.9" y2="2.7" width="0.2" layer="51"/>
<wire x1="3.9" y1="2.7" x2="-3.9" y2="2.7" width="0.2" layer="51"/>
<text x="-1.4" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="0603[1608-METRIC]-DIODE">
<smd name="C" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="A" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.5" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-0.3" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.9" y1="0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.9" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-0.6" y2="0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0.3" x2="-0.6" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.6" y2="-0.3" width="0.1" layer="51"/>
<wire x1="-0.6" y1="-0.3" x2="-0.9" y2="0" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0" x2="-1" y2="0" width="0.1" layer="51"/>
<wire x1="-0.6" y1="0" x2="-0.5" y2="0" width="0.1" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.4" x2="0.2" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.2" x2="0.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0" y2="0.1" width="0.127" layer="21"/>
<wire x1="0" y1="0.1" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.3" x2="0.1" y2="-0.3" width="0.127" layer="21"/>
</package>
<package name="0402[1005-METRIC]">
<smd name="P$1" x="-0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="P$2" x="0.45" y="0" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.6" y="-0.2" size="0.4064" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.85" y1="0.5" x2="-0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.85" y1="-0.5" x2="0.85" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="-0.5" x2="0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="0.85" y1="0.5" x2="-0.85" y2="0.5" width="0.2" layer="21"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="0805[2012-METRIC]">
<smd name="P$1" x="-1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="1.1" y="0" dx="1.4" dy="1" layer="1" rot="R90"/>
<text x="-1.6" y="1.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2" layer="51"/>
<text x="-1.2" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="21"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="21"/>
</package>
<package name="0201[0603-METRIC]">
<smd name="P$1" x="-0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="P$2" x="0.25" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<text x="-1.6" y="0.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-0.5" y="-0.15" size="0.3048" layer="51" font="vector">&gt;Name</text>
<wire x1="-0.75" y1="0.45" x2="0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="0.45" x2="0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="0.75" y1="-0.45" x2="-0.75" y2="-0.45" width="0.2" layer="21"/>
<wire x1="-0.75" y1="-0.45" x2="-0.75" y2="0.45" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.4" x2="0.7" y2="0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="0.4" x2="0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="0.7" y1="-0.4" x2="-0.7" y2="-0.4" width="0.2" layer="51"/>
<wire x1="-0.7" y1="-0.4" x2="-0.7" y2="0.4" width="0.2" layer="51"/>
</package>
<package name="1206[3216-METRIC]">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<text x="-1.2" y="1.5" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="P$2" x="1.5" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<wire x1="-2.3" y1="-1.1" x2="2.3" y2="-1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="-1.1" x2="2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="2.3" y1="1.1" x2="-2.3" y2="1.1" width="0.2" layer="51"/>
<wire x1="-2.3" y1="1.1" x2="-2.3" y2="-1.1" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1.3" x2="2.5" y2="1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="1.3" x2="2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="2.5" y1="-1.3" x2="-2.5" y2="-1.3" width="0.2" layer="21"/>
<wire x1="-2.5" y1="-1.3" x2="-2.5" y2="1.3" width="0.2" layer="21"/>
</package>
<package name="0603[1608-METRIC]-0OHM-ON">
<smd name="P$1" x="-0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="1" dy="0.7" layer="1" rot="R90"/>
<text x="-1.1" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<polygon width="0" layer="29">
<vertex x="-1.2" y="0.6"/>
<vertex x="1.2" y="0.6"/>
<vertex x="1.2" y="-0.6"/>
<vertex x="-1.2" y="-0.6"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="-0.4" y="0.4"/>
<vertex x="0" y="0"/>
<vertex x="-0.4" y="-0.4"/>
<vertex x="-1" y="-0.4"/>
<vertex x="-1" y="0.4"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="0.15" y="0.4"/>
<vertex x="1" y="0.4"/>
<vertex x="1" y="-0.4"/>
<vertex x="0.15" y="-0.4"/>
<vertex x="0.45" y="-0.1"/>
<vertex x="0.45" y="0.1"/>
</polygon>
<polygon width="0.2" layer="41">
<vertex x="-0.3" y="0.4"/>
<vertex x="-0.3" y="-0.4"/>
<vertex x="0.3" y="-0.4"/>
<vertex x="0.3" y="0.4"/>
</polygon>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.2" layer="51"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.2" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.2" layer="21"/>
</package>
<package name="DIP-8">
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="0.6" width="0.2" layer="21" curve="180"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="5.1" y1="-2.8" x2="5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="-5.1" y1="2.8" x2="-5.1" y2="0.6" width="0.2" layer="21"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-5.7" y="-1.5" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<wire x1="-4.8" y1="2.8" x2="-5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="4.8" y1="2.8" x2="5.1" y2="2.8" width="0.2" layer="21"/>
<wire x1="4.8" y1="-2.8" x2="5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-4.8" y1="-2.8" x2="-5.1" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="0.6" width="0.2" layer="51" curve="180"/>
<wire x1="-5.1" y1="-0.6" x2="-5.1" y2="-3.8" width="0.2" layer="51"/>
<wire x1="-5.1" y1="3.8" x2="-5.1" y2="0.6" width="0.2" layer="51"/>
<wire x1="-5.1" y1="3.8" x2="5.1" y2="3.8" width="0.2" layer="51"/>
<wire x1="5.1" y1="3.8" x2="5.1" y2="-3.8" width="0.2" layer="51"/>
<wire x1="5.1" y1="-3.8" x2="-5.1" y2="-3.8" width="0.2" layer="51"/>
<text x="-1.7" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="3.2X2.5">
<wire x1="-0.6" y1="1.7" x2="0.6" y2="1.7" width="0.2" layer="21"/>
<wire x1="2" y1="0.3" x2="2" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.7" x2="-0.6" y2="-1.7" width="0.2" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="3" x="1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.1" y="0.9" dx="1.4" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="-0.9" dx="1.4" dy="1.2" layer="1"/>
<text x="-1" y="2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.2" layer="51"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.2" layer="51"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.2" layer="51"/>
<text x="-1.3" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2X1.6">
<wire x1="-0.6" y1="1.2" x2="0.6" y2="1.2" width="0.2" layer="21"/>
<wire x1="1.4" y1="0.3" x2="1.4" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.6" y1="-1.2" x2="-0.6" y2="-1.2" width="0.2" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="-0.3" width="0.2" layer="21"/>
<smd name="1" x="-0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="3" x="0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="4" x="-0.75" y="0.6" dx="0.7" dy="0.6" layer="1"/>
<smd name="2" x="0.75" y="-0.6" dx="0.7" dy="0.6" layer="1"/>
<text x="-1.5" y="1.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.4" y1="-1.2" x2="1.4" y2="-1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="1.4" y1="1.2" x2="-1.4" y2="1.2" width="0.2" layer="51"/>
<wire x1="-1.4" y1="1.2" x2="-1.4" y2="-1.2" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED3MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="2.2" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.2" width="0.2" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.1" y1="0.4" x2="-1.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-0.4" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.2" x2="-1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.2" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.3" y2="0.1" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0.1" x2="-1.3" y2="0" width="0.127" layer="51"/>
<wire x1="-1.3" y1="0" x2="-1.3" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-0.1" x2="-1.6" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0" x2="-1.3" y2="0" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.4" x2="-1.7" y2="-0.4" width="0.127" layer="51"/>
<text x="-0.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED5MM">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.6" y="0.9" size="0.8128" layer="25" font="vector">&gt;Name</text>
<circle x="0" y="0" radius="3.2" width="0.2" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.2" width="0.2" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.4" x2="-1.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.4" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.7" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.7" y1="0.2" x2="-1.7" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.7" y1="-0.2" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0.1" x2="-1.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.8" y1="0" x2="-1.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-1.8" y1="-0.1" x2="-2.1" y2="0" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0" x2="-1.8" y2="0" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.4" x2="-2.2" y2="-0.4" width="0.127" layer="51"/>
<text x="-1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED2MM-SMD">
<smd name="C" x="-1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<smd name="A" x="1.7" y="0" dx="1.2" dy="1.5" layer="1"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1" x2="1.6" y2="1" width="0.2" layer="21"/>
<wire x1="-1.6" y1="-1" x2="1.6" y2="-1" width="0.2" layer="21"/>
<wire x1="-0.7" y1="0.7" x2="-0.7" y2="-0.7" width="0.2" layer="21"/>
<text x="-1.2" y="1.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-2.5" y1="1" x2="-2.1" y2="1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1" x2="2.5" y2="1" width="0.2" layer="51"/>
<wire x1="2.5" y1="1" x2="2.5" y2="-1" width="0.2" layer="51"/>
<wire x1="2.5" y1="-1" x2="-2.1" y2="-1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="-1" x2="-2.5" y2="-1" width="0.2" layer="51"/>
<wire x1="-2.5" y1="-1" x2="-2.5" y2="1" width="0.2" layer="51"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.2" layer="51"/>
<text x="-1.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="LED5MM_ANGLE">
<pad name="A" x="1.27" y="0" drill="0.8128"/>
<pad name="C" x="-1.27" y="0" drill="0.8128"/>
<text x="-1.8" y="-4.1" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.2" y1="0" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="0.2" y1="0.2" x2="0.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="0.2" y1="-0.2" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="0" width="0.127" layer="51"/>
<wire x1="0.1" y1="0" x2="0.1" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.1" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="0.1" y2="0" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
<text x="-1.8" y="-5.2" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3.2" y1="-2" x2="-1.8" y2="-2" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-2" x2="-0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="-0.7" y1="-2" x2="0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="0.7" y1="-2" x2="1.8" y2="-2" width="0.2" layer="51"/>
<wire x1="1.8" y1="-2" x2="3.2" y2="-2" width="0.2" layer="51"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="3.2" y1="-2.8" x2="2.8" y2="-2.8" width="0.2" layer="51"/>
<wire x1="2.8" y1="-2.8" x2="2.8" y2="-7.2" width="0.2" layer="51"/>
<wire x1="2.8" y1="-7.2" x2="-2.8" y2="-7.2" width="0.2" layer="51" curve="-180"/>
<wire x1="-2.8" y1="-7.2" x2="-2.8" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-2.8" y1="-2.8" x2="-3.2" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-2" x2="3.2" y2="-2" width="0.2" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="3.2" y1="-2.8" x2="2.8" y2="-2.8" width="0.2" layer="21"/>
<wire x1="2.8" y1="-2.8" x2="2.8" y2="-7.2" width="0.2" layer="21"/>
<wire x1="2.8" y1="-7.2" x2="-2.8" y2="-7.2" width="0.2" layer="21" curve="-180"/>
<wire x1="-2.8" y1="-7.2" x2="-2.8" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-2.8" y1="-2.8" x2="-3.2" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-2.8" x2="-3.2" y2="-2" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-2" x2="-1.8" y2="0.5" width="0.2" layer="51"/>
<wire x1="-1.8" y1="0.5" x2="-0.7" y2="0.5" width="0.2" layer="51"/>
<wire x1="-0.7" y1="0.5" x2="-0.7" y2="-2" width="0.2" layer="51"/>
<wire x1="0.7" y1="-2" x2="0.7" y2="0.5" width="0.2" layer="51"/>
<wire x1="0.7" y1="0.5" x2="1.8" y2="0.5" width="0.2" layer="51"/>
<wire x1="1.8" y1="0.5" x2="1.8" y2="-2" width="0.2" layer="51"/>
</package>
<package name="LQFP-48">
<wire x1="-3.3" y1="3.3" x2="3.3" y2="3.3" width="0.2" layer="21"/>
<wire x1="3.3" y1="3.3" x2="3.3" y2="-3.3" width="0.2" layer="21"/>
<wire x1="3.3" y1="-3.3" x2="-2.4" y2="-3.3" width="0.2" layer="21"/>
<wire x1="-2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-2.4" width="0.2" layer="21"/>
<circle x="-2" y="-2" radius="0.6" width="0.2" layer="51"/>
<smd name="1" x="-2.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="2" x="-2.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="3" x="-1.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="4" x="-1.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="5" x="-0.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="6" x="-0.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="7" x="0.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="8" x="0.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="9" x="1.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="10" x="1.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="11" x="2.25" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="12" x="2.75" y="-4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="13" x="4.25" y="-2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="14" x="4.25" y="-2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="15" x="4.25" y="-1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="16" x="4.25" y="-1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="17" x="4.25" y="-0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="18" x="4.25" y="-0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="19" x="4.25" y="0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="20" x="4.25" y="0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="21" x="4.25" y="1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="22" x="4.25" y="1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="23" x="4.25" y="2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="24" x="4.25" y="2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="25" x="2.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="26" x="2.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="27" x="1.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="28" x="1.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="29" x="0.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="30" x="0.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="31" x="-0.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="32" x="-0.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="33" x="-1.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="34" x="-1.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="35" x="-2.25" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="36" x="-2.75" y="4.25" dx="0.254" dy="1.143" layer="1"/>
<smd name="37" x="-4.25" y="2.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="38" x="-4.25" y="2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="39" x="-4.25" y="1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="40" x="-4.25" y="1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="41" x="-4.25" y="0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="42" x="-4.25" y="0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="43" x="-4.25" y="-0.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="44" x="-4.25" y="-0.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="45" x="-4.25" y="-1.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="46" x="-4.25" y="-1.75" dx="1.143" dy="0.254" layer="1"/>
<smd name="47" x="-4.25" y="-2.25" dx="1.143" dy="0.254" layer="1"/>
<smd name="48" x="-4.25" y="-2.75" dx="1.143" dy="0.254" layer="1"/>
<text x="-2" y="5.3" size="0.8128" layer="25" font="vector">&gt;Name</text>
<rectangle x1="-2.85" y1="-4.5" x2="-2.65" y2="-3.45" layer="51"/>
<rectangle x1="-2.35" y1="-4.5" x2="-2.15" y2="-3.45" layer="51"/>
<rectangle x1="-1.85" y1="-4.5" x2="-1.65" y2="-3.45" layer="51"/>
<rectangle x1="-1.35" y1="-4.5" x2="-1.15" y2="-3.45" layer="51"/>
<rectangle x1="-0.85" y1="-4.5" x2="-0.65" y2="-3.45" layer="51"/>
<rectangle x1="-0.35" y1="-4.5" x2="-0.15" y2="-3.45" layer="51"/>
<rectangle x1="0.15" y1="-4.5" x2="0.35" y2="-3.45" layer="51"/>
<rectangle x1="0.65" y1="-4.5" x2="0.85" y2="-3.45" layer="51"/>
<rectangle x1="1.15" y1="-4.5" x2="1.35" y2="-3.45" layer="51"/>
<rectangle x1="1.65" y1="-4.5" x2="1.85" y2="-3.45" layer="51"/>
<rectangle x1="2.15" y1="-4.5" x2="2.35" y2="-3.45" layer="51"/>
<rectangle x1="2.65" y1="-4.5" x2="2.85" y2="-3.45" layer="51"/>
<rectangle x1="3.45" y1="-2.85" x2="4.5" y2="-2.65" layer="51"/>
<rectangle x1="3.45" y1="-2.35" x2="4.5" y2="-2.15" layer="51"/>
<rectangle x1="3.45" y1="-1.85" x2="4.5" y2="-1.65" layer="51"/>
<rectangle x1="3.45" y1="-1.35" x2="4.5" y2="-1.15" layer="51"/>
<rectangle x1="3.45" y1="-0.85" x2="4.5" y2="-0.65" layer="51"/>
<rectangle x1="3.45" y1="-0.35" x2="4.5" y2="-0.15" layer="51"/>
<rectangle x1="3.45" y1="0.15" x2="4.5" y2="0.35" layer="51"/>
<rectangle x1="3.45" y1="0.65" x2="4.5" y2="0.85" layer="51"/>
<rectangle x1="3.45" y1="1.15" x2="4.5" y2="1.35" layer="51"/>
<rectangle x1="3.45" y1="1.65" x2="4.5" y2="1.85" layer="51"/>
<rectangle x1="3.45" y1="2.15" x2="4.5" y2="2.35" layer="51"/>
<rectangle x1="3.45" y1="2.65" x2="4.5" y2="2.85" layer="51"/>
<rectangle x1="2.65" y1="3.45" x2="2.85" y2="4.5" layer="51"/>
<rectangle x1="2.15" y1="3.45" x2="2.35" y2="4.5" layer="51"/>
<rectangle x1="1.65" y1="3.45" x2="1.85" y2="4.5" layer="51"/>
<rectangle x1="1.15" y1="3.45" x2="1.35" y2="4.5" layer="51"/>
<rectangle x1="0.65" y1="3.45" x2="0.85" y2="4.5" layer="51"/>
<rectangle x1="0.15" y1="3.45" x2="0.35" y2="4.5" layer="51"/>
<rectangle x1="-0.35" y1="3.45" x2="-0.15" y2="4.5" layer="51"/>
<rectangle x1="-0.85" y1="3.45" x2="-0.65" y2="4.5" layer="51"/>
<rectangle x1="-1.35" y1="3.45" x2="-1.15" y2="4.5" layer="51"/>
<rectangle x1="-1.85" y1="3.45" x2="-1.65" y2="4.5" layer="51"/>
<rectangle x1="-2.35" y1="3.45" x2="-2.15" y2="4.5" layer="51"/>
<rectangle x1="-2.85" y1="3.45" x2="-2.65" y2="4.5" layer="51"/>
<rectangle x1="-4.5" y1="2.65" x2="-3.45" y2="2.85" layer="51"/>
<rectangle x1="-4.5" y1="2.15" x2="-3.45" y2="2.35" layer="51"/>
<rectangle x1="-4.5" y1="1.65" x2="-3.45" y2="1.85" layer="51"/>
<rectangle x1="-4.5" y1="1.15" x2="-3.45" y2="1.35" layer="51"/>
<rectangle x1="-4.5" y1="0.65" x2="-3.45" y2="0.85" layer="51"/>
<rectangle x1="-4.5" y1="0.15" x2="-3.45" y2="0.35" layer="51"/>
<rectangle x1="-4.5" y1="-0.35" x2="-3.45" y2="-0.15" layer="51"/>
<rectangle x1="-4.5" y1="-0.85" x2="-3.45" y2="-0.65" layer="51"/>
<rectangle x1="-4.5" y1="-1.35" x2="-3.45" y2="-1.15" layer="51"/>
<rectangle x1="-4.5" y1="-1.85" x2="-3.45" y2="-1.65" layer="51"/>
<rectangle x1="-4.5" y1="-2.35" x2="-3.45" y2="-2.15" layer="51"/>
<rectangle x1="-4.5" y1="-2.85" x2="-3.45" y2="-2.65" layer="51"/>
<wire x1="-3.3" y1="-2.4" x2="-3.3" y2="3.3" width="0.2" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.2" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.2" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.2" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.2" layer="51"/>
<text x="-1.8" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-3.3" y1="-2.4" x2="-2.4" y2="-3.3" width="0.2" layer="21"/>
<text x="-3.6" y="-3.6" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
</package>
<package name="1812[4532-METRIC]">
<smd name="P$1" x="-2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<smd name="P$2" x="2.25" y="0" dx="4" dy="2.1" layer="1" rot="R90"/>
<text x="-1.1" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-3.6" y1="2.3" x2="3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="2.3" x2="3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="3.6" y1="-2.3" x2="-3.6" y2="-2.3" width="0.2" layer="21"/>
<wire x1="-3.6" y1="-2.3" x2="-3.6" y2="2.3" width="0.2" layer="21"/>
<wire x1="-3.4" y1="2.1" x2="3.4" y2="2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="2.1" x2="3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="3.4" y1="-2.1" x2="-3.4" y2="-2.1" width="0.2" layer="51"/>
<wire x1="-3.4" y1="-2.1" x2="-3.4" y2="2.1" width="0.2" layer="51"/>
<text x="-1.1" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
<package name="2512[6332-METRIC]">
<text x="-1.2" y="2.7" size="0.8128" layer="25" font="vector">&gt;Name</text>
<smd name="P$1" x="-2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<smd name="P$2" x="2.6" y="0" dx="3.6" dy="2.4" layer="1" rot="R90"/>
<wire x1="-3.9" y1="-1.9" x2="3.9" y2="-1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="-1.9" x2="3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="3.9" y1="1.9" x2="-3.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="-3.9" y1="1.9" x2="-3.9" y2="-1.9" width="0.2" layer="51"/>
<text x="-1.5" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<wire x1="-4.1" y1="2.1" x2="4.1" y2="2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="2.1" x2="4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="4.1" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="2.1" width="0.2" layer="21"/>
</package>
<package name="SOT-23-8">
<wire x1="1.8" y1="0.8" x2="1.8" y2="-0.8" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-1.8" y2="-0.4" width="0.2" layer="21"/>
<smd name="1" x="-0.975" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="2" x="-0.325" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="3" x="0.325" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="8" x="-0.975" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<text x="-2.4" y="-1.7" size="0.8128" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="7" x="-0.325" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<text x="-2" y="-2" size="0.8128" layer="21" font="vector" ratio="30" rot="R180">&gt;o</text>
<wire x1="-1.8" y1="-0.4" x2="-1.8" y2="0.8" width="0.2" layer="21"/>
<wire x1="-1.9" y1="-1.9" x2="1.9" y2="-1.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="-1.9" x2="1.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="1.9" y1="1.9" x2="-1.9" y2="1.9" width="0.2" layer="51"/>
<wire x1="-1.9" y1="1.9" x2="-1.9" y2="-1.9" width="0.2" layer="51"/>
<circle x="-1.325" y="-1.3" radius="0.282840625" width="0.2" layer="51"/>
<text x="-1.3" y="-0.3" size="0.8128" layer="51" font="vector">&gt;Name</text>
<smd name="4" x="0.975" y="-1.3" dx="0.45" dy="1.2" layer="1"/>
<smd name="5" x="0.975" y="1.3" dx="0.45" dy="1.2" layer="1"/>
<wire x1="-1.8" y1="-0.4" x2="-1.1" y2="-0.4" width="0.2" layer="21"/>
</package>
<package name="RESONATOR">
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="21"/>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="51"/>
<wire x1="1.8" y1="1" x2="1.8" y2="-1" width="0.2" layer="51"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="51"/>
<wire x1="-1.8" y1="-1" x2="-1.8" y2="1" width="0.2" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.7" dy="1.6" layer="1"/>
<smd name="G" x="0" y="0" dx="0.7" dy="1.6" layer="1"/>
<smd name="2" x="1.2" y="0" dx="0.7" dy="1.6" layer="1"/>
<text x="-1.6" y="1.2" size="0.8128" layer="25" font="vector">&gt;Name</text>
<wire x1="-1.8" y1="1" x2="1.8" y2="1" width="0.2" layer="21"/>
<wire x1="1.8" y1="-1" x2="-1.8" y2="-1" width="0.2" layer="21"/>
<text x="-1.6" y="-0.4" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-0.762" y1="-2.032" x2="-0.254" y2="2.032" layer="94"/>
<rectangle x1="0.254" y1="-2.032" x2="0.762" y2="2.032" layer="94"/>
<text x="-1.27" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="1.27" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="RESISTOR">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="-2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.048" y="0.508" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="3.048" y="0.508" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="REGULATOR_EN_BYP">
<pin name="VIN" x="-10.16" y="2.54" length="short" direction="pas" swaplevel="1"/>
<pin name="EN" x="-10.16" y="-2.54" length="short" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="BYP" x="10.16" y="-2.54" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="VOUT" x="10.16" y="2.54" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="6.35" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-5.08" y="-13.97" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="8PIN-CHIP">
<pin name="4" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="5" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="1" x="-10.16" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="6.35" width="0.254" layer="94"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.858" y="4.064" size="1.778" layer="95" font="vector">&gt;P1</text>
<text x="-6.858" y="1.524" size="1.778" layer="95" font="vector">&gt;P2</text>
<text x="-6.858" y="-1.016" size="1.778" layer="95" font="vector">&gt;P3</text>
<text x="-6.858" y="-3.556" size="1.778" layer="95" font="vector">&gt;P4</text>
<text x="6.858" y="-3.556" size="1.778" layer="95" font="vector" rot="MR0">&gt;P5</text>
<text x="-4.318" y="9.144" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-3.302" y="-6.604" size="1.778" layer="95" font="vector" rot="MR180">&gt;Value</text>
<pin name="6" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="7" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="8" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<text x="6.858" y="-1.016" size="1.778" layer="95" font="vector" rot="MR0">&gt;P6</text>
<text x="6.858" y="1.524" size="1.778" layer="95" font="vector" rot="MR0">&gt;P7</text>
<text x="6.858" y="4.064" size="1.778" layer="95" font="vector" rot="MR0">&gt;P8</text>
<wire x1="-7.62" y1="6.35" x2="-6.35" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="CRYSTAL-GND">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95" font="vector" rot="MR0">&gt;Name</text>
<text x="-3.81" y="-4.572" size="1.778" layer="96" font="vector">&gt;Value</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-2.54" visible="off" length="point"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="LED">
<pin name="C" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-1.016" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.381" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="1.651" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.27" x2="2.032" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.381" x2="1.651" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.651" y1="0.127" x2="2.032" y2="-0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="2.54" y2="0.381" width="0.254" layer="94"/>
<wire x1="2.032" y1="0.762" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.032" y1="-0.381" x2="1.905" y2="0.127" width="0.254" layer="94"/>
<text x="-0.508" y="-1.778" size="1.778" layer="95" font="vector" rot="MR270">&gt;Name</text>
<text x="-0.508" y="1.778" size="1.778" layer="96" font="vector" rot="R90">&gt;Value</text>
<wire x1="0" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FT2232D">
<wire x1="-12.7" y1="40.64" x2="12.7" y2="40.64" width="0.254" layer="94"/>
<wire x1="12.7" y1="40.64" x2="12.7" y2="-40.64" width="0.254" layer="94"/>
<wire x1="12.7" y1="-40.64" x2="-12.7" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-40.64" x2="-12.7" y2="40.64" width="0.254" layer="94"/>
<text x="-5.08" y="43.18" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="7.62" y="-43.18" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
<pin name="3V3OUT" x="-15.24" y="25.4" length="short" direction="pas"/>
<pin name="USBDM" x="-15.24" y="15.24" length="short"/>
<pin name="USBDP" x="-15.24" y="12.7" length="short"/>
<pin name="GND1" x="-15.24" y="-30.48" length="short" direction="pas"/>
<pin name="GND2" x="-15.24" y="-33.02" length="short" direction="pas"/>
<pin name="GND3" x="-15.24" y="-35.56" length="short" direction="pas"/>
<pin name="BDBUS0" x="15.24" y="0" length="short" rot="R180"/>
<pin name="BDBUS1" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="BDBUS2" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="BDBUS3" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="BDBUS4" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="BDBUS5" x="15.24" y="-12.7" length="short" rot="R180"/>
<pin name="BDBUS6" x="15.24" y="-15.24" length="short" rot="R180"/>
<pin name="BDBUS7" x="15.24" y="-17.78" length="short" rot="R180"/>
<pin name="PWEN#" x="15.24" y="-38.1" length="short" rot="R180"/>
<pin name="VCCIOA" x="-15.24" y="30.48" length="short" direction="pas"/>
<pin name="AGND" x="-15.24" y="-27.94" length="short" direction="pas"/>
<pin name="TEST" x="-15.24" y="-17.78" length="short" direction="in"/>
<pin name="VCC2" x="-15.24" y="35.56" length="short" direction="pas"/>
<pin name="RSTOUT#" x="-15.24" y="7.62" length="short"/>
<pin name="RESET#" x="-15.24" y="5.08" length="short"/>
<pin name="XTIN" x="-15.24" y="0" length="short"/>
<pin name="XTOUT" x="-15.24" y="-2.54" length="short"/>
<pin name="EECS" x="-15.24" y="-10.16" length="short"/>
<pin name="EESK" x="-15.24" y="-12.7" length="short"/>
<pin name="EEDATA" x="-15.24" y="-15.24" length="short"/>
<pin name="GND4" x="-15.24" y="-38.1" length="short"/>
<pin name="SI/WUB" x="15.24" y="-33.02" length="short" rot="R180"/>
<pin name="BCBUS3" x="15.24" y="-30.48" length="short" rot="R180"/>
<pin name="BCBUS2" x="15.24" y="-27.94" length="short" rot="R180"/>
<pin name="BCBUS1" x="15.24" y="-25.4" length="short" rot="R180"/>
<pin name="BCBUS0" x="15.24" y="-22.86" length="short" rot="R180"/>
<pin name="ACBUS2" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="ACBUS1" x="15.24" y="12.7" length="short" rot="R180"/>
<pin name="ACBUS0" x="15.24" y="15.24" length="short" rot="R180"/>
<pin name="ADBUS7" x="15.24" y="20.32" length="short" rot="R180"/>
<pin name="ADBUS6" x="15.24" y="22.86" length="short" rot="R180"/>
<pin name="ADBUS5" x="15.24" y="25.4" length="short" rot="R180"/>
<pin name="SI/WUA" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="ACBUS3" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="ADBUS4" x="15.24" y="27.94" length="short" rot="R180"/>
<pin name="ADBUS3" x="15.24" y="30.48" length="short" rot="R180"/>
<pin name="ADBUS2" x="15.24" y="33.02" length="short" rot="R180"/>
<pin name="ADBUS1" x="15.24" y="35.56" length="short" rot="R180"/>
<pin name="ADBUS0" x="15.24" y="38.1" length="short" rot="R180"/>
<pin name="VCC1" x="-15.24" y="38.1" length="short" direction="pas"/>
<pin name="AVCC" x="-15.24" y="33.02" length="short" direction="pas"/>
<pin name="VCCIOB" x="-15.24" y="27.94" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="C" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1812[4532-METRIC]" package="1812[4532-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603[1608-METRIC]-0OHM" package="0603[1608-METRIC]-0OHM-ON">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="C" pin="1" pad="P$1"/>
<connect gate="C" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>RESISTOR</description>
<gates>
<gate name="R" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0402[1005-METRIC]" package="0402[1005-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805[2012-METRIC]" package="0805[2012-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0201[0603-METRIC]" package="0201[0603-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1206[3216-METRIC]" package="1206[3216-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2512[6332-METRIC]" package="2512[6332-METRIC]">
<connects>
<connect gate="R" pin="P$1" pad="P$1"/>
<connect gate="R" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR_EN_BYP" prefix="U" uservalue="yes">
<description>REGULATOR</description>
<gates>
<gate name="U" symbol="REGULATOR_EN_BYP" x="0" y="0"/>
</gates>
<devices>
<device name="-TPS7933" package="SOT-23-5">
<connects>
<connect gate="U" pin="BYP" pad="4"/>
<connect gate="U" pin="EN" pad="3"/>
<connect gate="U" pin="GND" pad="2"/>
<connect gate="U" pin="VIN" pad="1"/>
<connect gate="U" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="8PIN-CHIP">
<gates>
<gate name="G$1" symbol="8PIN-CHIP" x="0" y="0"/>
</gates>
<devices>
<device name="-SOIC-8" package="SOIC-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-DIP-8" package="DIP-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOT-23-8" package="SOT-23-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL-GND" prefix="Q">
<description>CRYSTAL</description>
<gates>
<gate name="Q" symbol="CRYSTAL-GND" x="0" y="0"/>
</gates>
<devices>
<device name="-3.2X2.5" package="3.2X2.5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2X1.6" package="2X1.6">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7X5" package="7X5">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="3"/>
<connect gate="Q" pin="GND" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RESONATOR" package="RESONATOR">
<connects>
<connect gate="Q" pin="1" pad="1"/>
<connect gate="Q" pin="2" pad="2"/>
<connect gate="Q" pin="GND" pad="G"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>LED</description>
<gates>
<gate name="LED" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="-0603[1608-METRIC]" package="0603[1608-METRIC]-DIODE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM" package="LED3MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM" package="LED5MM">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2MM-SMD" package="LED2MM-SMD">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM_ANGLE" package="LED5MM_ANGLE">
<connects>
<connect gate="LED" pin="A" pad="A"/>
<connect gate="LED" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT2232D">
<gates>
<gate name="G$1" symbol="FT2232D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP-48">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="6"/>
<connect gate="G$1" pin="ACBUS0" pad="15"/>
<connect gate="G$1" pin="ACBUS1" pad="13"/>
<connect gate="G$1" pin="ACBUS2" pad="12"/>
<connect gate="G$1" pin="ACBUS3" pad="11"/>
<connect gate="G$1" pin="ADBUS0" pad="24"/>
<connect gate="G$1" pin="ADBUS1" pad="23"/>
<connect gate="G$1" pin="ADBUS2" pad="22"/>
<connect gate="G$1" pin="ADBUS3" pad="21"/>
<connect gate="G$1" pin="ADBUS4" pad="20"/>
<connect gate="G$1" pin="ADBUS5" pad="19"/>
<connect gate="G$1" pin="ADBUS6" pad="17"/>
<connect gate="G$1" pin="ADBUS7" pad="16"/>
<connect gate="G$1" pin="AGND" pad="45"/>
<connect gate="G$1" pin="AVCC" pad="46"/>
<connect gate="G$1" pin="BCBUS0" pad="30"/>
<connect gate="G$1" pin="BCBUS1" pad="29"/>
<connect gate="G$1" pin="BCBUS2" pad="28"/>
<connect gate="G$1" pin="BCBUS3" pad="27"/>
<connect gate="G$1" pin="BDBUS0" pad="40"/>
<connect gate="G$1" pin="BDBUS1" pad="39"/>
<connect gate="G$1" pin="BDBUS2" pad="38"/>
<connect gate="G$1" pin="BDBUS3" pad="37"/>
<connect gate="G$1" pin="BDBUS4" pad="36"/>
<connect gate="G$1" pin="BDBUS5" pad="35"/>
<connect gate="G$1" pin="BDBUS6" pad="33"/>
<connect gate="G$1" pin="BDBUS7" pad="32"/>
<connect gate="G$1" pin="EECS" pad="48"/>
<connect gate="G$1" pin="EEDATA" pad="2"/>
<connect gate="G$1" pin="EESK" pad="1"/>
<connect gate="G$1" pin="GND1" pad="18"/>
<connect gate="G$1" pin="GND2" pad="25"/>
<connect gate="G$1" pin="GND3" pad="34"/>
<connect gate="G$1" pin="GND4" pad="9"/>
<connect gate="G$1" pin="PWEN#" pad="41"/>
<connect gate="G$1" pin="RESET#" pad="4"/>
<connect gate="G$1" pin="RSTOUT#" pad="5"/>
<connect gate="G$1" pin="SI/WUA" pad="10"/>
<connect gate="G$1" pin="SI/WUB" pad="26"/>
<connect gate="G$1" pin="TEST" pad="47"/>
<connect gate="G$1" pin="USBDM" pad="8"/>
<connect gate="G$1" pin="USBDP" pad="7"/>
<connect gate="G$1" pin="VCC1" pad="3"/>
<connect gate="G$1" pin="VCC2" pad="42"/>
<connect gate="G$1" pin="VCCIOA" pad="14"/>
<connect gate="G$1" pin="VCCIOB" pad="31"/>
<connect gate="G$1" pin="XTIN" pad="43"/>
<connect gate="G$1" pin="XTOUT" pad="44"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_POWER">
<packages>
</packages>
<symbols>
<symbol name="+5V">
<pin name="+5V" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.016" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<text x="0.635" y="-5.715" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-7.62" visible="off" length="point" direction="sup" rot="R90"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="1.016" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="+5V_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3_">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="newprog">
<packages>
<package name="5X2">
<smd name="5" x="0" y="0" dx="4" dy="1.27" layer="1"/>
<smd name="3" x="0" y="2.54" dx="4" dy="1.27" layer="1"/>
<smd name="1" x="0" y="4.78" dx="4" dy="0.9" layer="1"/>
<smd name="7" x="0" y="-2.54" dx="4" dy="1.27" layer="1"/>
<smd name="9" x="0" y="-4.78" dx="4" dy="0.9" layer="1"/>
<smd name="10" x="0" y="-4.78" dx="4" dy="0.9" layer="16" rot="R180"/>
<smd name="8" x="0" y="-2.54" dx="4" dy="1.27" layer="16" rot="R180"/>
<smd name="6" x="0" y="0" dx="4" dy="1.27" layer="16" rot="R180"/>
<smd name="4" x="0" y="2.54" dx="4" dy="1.27" layer="16" rot="R180"/>
<smd name="2" x="0" y="4.78" dx="4" dy="0.9" layer="16" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="5X2">
<pin name="P$1" x="5.08" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="P$2" x="-5.08" y="5.08" visible="off" length="short"/>
<pin name="P$3" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="P$4" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="P$5" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="P$6" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$7" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="P$8" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="P$9" x="5.08" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="P$10" x="-5.08" y="-5.08" visible="off" length="short"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5X2">
<gates>
<gate name="G$1" symbol="5X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="5X2">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="REBANE_HEADER_THT_2.54X1_MALE">
<packages>
<package name="HEADER_THT_2.54X1_MALE-05">
<pad name="1" x="-5.08" y="0" drill="1" shape="square"/>
<pad name="2" x="-2.54" y="0" drill="1"/>
<pad name="3" x="0" y="0" drill="1"/>
<pad name="4" x="2.54" y="0" drill="1"/>
<pad name="5" x="5.08" y="0" drill="1"/>
<wire x1="-6.35" y1="1.27" x2="6.35" y2="1.27" width="0.2" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.2" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-6.35" y2="-1.27" width="0.2" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.2" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="6.35" y2="1.27" width="0.2" layer="51"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.2" layer="51"/>
<wire x1="6.35" y1="-1.27" x2="-6.35" y2="-1.27" width="0.2" layer="51"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.2" layer="51"/>
<wire x1="-5.715" y1="-0.635" x2="-4.445" y2="-0.635" width="0.2" layer="51"/>
<wire x1="-4.445" y1="-0.635" x2="-4.445" y2="0.635" width="0.2" layer="51"/>
<wire x1="-4.445" y1="0.635" x2="-5.715" y2="0.635" width="0.2" layer="51"/>
<wire x1="-5.715" y1="0.635" x2="-5.715" y2="-0.635" width="0.2" layer="51"/>
<text x="-1" y="1.8" size="0.8128" layer="25" font="vector">&gt;Name</text>
<text x="-1" y="-0.5" size="0.8128" layer="51" font="vector">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="HEADER_THT_2.54X1_MALE-05">
<pin name="1" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="2.54" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="2.54" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.778" y="4.364" size="1.4224" layer="95" font="vector">1</text>
<text x="-1.778" y="1.824" size="1.4224" layer="95" font="vector">2</text>
<text x="-1.778" y="-0.716" size="1.4224" layer="95" font="vector">3</text>
<text x="-1.778" y="-3.256" size="1.4224" layer="95" font="vector">4</text>
<text x="-1.778" y="-5.796" size="1.4224" layer="95" font="vector">5</text>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="0" y="-10.16" size="1.778" layer="96" font="vector" rot="R180">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER_THT_2.54X1_MALE-05" prefix="JP" uservalue="yes">
<description>"HEADER_THT_2.54X1_MALE-05"</description>
<gates>
<gate name="JP" symbol="HEADER_THT_2.54X1_MALE-05" x="0" y="0"/>
</gates>
<devices>
<device name="-2.54" package="HEADER_THT_2.54X1_MALE-05">
<connects>
<connect gate="JP" pin="1" pad="1"/>
<connect gate="JP" pin="2" pad="2"/>
<connect gate="JP" pin="3" pad="3"/>
<connect gate="JP" pin="4" pad="4"/>
<connect gate="JP" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="-2.54"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="S1" library="switch-dil" deviceset="219-03*" device="" value="2792714"/>
<part name="U2" library="REBANE" deviceset="FT2232D" device=""/>
<part name="C9" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R9" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R10" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k5"/>
<part name="R11" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k"/>
<part name="C8" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C7" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C6" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C5" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="C4" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="0.1uF"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C10" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="18pF"/>
<part name="C11" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="18pF"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R7" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k5"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="R6" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="10k"/>
<part name="R13" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R14" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="270"/>
<part name="R15" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R16" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R17" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R12" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R18" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="R19" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="27"/>
<part name="U1" library="REBANE" deviceset="REGULATOR_EN_BYP" device="-TPS7933" value="XC6210B332MR"/>
<part name="C1" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="4.7uF"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="REBANE" deviceset="CAPACITOR" device="-0603[1608-METRIC]" value="4.7uF"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="FRAME_SCH1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="P+8" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="+3V6" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V1" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="P+4" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+5" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+7" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="P+3" library="REBANE_POWER" deviceset="+5V" device=""/>
<part name="U3" library="REBANE" deviceset="8PIN-CHIP" device="-SOIC-8" value="M93C46">
<attribute name="P1" value="S"/>
<attribute name="P2" value="C"/>
<attribute name="P3" value="D"/>
<attribute name="P4" value="Q"/>
<attribute name="P5" value="VSS"/>
<attribute name="P6" value="ORG"/>
<attribute name="P7" value="DU"/>
<attribute name="P8" value="VCC"/>
</part>
<part name="JP2" library="REBANE_HEADER_THT_2.54X1_MALE" deviceset="HEADER_THT_2.54X1_MALE-05" device="-2.54" technology="-2.54" value="N/A"/>
<part name="Q1" library="REBANE" deviceset="CRYSTAL-GND" device="-7X5" value="6MHz"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="JP1" library="newprog" deviceset="5X2" device=""/>
<part name="LED1" library="REBANE" deviceset="LED" device="-0603[1608-METRIC]" value="GREEN"/>
<part name="LED2" library="REBANE" deviceset="LED" device="-0603[1608-METRIC]" value="BLUE"/>
<part name="R1" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k"/>
<part name="R2" library="REBANE" deviceset="RESISTOR" device="-0603[1608-METRIC]" value="1k"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="+3V2" library="REBANE_POWER" deviceset="+3V3" device=""/>
<part name="+3V3" library="REBANE_POWER" deviceset="+3V3" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="7.62" y1="7.62" x2="157.48" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="7.62" x2="157.48" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="157.48" y1="27.94" x2="185.42" y2="27.94" width="0.1524" layer="94" style="longdash"/>
<wire x1="185.42" y1="27.94" x2="185.42" y2="121.92" width="0.1524" layer="94" style="longdash"/>
<wire x1="185.42" y1="121.92" x2="7.62" y2="121.92" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="121.92" x2="7.62" y2="7.62" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="172.72" x2="58.42" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="172.72" x2="58.42" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="58.42" y1="127" x2="7.62" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="7.62" y1="127" x2="7.62" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="63.5" y1="172.72" x2="149.86" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="149.86" y1="172.72" x2="149.86" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="149.86" y1="127" x2="63.5" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="63.5" y1="127" x2="63.5" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<text x="10.16" y="170.18" size="2.54" layer="94" font="vector" align="top-left">3V3 POWER</text>
<text x="66.04" y="170.18" size="2.54" layer="94" font="vector" align="top-left">JTAG HEADER</text>
<text x="10.16" y="119.38" size="2.54" layer="94" font="vector" align="top-left">FT2232D</text>
<wire x1="154.94" y1="172.72" x2="154.94" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="185.42" y1="172.72" x2="185.42" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="172.72" x2="185.42" y2="172.72" width="0.1524" layer="94" style="longdash"/>
<wire x1="154.94" y1="127" x2="185.42" y2="127" width="0.1524" layer="94" style="longdash"/>
<text x="157.48" y="170.18" size="2.54" layer="94" font="vector" align="top-left">LEDS</text>
</plain>
<instances>
<instance part="S1" gate="G$1" x="129.54" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="128.016" y="152.4" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="135.382" y="142.113" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="U2" gate="G$1" x="111.76" y="66.04" smashed="yes">
<attribute name="NAME" x="106.172" y="107.188" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="105.918" y="24.892" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C9" gate="C" x="63.5" y="91.44" smashed="yes">
<attribute name="NAME" x="62.23" y="91.694" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="91.694" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND1" gate="1" x="53.34" y="91.44" smashed="yes" rot="R270"/>
<instance part="GND3" gate="1" x="78.74" y="17.78" smashed="yes"/>
<instance part="R8" gate="R" x="43.18" y="81.28" smashed="yes">
<attribute name="NAME" x="40.132" y="81.534" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="46.228" y="81.534" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R9" gate="R" x="43.18" y="78.74" smashed="yes">
<attribute name="NAME" x="40.132" y="78.994" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="46.228" y="78.994" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R10" gate="R" x="63.5" y="73.66" smashed="yes">
<attribute name="NAME" x="60.452" y="74.168" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="66.548" y="74.168" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R11" gate="R" x="63.5" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="60.452" y="86.868" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="66.548" y="86.868" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C8" gate="C" x="63.5" y="93.98" smashed="yes">
<attribute name="NAME" x="62.23" y="94.234" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="94.234" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C7" gate="C" x="63.5" y="96.52" smashed="yes">
<attribute name="NAME" x="62.23" y="96.774" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="96.774" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="C" x="63.5" y="99.06" smashed="yes">
<attribute name="NAME" x="62.23" y="99.314" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="99.314" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="C" x="63.5" y="101.6" smashed="yes">
<attribute name="NAME" x="62.23" y="101.854" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="101.854" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C4" gate="C" x="63.5" y="104.14" smashed="yes">
<attribute name="NAME" x="62.23" y="104.394" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="104.394" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND4" gate="1" x="53.34" y="93.98" smashed="yes" rot="R270"/>
<instance part="GND5" gate="1" x="53.34" y="96.52" smashed="yes" rot="R270"/>
<instance part="GND6" gate="1" x="53.34" y="99.06" smashed="yes" rot="R270"/>
<instance part="GND7" gate="1" x="53.34" y="101.6" smashed="yes" rot="R270"/>
<instance part="GND8" gate="1" x="53.34" y="104.14" smashed="yes" rot="R270"/>
<instance part="C10" gate="C" x="63.5" y="68.58" smashed="yes">
<attribute name="NAME" x="62.23" y="69.088" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="69.088" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C11" gate="C" x="63.5" y="58.42" smashed="yes">
<attribute name="NAME" x="62.23" y="58.928" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="64.77" y="58.928" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND9" gate="1" x="53.34" y="68.58" smashed="yes" rot="R270"/>
<instance part="GND10" gate="1" x="53.34" y="58.42" smashed="yes" rot="R270"/>
<instance part="R7" gate="R" x="63.5" y="38.1" smashed="yes">
<attribute name="NAME" x="60.452" y="38.608" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="66.548" y="38.608" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND11" gate="1" x="15.24" y="17.78" smashed="yes"/>
<instance part="R6" gate="R" x="35.56" y="53.34" smashed="yes">
<attribute name="NAME" x="32.512" y="53.848" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="38.608" y="53.848" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R13" gate="R" x="142.24" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="104.394" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="104.394" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R14" gate="R" x="142.24" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="101.854" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="101.854" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R15" gate="R" x="142.24" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="99.314" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="99.314" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R16" gate="R" x="142.24" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="96.774" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="96.774" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R17" gate="R" x="142.24" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="94.234" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="94.234" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R12" gate="R" x="129.54" y="134.62" smashed="yes">
<attribute name="NAME" x="126.492" y="135.128" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="132.588" y="135.128" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R18" gate="R" x="142.24" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="66.294" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="66.294" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R19" gate="R" x="142.24" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="139.192" y="63.754" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="145.288" y="63.754" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="U1" gate="U" x="33.02" y="144.78" smashed="yes">
<attribute name="NAME" x="25.4" y="150.368" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="30.734" y="150.368" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C1" gate="C" x="15.24" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="14.732" y="135.89" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="14.732" y="138.43" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="15.24" y="129.54" smashed="yes"/>
<instance part="GND13" gate="1" x="33.02" y="129.54" smashed="yes"/>
<instance part="GND14" gate="1" x="50.8" y="129.54" smashed="yes"/>
<instance part="C2" gate="C" x="50.8" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="50.292" y="135.89" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="50.292" y="138.43" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND17" gate="1" x="27.94" y="71.12" smashed="yes"/>
<instance part="GND18" gate="1" x="68.58" y="157.48" smashed="yes" rot="R270"/>
<instance part="FRAME_SCH1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="P+8" gate="1" x="15.24" y="165.1"/>
<instance part="+3V6" gate="G$1" x="50.8" y="165.1"/>
<instance part="+3V1" gate="G$1" x="147.32" y="147.32" rot="R270"/>
<instance part="P+4" gate="1" x="147.32" y="149.86" rot="R270"/>
<instance part="P+5" gate="1" x="35.56" y="119.38"/>
<instance part="P+7" gate="1" x="78.74" y="119.38"/>
<instance part="P+3" gate="1" x="15.24" y="63.5"/>
<instance part="U3" gate="G$1" x="35.56" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.528" y="48.768" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="30.988" y="35.052" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="P1" x="42.418" y="44.704" size="1.778" layer="95" rot="MR0"/>
<attribute name="P2" x="42.418" y="42.164" size="1.778" layer="95" rot="MR0"/>
<attribute name="P3" x="42.418" y="39.624" size="1.778" layer="95" rot="MR0"/>
<attribute name="P4" x="42.418" y="37.084" size="1.778" layer="95" rot="MR0"/>
<attribute name="P5" x="28.702" y="37.084" size="1.778" layer="95"/>
<attribute name="P6" x="28.702" y="39.624" size="1.778" layer="95"/>
<attribute name="P7" x="28.702" y="42.164" size="1.778" layer="95"/>
<attribute name="P8" x="28.702" y="44.704" size="1.778" layer="95"/>
</instance>
<instance part="JP2" gate="JP" x="17.78" y="91.44" smashed="yes" rot="MR180">
<attribute name="NAME" x="19.304" y="99.568" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="Q1" gate="Q" x="76.2" y="63.5" rot="R270"/>
<instance part="GND2" gate="1" x="53.34" y="63.5" smashed="yes" rot="R270"/>
<instance part="JP1" gate="G$1" x="96.52" y="152.4"/>
<instance part="LED1" gate="LED" x="170.18" y="154.94" smashed="yes">
<attribute name="NAME" x="169.672" y="153.162" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="169.672" y="156.718" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="LED2" gate="LED" x="177.8" y="154.94" smashed="yes">
<attribute name="NAME" x="177.292" y="153.162" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="177.292" y="156.718" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R1" gate="R" x="170.18" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="169.672" y="136.652" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="169.672" y="142.748" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="R2" gate="R" x="177.8" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="177.292" y="136.652" size="1.778" layer="95" font="vector" rot="MR270"/>
<attribute name="VALUE" x="177.292" y="142.748" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="GND15" gate="1" x="170.18" y="129.54" smashed="yes"/>
<instance part="GND16" gate="1" x="177.8" y="129.54" smashed="yes"/>
<instance part="+3V2" gate="G$1" x="170.18" y="170.18"/>
<instance part="+3V3" gate="G$1" x="73.66" y="119.38"/>
</instances>
<busses>
</busses>
<nets>
<net name="3V3OUT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="3V3OUT"/>
<pinref part="C9" gate="C" pin="2"/>
<wire x1="66.04" y1="91.44" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<label x="81.28" y="91.44" size="1.778" layer="95" font="vector"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SI/WUA"/>
<wire x1="127" y1="71.12" x2="177.038" y2="71.12" width="0.1524" layer="91"/>
<label x="177.038" y="71.12" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SI/WUB"/>
<wire x1="127" y1="33.02" x2="177.038" y2="33.02" width="0.1524" layer="91"/>
<label x="177.038" y="33.02" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C9" gate="C" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="55.88" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="TEST"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="96.52" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="AGND"/>
<wire x1="78.74" y1="38.1" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="35.56" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<wire x1="78.74" y1="33.02" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="27.94" x2="78.74" y2="20.32" width="0.1524" layer="91"/>
<wire x1="96.52" y1="38.1" x2="78.74" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND1"/>
<wire x1="96.52" y1="35.56" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND2"/>
<wire x1="96.52" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND3"/>
<wire x1="96.52" y1="30.48" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND4"/>
<wire x1="96.52" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<junction x="78.74" y="38.1"/>
<junction x="78.74" y="35.56"/>
<junction x="78.74" y="33.02"/>
<junction x="78.74" y="30.48"/>
<junction x="78.74" y="27.94"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C4" gate="C" pin="1"/>
<wire x1="55.88" y1="104.14" x2="60.96" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C5" gate="C" pin="1"/>
<wire x1="55.88" y1="101.6" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="C6" gate="C" pin="1"/>
<wire x1="55.88" y1="99.06" x2="60.96" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C7" gate="C" pin="1"/>
<wire x1="55.88" y1="96.52" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C8" gate="C" pin="1"/>
<wire x1="55.88" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C10" gate="C" pin="1"/>
<wire x1="55.88" y1="68.58" x2="60.96" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C11" gate="C" pin="1"/>
<wire x1="55.88" y1="58.42" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="15.24" y1="38.1" x2="15.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="5"/>
<wire x1="25.4" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="15.24" y1="132.08" x2="15.24" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="U" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="33.02" y1="132.08" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="50.8" y1="132.08" x2="50.8" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="20.32" y1="86.36" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="1"/>
<pinref part="JP2" gate="JP" pin="2"/>
<wire x1="20.32" y1="88.9" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="27.94" y1="88.9" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<junction x="27.94" y="86.36"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="71.12" y1="157.48" x2="91.44" y2="157.48" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="P$2"/>
</segment>
<segment>
<pinref part="Q1" gate="Q" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="55.88" y1="63.5" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="R1" gate="R" pin="P$2"/>
<wire x1="170.18" y1="134.62" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R2" gate="R" pin="P$2"/>
<wire x1="177.8" y1="134.62" x2="177.8" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="76.2" y1="66.04" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="76.2" y1="68.58" x2="78.74" y2="68.58" width="0.1524" layer="91"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="XTIN"/>
<wire x1="78.74" y1="66.04" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C10" gate="C" pin="2"/>
<wire x1="66.04" y1="68.58" x2="76.2" y2="68.58" width="0.1524" layer="91"/>
<junction x="76.2" y="68.58"/>
<pinref part="Q1" gate="Q" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="XTOUT"/>
<wire x1="96.52" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C11" gate="C" pin="2"/>
<wire x1="66.04" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<junction x="76.2" y="58.42"/>
<pinref part="Q1" gate="Q" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R8" gate="R" pin="P$2"/>
<pinref part="U2" gate="G$1" pin="USBDM"/>
<wire x1="48.26" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R9" gate="R" pin="P$2"/>
<pinref part="U2" gate="G$1" pin="USBDP"/>
<wire x1="48.26" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R10" gate="R" pin="P$1"/>
<wire x1="53.34" y1="78.74" x2="96.52" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<junction x="53.34" y="78.74"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="RSTOUT#"/>
<pinref part="R10" gate="R" pin="P$2"/>
<wire x1="96.52" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="RESET#"/>
<wire x1="96.52" y1="71.12" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="78.74" y1="71.12" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC1"/>
<wire x1="78.74" y1="83.82" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="101.6" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC2"/>
<wire x1="96.52" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="104.14" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="78.74" y="104.14"/>
<junction x="78.74" y="101.6"/>
<pinref part="R11" gate="R" pin="P$2"/>
<pinref part="C4" gate="C" pin="2"/>
<wire x1="66.04" y1="104.14" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C5" gate="C" pin="2"/>
<wire x1="66.04" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+7" gate="1" pin="+5V"/>
<wire x1="78.74" y1="83.82" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="83.82" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="53.34" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<junction x="78.74" y="83.82"/>
</segment>
<segment>
<wire x1="15.24" y1="40.64" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<junction x="15.24" y="45.72"/>
<pinref part="R6" gate="R" pin="P$1"/>
<wire x1="15.24" y1="53.34" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="15.24" y2="53.34" width="0.1524" layer="91"/>
<junction x="15.24" y="53.34"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="U3" gate="G$1" pin="8"/>
<wire x1="25.4" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="6"/>
<wire x1="25.4" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="20.32" y1="96.52" x2="35.56" y2="96.52" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="5"/>
<wire x1="35.56" y1="96.52" x2="35.56" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="U" pin="EN"/>
<wire x1="22.86" y1="142.24" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<wire x1="15.24" y1="142.24" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U1" gate="U" pin="VIN"/>
<wire x1="15.24" y1="147.32" x2="15.24" y2="157.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="147.32" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<junction x="15.24" y="147.32"/>
<pinref part="C1" gate="C" pin="2"/>
<wire x1="15.24" y1="139.7" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="15.24" y="142.24"/>
<pinref part="P+8" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="137.16" y1="149.86" x2="139.7" y2="149.86" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="AVCC"/>
<pinref part="R11" gate="R" pin="P$1"/>
<wire x1="96.52" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="76.2" y1="99.06" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="68.58" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="2"/>
<wire x1="66.04" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="76.2" y="99.06"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EECS"/>
<wire x1="96.52" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<wire x1="68.58" y1="55.88" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
<wire x1="68.58" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EESK"/>
<wire x1="96.52" y1="53.34" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="71.12" y1="43.18" x2="45.72" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EEDATA"/>
<wire x1="96.52" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="40.64" x2="45.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R7" gate="R" pin="P$2"/>
<wire x1="68.58" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="38.1" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<junction x="73.66" y="40.64"/>
<pinref part="U3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R7" gate="R" pin="P$1"/>
<pinref part="R6" gate="R" pin="P$2"/>
<wire x1="53.34" y1="38.1" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="40.64" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="53.34" y="38.1"/>
<pinref part="U3" gate="G$1" pin="4"/>
<wire x1="45.72" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS0"/>
<pinref part="R13" gate="R" pin="P$2"/>
<wire x1="127" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS1"/>
<pinref part="R14" gate="R" pin="P$2"/>
<wire x1="127" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS2"/>
<pinref part="R15" gate="R" pin="P$2"/>
<wire x1="127" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS3"/>
<pinref part="R16" gate="R" pin="P$2"/>
<wire x1="127" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS4"/>
<pinref part="R17" gate="R" pin="P$2"/>
<wire x1="127" y1="93.98" x2="137.16" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TCK/SWC/TX_A" class="0">
<segment>
<pinref part="R13" gate="R" pin="P$1"/>
<wire x1="147.32" y1="104.14" x2="177.038" y2="104.14" width="0.1524" layer="91"/>
<label x="177.038" y="104.14" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="91.44" y1="154.94" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<label x="68.58" y="154.94" size="1.778" layer="95" font="vector"/>
<pinref part="JP1" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="TDI/RX_A" class="0">
<segment>
<pinref part="R14" gate="R" pin="P$1"/>
<label x="177.038" y="101.6" size="1.778" layer="95" font="vector" rot="MR0"/>
<wire x1="147.32" y1="101.6" x2="177.038" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="101.6" y1="157.48" x2="104.14" y2="157.48" width="0.1524" layer="91"/>
<label x="121.92" y="157.48" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="JP1" gate="G$1" pin="P$1"/>
<pinref part="S1" gate="G$1" pin="6"/>
<wire x1="104.14" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<wire x1="121.92" y1="144.78" x2="104.14" y2="144.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="144.78" x2="104.14" y2="157.48" width="0.1524" layer="91"/>
<junction x="104.14" y="157.48"/>
</segment>
</net>
<net name="TDO/SWD/RTS_A" class="0">
<segment>
<pinref part="R15" gate="R" pin="P$1"/>
<label x="177.038" y="99.06" size="1.778" layer="95" font="vector" rot="MR0"/>
<wire x1="147.32" y1="99.06" x2="177.038" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="91.44" y1="152.4" x2="88.9" y2="152.4" width="0.1524" layer="91"/>
<label x="68.58" y="152.4" size="1.778" layer="95" font="vector"/>
<pinref part="JP1" gate="G$1" pin="P$6"/>
<wire x1="88.9" y1="152.4" x2="68.58" y2="152.4" width="0.1524" layer="91"/>
<wire x1="124.46" y1="134.62" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<wire x1="88.9" y1="134.62" x2="88.9" y2="152.4" width="0.1524" layer="91"/>
<junction x="88.9" y="152.4"/>
<pinref part="R12" gate="R" pin="P$1"/>
</segment>
</net>
<net name="TMS/CTS_A" class="0">
<segment>
<pinref part="R16" gate="R" pin="P$1"/>
<wire x1="147.32" y1="96.52" x2="177.038" y2="96.52" width="0.1524" layer="91"/>
<label x="177.038" y="96.52" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="101.6" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<label x="121.92" y="154.94" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="JP1" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="TRST/DTR_A" class="0">
<segment>
<pinref part="R17" gate="R" pin="P$1"/>
<wire x1="147.32" y1="93.98" x2="177.038" y2="93.98" width="0.1524" layer="91"/>
<label x="177.038" y="93.98" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="101.6" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="121.92" y="152.4" size="1.778" layer="95" font="vector" rot="MR0"/>
<pinref part="JP1" gate="G$1" pin="P$5"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS0"/>
<pinref part="R18" gate="R" pin="P$2"/>
<wire x1="127" y1="66.04" x2="137.16" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BDBUS1"/>
<pinref part="R19" gate="R" pin="P$2"/>
<wire x1="127" y1="63.5" x2="137.16" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_B" class="0">
<segment>
<pinref part="R18" gate="R" pin="P$1"/>
<wire x1="147.32" y1="66.04" x2="177.038" y2="66.04" width="0.1524" layer="91"/>
<label x="177.038" y="66.04" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="91.44" y1="147.32" x2="68.58" y2="147.32" width="0.1524" layer="91"/>
<label x="68.58" y="147.32" size="1.778" layer="95" font="vector"/>
<pinref part="JP1" gate="G$1" pin="P$10"/>
</segment>
</net>
<net name="RX_B" class="0">
<segment>
<pinref part="R19" gate="R" pin="P$1"/>
<wire x1="147.32" y1="63.5" x2="177.038" y2="63.5" width="0.1524" layer="91"/>
<label x="177.038" y="63.5" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
<segment>
<wire x1="91.44" y1="149.86" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<label x="68.58" y="149.86" size="1.778" layer="95" font="vector"/>
<pinref part="JP1" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="U" pin="VOUT"/>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="43.18" y1="147.32" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="147.32" x2="50.8" y2="139.7" width="0.1524" layer="91"/>
<wire x1="50.8" y1="157.48" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<junction x="50.8" y="147.32"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="137.16" y1="147.32" x2="139.7" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="LED1" gate="LED" pin="A"/>
<wire x1="170.18" y1="157.48" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCCIOA"/>
<wire x1="96.52" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C7" gate="C" pin="2"/>
<pinref part="U2" gate="G$1" pin="VCCIOB"/>
<wire x1="73.66" y1="96.52" x2="66.04" y2="96.52" width="0.1524" layer="91"/>
<wire x1="96.52" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C8" gate="C" pin="2"/>
<wire x1="73.66" y1="93.98" x2="66.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="73.66" y1="96.52" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="73.66" y="96.52"/>
<junction x="73.66" y="93.98"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="73.66" y1="111.76" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="20.32" y1="93.98" x2="35.56" y2="93.98" width="0.1524" layer="91"/>
<wire x1="35.56" y1="93.98" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R8" gate="R" pin="P$1"/>
<wire x1="35.56" y1="81.28" x2="38.1" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="4"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="20.32" y1="91.44" x2="33.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="33.02" y1="91.44" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R9" gate="R" pin="P$1"/>
<wire x1="33.02" y1="78.74" x2="38.1" y2="78.74" width="0.1524" layer="91"/>
<pinref part="JP2" gate="JP" pin="3"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="4"/>
<pinref part="JP1" gate="G$1" pin="P$7"/>
<wire x1="121.92" y1="149.86" x2="101.6" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="P$9"/>
<pinref part="S1" gate="G$1" pin="5"/>
<wire x1="101.6" y1="147.32" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R12" gate="R" pin="P$2"/>
<wire x1="134.62" y1="134.62" x2="144.78" y2="134.62" width="0.1524" layer="91"/>
<wire x1="144.78" y1="134.62" x2="144.78" y2="144.78" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="144.78" y1="144.78" x2="137.16" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<wire x1="177.8" y1="157.48" x2="177.8" y2="170.18" width="0.1524" layer="91"/>
<label x="177.8" y="170.18" size="1.778" layer="95" font="vector" rot="MR270"/>
<pinref part="LED2" gate="LED" pin="A"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="ADBUS5"/>
<wire x1="127" y1="91.44" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<label x="177.8" y="91.44" size="1.778" layer="95" font="vector" rot="MR0"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R1" gate="R" pin="P$1"/>
<pinref part="LED1" gate="LED" pin="C"/>
<wire x1="170.18" y1="152.4" x2="170.18" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R2" gate="R" pin="P$1"/>
<pinref part="LED2" gate="LED" pin="C"/>
<wire x1="177.8" y1="152.4" x2="177.8" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
